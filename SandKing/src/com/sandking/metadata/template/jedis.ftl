package ${packageName};

<#list jedisImports as imports>
import ${imports};
</#list>

public class ${d_tableName}Jedis{
	<#list indexKeys as index>
	/**
	 * 根据(<#list index.columnNames as columnName> ${columnName} </#list>) 查询
	 */
	<#if index.unique>
	<#-- 唯一索引查询 -->
	public static ${d_tableName} getBy<#list index.d_columnNames as d_columnName>${d_columnName}</#list>(${index.all_basicType_x_columnName}){
		${d_tableName} ${x_tableName} = null;
		String key = SK_Plus.b(Bydl.TABLENAME, "_Object").e();
		String field = SK_Plus.b(id).e();
		Jedis jedis = SK_Jedis.getInstance().getJedis();
		try {

		} catch (Exception e) {
			// TODO: handle exception
		} finally {
			SK_Jedis.getInstance().returnJedis(jedis);
		}
		<#if index.indexName == "PRIMARY">
		${x_tableName} = <#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.get(key);
		
		<#else>
		key = <#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.getValseByKey(key);
		if(key!=null){
			${x_tableName} = ${primaryX_columnName}Cache.get(key);	
		} 
		</#if>
		if(${x_tableName}==null){
			//查询数据库
			${x_tableName} = ${d_tableName}Dao.getBy<#list index.d_columnNames as d_columnName>${d_columnName}</#list>(${index.all_x_columnName});
			if(${x_tableName}!=null){
				//加入缓存
				loadCache(${x_tableName});
			}
		}
		return ${x_tableName};
	}
	
	<#else>
	<#-- 聚集索引查询 -->
	public static List<${d_tableName}> getBy<#list index.d_columnNames as d_columnName>${d_columnName}</#list>(${index.all_basicType_x_columnName}){
		List<${d_tableName}> ${x_tableName}s = new ArrayList<${d_tableName}>();
		String key = ${index.all_d_columnName_plus};
		Set<String> keys = <#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			${d_tableName} ${x_tableName} = null;
			for (String k : list) {
				${x_tableName} = getBy${primaryD_columnName}(Integer.valueOf(k));
				if (${x_tableName} == null) continue;
					${x_tableName}s.add(${x_tableName});
			}
		}else{
			${x_tableName}s = ${d_tableName}Dao.getBy<#list index.d_columnNames as d_columnName>${d_columnName}</#list>(${index.all_x_columnName});
			if(!${x_tableName}s.isEmpty()){
				loadCaches(${x_tableName}s);
			}
		}
		return ${x_tableName}s;
	}
	
	<#-- 聚集索引分页查询 -->
	public static List<${d_tableName}> getByPage<#list index.d_columnNames as d_columnName>${d_columnName}</#list>(${index.all_basicType_x_columnName},int page,int pageCount){
		List<${d_tableName}> ${x_tableName}s = null;
		String key = ${index.all_d_columnName_plus};
		Set<String> keys = <#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.get(key);
		if(keys == null){
			${x_tableName}s = getBy<#list index.d_columnNames as d_columnName>${d_columnName}</#list>(${index.all_x_columnName});
		}
		${x_tableName}s = SK_List.getPage(${x_tableName}s, page, pageCount);
		return ${x_tableName}s;
	}
	</#if>
	</#list>
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<${d_tableName}> ${x_tableName}s){
		for(${d_tableName} ${x_tableName} : ${x_tableName}s){
			loadCache(${x_tableName});
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(${d_tableName} ${x_tableName}){
		<#list indexKeys as index>
		<#if index.unique>
		<#if index.indexName == "PRIMARY">
		<#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.put(${index.all_d_columnName_get},${x_tableName});
		<#else>
		<#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.put(${index.all_d_columnName_get},String.valueOf(${x_tableName}.get${primaryD_columnName}()));
		</#if>
		<#else>
		Set<String> <#list index.x_columnNames as x_columnName>${x_columnName}</#list>set = <#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.get(String.valueOf(${index.all_d_columnName_get}));
		if(<#list index.x_columnNames as x_columnName>${x_columnName}</#list>set == null){
			<#list index.x_columnNames as x_columnName>${x_columnName}</#list>set = new HashSet<String>();
		}
		<#list index.x_columnNames as x_columnName>${x_columnName}</#list>set.add(String.valueOf(${x_tableName}.get${primaryD_columnName}()));
		<#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.put(${index.all_d_columnName_get},<#list index.x_columnNames as x_columnName>${x_columnName}</#list>set);
		</#if>
		</#list>
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(${d_tableName} ${x_tableName}){
		<#list indexKeys as index>
		<#if index.unique>
		<#if index.indexName == "PRIMARY">
		<#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.remove(${index.all_d_columnName_get});
		<#else>
		<#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.removeByValue(String.valueOf(${x_tableName}.get${primaryD_columnName}()));
		</#if>
		<#else>
		Set<String> <#list index.x_columnNames as x_columnName>${x_columnName}</#list>set = <#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.get(String.valueOf(${index.all_d_columnName_get}));
		if(<#list index.x_columnNames as x_columnName>${x_columnName}</#list>set == null){
			<#list index.x_columnNames as x_columnName>${x_columnName}</#list>set = new HashSet<String>();
		}
		if (<#list index.x_columnNames as x_columnName>${x_columnName}</#list>set.contains(String.valueOf(${x_tableName}.get${primaryD_columnName}()))) {
			<#list index.x_columnNames as x_columnName>${x_columnName}</#list>set.remove(String.valueOf(${x_tableName}.get${primaryD_columnName}()));
		}
		<#list index.x_columnNames as x_columnName>${x_columnName}</#list>Cache.put(${index.all_d_columnName_get},<#list index.x_columnNames as x_columnName>${x_columnName}</#list>set);
		</#if>
		</#list>
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<${d_tableName}> ${x_tableName}s){
		for(${d_tableName} ${x_tableName} : ${x_tableName}s){
			clearCache(${x_tableName});
		}
	}
	
	public static ${d_tableName} insert(${d_tableName} ${x_tableName}){
		int id = LASTID.get();
    	if(id < 1){
    		${x_tableName} = ${d_tableName}Dao.insert(${x_tableName});
    		LASTID.set(${x_tableName}.get${primaryD_columnName}());
    	}else{
    		${x_tableName}.set${primaryD_columnName}(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(${x_tableName});
    	return ${x_tableName};
    }
    
    public static ${d_tableName} update(${d_tableName} ${x_tableName}){
    	clearCache(${x_tableName});
    	loadCache(${x_tableName});
    	//加入定时器
    	return ${x_tableName};
    }
    
    public static boolean delete(${d_tableName} ${x_tableName}){
    	clearCache(${x_tableName});
    	//加入定时器
    	return false;
    }
    
    public static ${d_tableName} updateAndFlush(${d_tableName} ${x_tableName}){
    	update(${x_tableName});
    	return ${d_tableName}Dao.update(${x_tableName});
    }
    
    public static ${d_tableName} insertAndFlush(${d_tableName} ${x_tableName}){
    	int id = LASTID.get();
    	insert(${x_tableName});
    	if(id > 0){
    		${x_tableName} = ${d_tableName}Dao.insert(${x_tableName});
    	}
    	return ${x_tableName};
    }
    
    public static boolean deleteAndFlush(${d_tableName} ${x_tableName}){
    	delete(${x_tableName});
    	return ${d_tableName}Dao.delete(${x_tableName});
    }
}