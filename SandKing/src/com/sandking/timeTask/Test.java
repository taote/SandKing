package com.sandking.timeTask;

import java.util.Date;

import com.sandking.tools.SK_Date;

/**
 * @UserName : SandKing
 * @DataTime : 2013年11月26日 下午10:55:53
 * @Description ：任务调度器测试
 */
public class Test {
	public static void main(String[] args) throws InterruptedException {
		
		MyTask mytask = new MyTask(SK_Date.addMillisSecond(5000, new Date()),
				null, "Group", "task1");
		// 添加一个任务
		TaskManager.addJob(mytask);
		// 删除一个任务
		TaskManager.deleteJob("Group", "task1");
		// 修改一个任务
		TaskManager.updateJob("Group", "task1", SK_Date.addDay(1, new Date()));
	}
}
