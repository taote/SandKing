package com.sandking.timeTask;

import java.util.Date;

/**
 * @UserName : SandKing
 * @DataTime : 2012-9-21 上午11:11:54
 * @Description ：Please describe this document
 */
public abstract class JobManager implements Runnable {
	public Date runTime = null;
	public Object obj = null;
	public String groupName = "";
	public String jobName = "";

	public JobManager(Date runTime, Object obj, String groupName, String jobName) {
		super();
		this.runTime = runTime;
		this.obj = obj;
		this.groupName = groupName;
		this.jobName = jobName;
	}

	public void run() {
		JobTask jobTask = (JobTask) this;
		// 执行任务
		jobTask.execution(jobTask);
		// 删除任务对象
		Group group = TaskManager.GROUP.get(groupName);
		if (group == null) {
			return;
		}
		group.getJobs().remove(jobName);
		if (group.getJobs().isEmpty()) {
			TaskManager.GROUP.remove(groupName);
		}
	}

}
