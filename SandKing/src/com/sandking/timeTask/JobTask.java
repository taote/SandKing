package com.sandking.timeTask;

import java.util.Date;

/**
 * @UserName : SandKing
 * @DataTime : 2012-9-21 下午12:18:48
 * @Description ：任务基类
 */
public abstract class JobTask extends JobManager {
	public JobTask(Date runTime, Object obj, String groupName, String jobName) {
		super(runTime, obj, groupName, jobName);
	}

	public abstract void execution(JobTask jobTask);

}
