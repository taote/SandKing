package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.dao.JzdlDao;
import com.sandking.db.bean.Jzdl;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.tools.SK_List;
public class JzdlCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Jzdl> idCache = new Hashtable<String, Jzdl>();
	//列表关系
	static final Map<String, Set<String>> yhjz_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Jzdl getById(int id){
		Jzdl jzdl = null;
		String key = SK_Plus.b(id).e();
		jzdl = idCache.get(key);
		
		if(jzdl==null){
			//查询数据库
			jzdl = JzdlDao.getById(id);
			if(jzdl!=null){
				//加入缓存
				loadCache(jzdl);
			}
		}
		return jzdl;
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Jzdl> getByYhjz_id(int yhjz_id){
		List<Jzdl> jzdls = new ArrayList<Jzdl>();
		String key = SK_Plus.b(yhjz_id).e();
		Set<String> keys = yhjz_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Jzdl jzdl = null;
			for (String k : list) {
				jzdl = getById(Integer.valueOf(k));
				if (jzdl == null) continue;
					jzdls.add(jzdl);
			}
		}else{
			jzdls = JzdlDao.getByYhjz_id(yhjz_id);
			if(!jzdls.isEmpty()){
				loadCaches(jzdls);
			}
		}
		return jzdls;
	}
	
	public static List<Jzdl> getByPageYhjz_id(int yhjz_id,int page,int pageCount){
		List<Jzdl> jzdls = null;
		String key = SK_Plus.b(yhjz_id).e();
		Set<String> keys = yhjz_idCache.get(key);
		if(keys == null){
			jzdls = getByYhjz_id(yhjz_id);
		}
		jzdls = SK_List.getPage(jzdls, page, pageCount);
		return jzdls;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Jzdl> jzdls){
		for(Jzdl jzdl : jzdls){
			loadCache(jzdl);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Jzdl jzdl){
		idCache.put(SK_Plus.b(jzdl.getId()).e(),jzdl);
		Set<String> yhjz_idset = yhjz_idCache.get(String.valueOf(SK_Plus.b(jzdl.getYhjz_id()).e()));
		if(yhjz_idset == null){
			yhjz_idset = new HashSet<String>();
		}
		yhjz_idset.add(String.valueOf(jzdl.getId()));
		yhjz_idCache.put(SK_Plus.b(jzdl.getYhjz_id()).e(),yhjz_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Jzdl jzdl){
		idCache.remove(SK_Plus.b(jzdl.getId()).e());
		Set<String> yhjz_idset = yhjz_idCache.get(String.valueOf(SK_Plus.b(jzdl.getYhjz_id()).e()));
		if(yhjz_idset == null){
			yhjz_idset = new HashSet<String>();
		}
		if (yhjz_idset.contains(String.valueOf(jzdl.getId()))) {
			yhjz_idset.remove(String.valueOf(jzdl.getId()));
		}
		yhjz_idCache.put(SK_Plus.b(jzdl.getYhjz_id()).e(),yhjz_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Jzdl> jzdls){
		for(Jzdl jzdl : jzdls){
			clearCache(jzdl);
		}
	}
	
	public static Jzdl insert(Jzdl jzdl){
		int id = LASTID.get();
    	if(id < 1){
    		jzdl = JzdlDao.insert(jzdl);
    		LASTID.set(jzdl.getId());
    	}else{
    		jzdl.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(jzdl);
    	return jzdl;
    }
    
    public static Jzdl update(Jzdl jzdl){
    	clearCache(jzdl);
    	loadCache(jzdl);
    	//加入定时器
    	return jzdl;
    }
    
    public static boolean delete(Jzdl jzdl){
    	clearCache(jzdl);
    	//加入定时器
    	return false;
    }
    
    public static Jzdl updateAndFlush(Jzdl jzdl){
    	update(jzdl);
    	return JzdlDao.update(jzdl);
    }
    
    public static Jzdl insertAndFlush(Jzdl jzdl){
    	int id = LASTID.get();
    	insert(jzdl);
    	if(id > 0){
    		jzdl = JzdlDao.insert(jzdl);
    	}
    	return jzdl;
    }
    
    public static boolean deleteAndFlush(Jzdl jzdl){
    	delete(jzdl);
    	return JzdlDao.delete(jzdl);
    }
}