package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.dao.BydlDao;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.db.bean.Bydl;
import com.sandking.tools.SK_List;
public class BydlCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Bydl> idCache = new Hashtable<String, Bydl>();
	//列表关系
	static final Map<String, Set<String>> yhjz_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Bydl getById(int id){
		Bydl bydl = null;
		String key = SK_Plus.b(id).e();
		bydl = idCache.get(key);
		
		if(bydl==null){
			//查询数据库
			bydl = BydlDao.getById(id);
			if(bydl!=null){
				//加入缓存
				loadCache(bydl);
			}
		}
		return bydl;
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Bydl> getByYhjz_id(int yhjz_id){
		List<Bydl> bydls = new ArrayList<Bydl>();
		String key = SK_Plus.b(yhjz_id).e();
		Set<String> keys = yhjz_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Bydl bydl = null;
			for (String k : list) {
				bydl = getById(Integer.valueOf(k));
				if (bydl == null) continue;
					bydls.add(bydl);
			}
		}else{
			bydls = BydlDao.getByYhjz_id(yhjz_id);
			if(!bydls.isEmpty()){
				loadCaches(bydls);
			}
		}
		return bydls;
	}
	
	public static List<Bydl> getByPageYhjz_id(int yhjz_id,int page,int pageCount){
		List<Bydl> bydls = null;
		String key = SK_Plus.b(yhjz_id).e();
		Set<String> keys = yhjz_idCache.get(key);
		if(keys == null){
			bydls = getByYhjz_id(yhjz_id);
		}
		bydls = SK_List.getPage(bydls, page, pageCount);
		return bydls;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Bydl> bydls){
		for(Bydl bydl : bydls){
			loadCache(bydl);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Bydl bydl){
		idCache.put(SK_Plus.b(bydl.getId()).e(),bydl);
		Set<String> yhjz_idset = yhjz_idCache.get(String.valueOf(SK_Plus.b(bydl.getYhjz_id()).e()));
		if(yhjz_idset == null){
			yhjz_idset = new HashSet<String>();
		}
		yhjz_idset.add(String.valueOf(bydl.getId()));
		yhjz_idCache.put(SK_Plus.b(bydl.getYhjz_id()).e(),yhjz_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Bydl bydl){
		idCache.remove(SK_Plus.b(bydl.getId()).e());
		Set<String> yhjz_idset = yhjz_idCache.get(String.valueOf(SK_Plus.b(bydl.getYhjz_id()).e()));
		if(yhjz_idset == null){
			yhjz_idset = new HashSet<String>();
		}
		if (yhjz_idset.contains(String.valueOf(bydl.getId()))) {
			yhjz_idset.remove(String.valueOf(bydl.getId()));
		}
		yhjz_idCache.put(SK_Plus.b(bydl.getYhjz_id()).e(),yhjz_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Bydl> bydls){
		for(Bydl bydl : bydls){
			clearCache(bydl);
		}
	}
	
	public static Bydl insert(Bydl bydl){
		int id = LASTID.get();
    	if(id < 1){
    		bydl = BydlDao.insert(bydl);
    		LASTID.set(bydl.getId());
    	}else{
    		bydl.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(bydl);
    	return bydl;
    }
    
    public static Bydl update(Bydl bydl){
    	clearCache(bydl);
    	loadCache(bydl);
    	//加入定时器
    	return bydl;
    }
    
    public static boolean delete(Bydl bydl){
    	clearCache(bydl);
    	//加入定时器
    	return false;
    }
    
    public static Bydl updateAndFlush(Bydl bydl){
    	update(bydl);
    	return BydlDao.update(bydl);
    }
    
    public static Bydl insertAndFlush(Bydl bydl){
    	int id = LASTID.get();
    	insert(bydl);
    	if(id > 0){
    		bydl = BydlDao.insert(bydl);
    	}
    	return bydl;
    }
    
    public static boolean deleteAndFlush(Bydl bydl){
    	delete(bydl);
    	return BydlDao.delete(bydl);
    }
}