package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import com.sandking.db.bean.Yxjn;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.tools.SK_List;
import com.sandking.db.dao.YxjnDao;
public class YxjnCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yxjn> idCache = new Hashtable<String, Yxjn>();
	//列表关系
	static final Map<String, Set<String>> yhyx_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yxjn getById(int id){
		Yxjn yxjn = null;
		String key = SK_Plus.b(id).e();
		yxjn = idCache.get(key);
		
		if(yxjn==null){
			//查询数据库
			yxjn = YxjnDao.getById(id);
			if(yxjn!=null){
				//加入缓存
				loadCache(yxjn);
			}
		}
		return yxjn;
	}
	
	/**
	 * 根据( 用户英雄_id ) 查询
	 */
	public static List<Yxjn> getByYhyx_id(int yhyx_id){
		List<Yxjn> yxjns = new ArrayList<Yxjn>();
		String key = SK_Plus.b(yhyx_id).e();
		Set<String> keys = yhyx_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yxjn yxjn = null;
			for (String k : list) {
				yxjn = getById(Integer.valueOf(k));
				if (yxjn == null) continue;
					yxjns.add(yxjn);
			}
		}else{
			yxjns = YxjnDao.getByYhyx_id(yhyx_id);
			if(!yxjns.isEmpty()){
				loadCaches(yxjns);
			}
		}
		return yxjns;
	}
	
	public static List<Yxjn> getByPageYhyx_id(int yhyx_id,int page,int pageCount){
		List<Yxjn> yxjns = null;
		String key = SK_Plus.b(yhyx_id).e();
		Set<String> keys = yhyx_idCache.get(key);
		if(keys == null){
			yxjns = getByYhyx_id(yhyx_id);
		}
		yxjns = SK_List.getPage(yxjns, page, pageCount);
		return yxjns;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yxjn> yxjns){
		for(Yxjn yxjn : yxjns){
			loadCache(yxjn);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yxjn yxjn){
		idCache.put(SK_Plus.b(yxjn.getId()).e(),yxjn);
		Set<String> yhyx_idset = yhyx_idCache.get(String.valueOf(SK_Plus.b(yxjn.getYhyx_id()).e()));
		if(yhyx_idset == null){
			yhyx_idset = new HashSet<String>();
		}
		yhyx_idset.add(String.valueOf(yxjn.getId()));
		yhyx_idCache.put(SK_Plus.b(yxjn.getYhyx_id()).e(),yhyx_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yxjn yxjn){
		idCache.remove(SK_Plus.b(yxjn.getId()).e());
		Set<String> yhyx_idset = yhyx_idCache.get(String.valueOf(SK_Plus.b(yxjn.getYhyx_id()).e()));
		if(yhyx_idset == null){
			yhyx_idset = new HashSet<String>();
		}
		if (yhyx_idset.contains(String.valueOf(yxjn.getId()))) {
			yhyx_idset.remove(String.valueOf(yxjn.getId()));
		}
		yhyx_idCache.put(SK_Plus.b(yxjn.getYhyx_id()).e(),yhyx_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yxjn> yxjns){
		for(Yxjn yxjn : yxjns){
			clearCache(yxjn);
		}
	}
	
	public static Yxjn insert(Yxjn yxjn){
		int id = LASTID.get();
    	if(id < 1){
    		yxjn = YxjnDao.insert(yxjn);
    		LASTID.set(yxjn.getId());
    	}else{
    		yxjn.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yxjn);
    	return yxjn;
    }
    
    public static Yxjn update(Yxjn yxjn){
    	clearCache(yxjn);
    	loadCache(yxjn);
    	//加入定时器
    	return yxjn;
    }
    
    public static boolean delete(Yxjn yxjn){
    	clearCache(yxjn);
    	//加入定时器
    	return false;
    }
    
    public static Yxjn updateAndFlush(Yxjn yxjn){
    	update(yxjn);
    	return YxjnDao.update(yxjn);
    }
    
    public static Yxjn insertAndFlush(Yxjn yxjn){
    	int id = LASTID.get();
    	insert(yxjn);
    	if(id > 0){
    		yxjn = YxjnDao.insert(yxjn);
    	}
    	return yxjn;
    }
    
    public static boolean deleteAndFlush(Yxjn yxjn){
    	delete(yxjn);
    	return YxjnDao.delete(yxjn);
    }
}