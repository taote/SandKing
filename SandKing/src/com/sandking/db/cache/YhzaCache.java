package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.db.bean.Yhza;
import com.sandking.tools.SK_List;
import com.sandking.db.dao.YhzaDao;
public class YhzaCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yhza> idCache = new Hashtable<String, Yhza>();
	//列表关系
	static final Map<String, Set<String>> yh_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhza getById(int id){
		Yhza yhza = null;
		String key = SK_Plus.b(id).e();
		yhza = idCache.get(key);
		
		if(yhza==null){
			//查询数据库
			yhza = YhzaDao.getById(id);
			if(yhza!=null){
				//加入缓存
				loadCache(yhza);
			}
		}
		return yhza;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhza> getByYh_id(int yh_id){
		List<Yhza> yhzas = new ArrayList<Yhza>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yhza yhza = null;
			for (String k : list) {
				yhza = getById(Integer.valueOf(k));
				if (yhza == null) continue;
					yhzas.add(yhza);
			}
		}else{
			yhzas = YhzaDao.getByYh_id(yh_id);
			if(!yhzas.isEmpty()){
				loadCaches(yhzas);
			}
		}
		return yhzas;
	}
	
	public static List<Yhza> getByPageYh_id(int yh_id,int page,int pageCount){
		List<Yhza> yhzas = null;
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys == null){
			yhzas = getByYh_id(yh_id);
		}
		yhzas = SK_List.getPage(yhzas, page, pageCount);
		return yhzas;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhza> yhzas){
		for(Yhza yhza : yhzas){
			loadCache(yhza);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhza yhza){
		idCache.put(SK_Plus.b(yhza.getId()).e(),yhza);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhza.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhza.getId()));
		yh_idCache.put(SK_Plus.b(yhza.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yhza yhza){
		idCache.remove(SK_Plus.b(yhza.getId()).e());
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhza.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhza.getId()))) {
			yh_idset.remove(String.valueOf(yhza.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhza.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yhza> yhzas){
		for(Yhza yhza : yhzas){
			clearCache(yhza);
		}
	}
	
	public static Yhza insert(Yhza yhza){
		int id = LASTID.get();
    	if(id < 1){
    		yhza = YhzaDao.insert(yhza);
    		LASTID.set(yhza.getId());
    	}else{
    		yhza.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yhza);
    	return yhza;
    }
    
    public static Yhza update(Yhza yhza){
    	clearCache(yhza);
    	loadCache(yhza);
    	//加入定时器
    	return yhza;
    }
    
    public static boolean delete(Yhza yhza){
    	clearCache(yhza);
    	//加入定时器
    	return false;
    }
    
    public static Yhza updateAndFlush(Yhza yhza){
    	update(yhza);
    	return YhzaDao.update(yhza);
    }
    
    public static Yhza insertAndFlush(Yhza yhza){
    	int id = LASTID.get();
    	insert(yhza);
    	if(id > 0){
    		yhza = YhzaDao.insert(yhza);
    	}
    	return yhza;
    }
    
    public static boolean deleteAndFlush(Yhza yhza){
    	delete(yhza);
    	return YhzaDao.delete(yhza);
    }
}