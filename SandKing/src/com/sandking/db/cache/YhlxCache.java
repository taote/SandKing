package com.sandking.db.cache;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.List;
import java.util.Hashtable;
import com.sandking.db.dao.YhlxDao;
import com.sandking.db.bean.Yhlx;
public class YhlxCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yhlx> idCache = new Hashtable<String, Yhlx>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhlx getById(int id){
		Yhlx yhlx = null;
		String key = SK_Plus.b(id).e();
		yhlx = idCache.get(key);
		
		if(yhlx==null){
			//查询数据库
			yhlx = YhlxDao.getById(id);
			if(yhlx!=null){
				//加入缓存
				loadCache(yhlx);
			}
		}
		return yhlx;
	}
	
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhlx> yhlxs){
		for(Yhlx yhlx : yhlxs){
			loadCache(yhlx);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhlx yhlx){
		idCache.put(SK_Plus.b(yhlx.getId()).e(),yhlx);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yhlx yhlx){
		idCache.remove(SK_Plus.b(yhlx.getId()).e());
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yhlx> yhlxs){
		for(Yhlx yhlx : yhlxs){
			clearCache(yhlx);
		}
	}
	
	public static Yhlx insert(Yhlx yhlx){
		int id = LASTID.get();
    	if(id < 1){
    		yhlx = YhlxDao.insert(yhlx);
    		LASTID.set(yhlx.getId());
    	}else{
    		yhlx.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yhlx);
    	return yhlx;
    }
    
    public static Yhlx update(Yhlx yhlx){
    	clearCache(yhlx);
    	loadCache(yhlx);
    	//加入定时器
    	return yhlx;
    }
    
    public static boolean delete(Yhlx yhlx){
    	clearCache(yhlx);
    	//加入定时器
    	return false;
    }
    
    public static Yhlx updateAndFlush(Yhlx yhlx){
    	update(yhlx);
    	return YhlxDao.update(yhlx);
    }
    
    public static Yhlx insertAndFlush(Yhlx yhlx){
    	int id = LASTID.get();
    	insert(yhlx);
    	if(id > 0){
    		yhlx = YhlxDao.insert(yhlx);
    	}
    	return yhlx;
    }
    
    public static boolean deleteAndFlush(Yhlx yhlx){
    	delete(yhlx);
    	return YhlxDao.delete(yhlx);
    }
}