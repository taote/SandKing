package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import com.sandking.db.bean.Yhyx;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.db.dao.YhyxDao;
import com.sandking.tools.SK_List;
public class YhyxCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yhyx> idCache = new Hashtable<String, Yhyx>();
	//列表关系
	static final Map<String, Set<String>> yh_idCache = new Hashtable<String, Set<String>>();
	//列表关系
	static final Map<String, Set<String>> yxztidCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhyx getById(int id){
		Yhyx yhyx = null;
		String key = SK_Plus.b(id).e();
		yhyx = idCache.get(key);
		
		if(yhyx==null){
			//查询数据库
			yhyx = YhyxDao.getById(id);
			if(yhyx!=null){
				//加入缓存
				loadCache(yhyx);
			}
		}
		return yhyx;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhyx> getByYh_id(int yh_id){
		List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yhyx yhyx = null;
			for (String k : list) {
				yhyx = getById(Integer.valueOf(k));
				if (yhyx == null) continue;
					yhyxs.add(yhyx);
			}
		}else{
			yhyxs = YhyxDao.getByYh_id(yh_id);
			if(!yhyxs.isEmpty()){
				loadCaches(yhyxs);
			}
		}
		return yhyxs;
	}
	
	public static List<Yhyx> getByPageYh_id(int yh_id,int page,int pageCount){
		List<Yhyx> yhyxs = null;
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys == null){
			yhyxs = getByYh_id(yh_id);
		}
		yhyxs = SK_List.getPage(yhyxs, page, pageCount);
		return yhyxs;
	}
	/**
	 * 根据( 英雄状态id ) 查询
	 */
	public static List<Yhyx> getByYxztid(int yxztid){
		List<Yhyx> yhyxs = new ArrayList<Yhyx>();
		String key = SK_Plus.b(yxztid).e();
		Set<String> keys = yxztidCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yhyx yhyx = null;
			for (String k : list) {
				yhyx = getById(Integer.valueOf(k));
				if (yhyx == null) continue;
					yhyxs.add(yhyx);
			}
		}else{
			yhyxs = YhyxDao.getByYxztid(yxztid);
			if(!yhyxs.isEmpty()){
				loadCaches(yhyxs);
			}
		}
		return yhyxs;
	}
	
	public static List<Yhyx> getByPageYxztid(int yxztid,int page,int pageCount){
		List<Yhyx> yhyxs = null;
		String key = SK_Plus.b(yxztid).e();
		Set<String> keys = yxztidCache.get(key);
		if(keys == null){
			yhyxs = getByYxztid(yxztid);
		}
		yhyxs = SK_List.getPage(yhyxs, page, pageCount);
		return yhyxs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhyx> yhyxs){
		for(Yhyx yhyx : yhyxs){
			loadCache(yhyx);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhyx yhyx){
		idCache.put(SK_Plus.b(yhyx.getId()).e(),yhyx);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhyx.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhyx.getId()));
		yh_idCache.put(SK_Plus.b(yhyx.getYh_id()).e(),yh_idset);
		Set<String> yxztidset = yxztidCache.get(String.valueOf(SK_Plus.b(yhyx.getYxztid()).e()));
		if(yxztidset == null){
			yxztidset = new HashSet<String>();
		}
		yxztidset.add(String.valueOf(yhyx.getId()));
		yxztidCache.put(SK_Plus.b(yhyx.getYxztid()).e(),yxztidset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yhyx yhyx){
		idCache.remove(SK_Plus.b(yhyx.getId()).e());
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhyx.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhyx.getId()))) {
			yh_idset.remove(String.valueOf(yhyx.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhyx.getYh_id()).e(),yh_idset);
		Set<String> yxztidset = yxztidCache.get(String.valueOf(SK_Plus.b(yhyx.getYxztid()).e()));
		if(yxztidset == null){
			yxztidset = new HashSet<String>();
		}
		if (yxztidset.contains(String.valueOf(yhyx.getId()))) {
			yxztidset.remove(String.valueOf(yhyx.getId()));
		}
		yxztidCache.put(SK_Plus.b(yhyx.getYxztid()).e(),yxztidset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yhyx> yhyxs){
		for(Yhyx yhyx : yhyxs){
			clearCache(yhyx);
		}
	}
	
	public static Yhyx insert(Yhyx yhyx){
		int id = LASTID.get();
    	if(id < 1){
    		yhyx = YhyxDao.insert(yhyx);
    		LASTID.set(yhyx.getId());
    	}else{
    		yhyx.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yhyx);
    	return yhyx;
    }
    
    public static Yhyx update(Yhyx yhyx){
    	clearCache(yhyx);
    	loadCache(yhyx);
    	//加入定时器
    	return yhyx;
    }
    
    public static boolean delete(Yhyx yhyx){
    	clearCache(yhyx);
    	//加入定时器
    	return false;
    }
    
    public static Yhyx updateAndFlush(Yhyx yhyx){
    	update(yhyx);
    	return YhyxDao.update(yhyx);
    }
    
    public static Yhyx insertAndFlush(Yhyx yhyx){
    	int id = LASTID.get();
    	insert(yhyx);
    	if(id > 0){
    		yhyx = YhyxDao.insert(yhyx);
    	}
    	return yhyx;
    }
    
    public static boolean deleteAndFlush(Yhyx yhyx){
    	delete(yhyx);
    	return YhyxDao.delete(yhyx);
    }
}