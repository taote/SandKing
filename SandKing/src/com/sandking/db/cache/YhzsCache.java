package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.db.dao.YhzsDao;
import com.sandking.db.bean.Yhzs;
import com.sandking.tools.SK_List;
public class YhzsCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yhzs> idCache = new Hashtable<String, Yhzs>();
	//列表关系
	static final Map<String, Set<String>> yh_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhzs getById(int id){
		Yhzs yhzs = null;
		String key = SK_Plus.b(id).e();
		yhzs = idCache.get(key);
		
		if(yhzs==null){
			//查询数据库
			yhzs = YhzsDao.getById(id);
			if(yhzs!=null){
				//加入缓存
				loadCache(yhzs);
			}
		}
		return yhzs;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhzs> getByYh_id(int yh_id){
		List<Yhzs> yhzss = new ArrayList<Yhzs>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yhzs yhzs = null;
			for (String k : list) {
				yhzs = getById(Integer.valueOf(k));
				if (yhzs == null) continue;
					yhzss.add(yhzs);
			}
		}else{
			yhzss = YhzsDao.getByYh_id(yh_id);
			if(!yhzss.isEmpty()){
				loadCaches(yhzss);
			}
		}
		return yhzss;
	}
	
	public static List<Yhzs> getByPageYh_id(int yh_id,int page,int pageCount){
		List<Yhzs> yhzss = null;
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys == null){
			yhzss = getByYh_id(yh_id);
		}
		yhzss = SK_List.getPage(yhzss, page, pageCount);
		return yhzss;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhzs> yhzss){
		for(Yhzs yhzs : yhzss){
			loadCache(yhzs);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhzs yhzs){
		idCache.put(SK_Plus.b(yhzs.getId()).e(),yhzs);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhzs.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhzs.getId()));
		yh_idCache.put(SK_Plus.b(yhzs.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yhzs yhzs){
		idCache.remove(SK_Plus.b(yhzs.getId()).e());
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhzs.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhzs.getId()))) {
			yh_idset.remove(String.valueOf(yhzs.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhzs.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yhzs> yhzss){
		for(Yhzs yhzs : yhzss){
			clearCache(yhzs);
		}
	}
	
	public static Yhzs insert(Yhzs yhzs){
		int id = LASTID.get();
    	if(id < 1){
    		yhzs = YhzsDao.insert(yhzs);
    		LASTID.set(yhzs.getId());
    	}else{
    		yhzs.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yhzs);
    	return yhzs;
    }
    
    public static Yhzs update(Yhzs yhzs){
    	clearCache(yhzs);
    	loadCache(yhzs);
    	//加入定时器
    	return yhzs;
    }
    
    public static boolean delete(Yhzs yhzs){
    	clearCache(yhzs);
    	//加入定时器
    	return false;
    }
    
    public static Yhzs updateAndFlush(Yhzs yhzs){
    	update(yhzs);
    	return YhzsDao.update(yhzs);
    }
    
    public static Yhzs insertAndFlush(Yhzs yhzs){
    	int id = LASTID.get();
    	insert(yhzs);
    	if(id > 0){
    		yhzs = YhzsDao.insert(yhzs);
    	}
    	return yhzs;
    }
    
    public static boolean deleteAndFlush(Yhzs yhzs){
    	delete(yhzs);
    	return YhzsDao.delete(yhzs);
    }
}