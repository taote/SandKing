package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import com.sandking.db.bean.Yhxj;
import com.sandking.db.dao.YhxjDao;
import java.util.Set;
import com.sandking.tools.SK_List;
public class YhxjCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yhxj> idCache = new Hashtable<String, Yhxj>();
	//列表关系
	static final Map<String, Set<String>> yh_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhxj getById(int id){
		Yhxj yhxj = null;
		String key = SK_Plus.b(id).e();
		yhxj = idCache.get(key);
		
		if(yhxj==null){
			//查询数据库
			yhxj = YhxjDao.getById(id);
			if(yhxj!=null){
				//加入缓存
				loadCache(yhxj);
			}
		}
		return yhxj;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhxj> getByYh_id(int yh_id){
		List<Yhxj> yhxjs = new ArrayList<Yhxj>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yhxj yhxj = null;
			for (String k : list) {
				yhxj = getById(Integer.valueOf(k));
				if (yhxj == null) continue;
					yhxjs.add(yhxj);
			}
		}else{
			yhxjs = YhxjDao.getByYh_id(yh_id);
			if(!yhxjs.isEmpty()){
				loadCaches(yhxjs);
			}
		}
		return yhxjs;
	}
	
	public static List<Yhxj> getByPageYh_id(int yh_id,int page,int pageCount){
		List<Yhxj> yhxjs = null;
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys == null){
			yhxjs = getByYh_id(yh_id);
		}
		yhxjs = SK_List.getPage(yhxjs, page, pageCount);
		return yhxjs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhxj> yhxjs){
		for(Yhxj yhxj : yhxjs){
			loadCache(yhxj);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhxj yhxj){
		idCache.put(SK_Plus.b(yhxj.getId()).e(),yhxj);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhxj.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhxj.getId()));
		yh_idCache.put(SK_Plus.b(yhxj.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yhxj yhxj){
		idCache.remove(SK_Plus.b(yhxj.getId()).e());
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhxj.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhxj.getId()))) {
			yh_idset.remove(String.valueOf(yhxj.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhxj.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yhxj> yhxjs){
		for(Yhxj yhxj : yhxjs){
			clearCache(yhxj);
		}
	}
	
	public static Yhxj insert(Yhxj yhxj){
		int id = LASTID.get();
    	if(id < 1){
    		yhxj = YhxjDao.insert(yhxj);
    		LASTID.set(yhxj.getId());
    	}else{
    		yhxj.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yhxj);
    	return yhxj;
    }
    
    public static Yhxj update(Yhxj yhxj){
    	clearCache(yhxj);
    	loadCache(yhxj);
    	//加入定时器
    	return yhxj;
    }
    
    public static boolean delete(Yhxj yhxj){
    	clearCache(yhxj);
    	//加入定时器
    	return false;
    }
    
    public static Yhxj updateAndFlush(Yhxj yhxj){
    	update(yhxj);
    	return YhxjDao.update(yhxj);
    }
    
    public static Yhxj insertAndFlush(Yhxj yhxj){
    	int id = LASTID.get();
    	insert(yhxj);
    	if(id > 0){
    		yhxj = YhxjDao.insert(yhxj);
    	}
    	return yhxj;
    }
    
    public static boolean deleteAndFlush(Yhxj yhxj){
    	delete(yhxj);
    	return YhxjDao.delete(yhxj);
    }
}