package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.dao.YhbbDao;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import com.sandking.db.bean.Yhbb;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.tools.SK_List;
public class YhbbCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yhbb> idCache = new Hashtable<String, Yhbb>();
	//列表关系
	static final Map<String, Set<String>> yh_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhbb getById(int id){
		Yhbb yhbb = null;
		String key = SK_Plus.b(id).e();
		yhbb = idCache.get(key);
		
		if(yhbb==null){
			//查询数据库
			yhbb = YhbbDao.getById(id);
			if(yhbb!=null){
				//加入缓存
				loadCache(yhbb);
			}
		}
		return yhbb;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhbb> getByYh_id(int yh_id){
		List<Yhbb> yhbbs = new ArrayList<Yhbb>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yhbb yhbb = null;
			for (String k : list) {
				yhbb = getById(Integer.valueOf(k));
				if (yhbb == null) continue;
					yhbbs.add(yhbb);
			}
		}else{
			yhbbs = YhbbDao.getByYh_id(yh_id);
			if(!yhbbs.isEmpty()){
				loadCaches(yhbbs);
			}
		}
		return yhbbs;
	}
	
	public static List<Yhbb> getByPageYh_id(int yh_id,int page,int pageCount){
		List<Yhbb> yhbbs = null;
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys == null){
			yhbbs = getByYh_id(yh_id);
		}
		yhbbs = SK_List.getPage(yhbbs, page, pageCount);
		return yhbbs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhbb> yhbbs){
		for(Yhbb yhbb : yhbbs){
			loadCache(yhbb);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhbb yhbb){
		idCache.put(SK_Plus.b(yhbb.getId()).e(),yhbb);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhbb.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhbb.getId()));
		yh_idCache.put(SK_Plus.b(yhbb.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yhbb yhbb){
		idCache.remove(SK_Plus.b(yhbb.getId()).e());
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhbb.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhbb.getId()))) {
			yh_idset.remove(String.valueOf(yhbb.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhbb.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yhbb> yhbbs){
		for(Yhbb yhbb : yhbbs){
			clearCache(yhbb);
		}
	}
	
	public static Yhbb insert(Yhbb yhbb){
		int id = LASTID.get();
    	if(id < 1){
    		yhbb = YhbbDao.insert(yhbb);
    		LASTID.set(yhbb.getId());
    	}else{
    		yhbb.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yhbb);
    	return yhbb;
    }
    
    public static Yhbb update(Yhbb yhbb){
    	clearCache(yhbb);
    	loadCache(yhbb);
    	//加入定时器
    	return yhbb;
    }
    
    public static boolean delete(Yhbb yhbb){
    	clearCache(yhbb);
    	//加入定时器
    	return false;
    }
    
    public static Yhbb updateAndFlush(Yhbb yhbb){
    	update(yhbb);
    	return YhbbDao.update(yhbb);
    }
    
    public static Yhbb insertAndFlush(Yhbb yhbb){
    	int id = LASTID.get();
    	insert(yhbb);
    	if(id > 0){
    		yhbb = YhbbDao.insert(yhbb);
    	}
    	return yhbb;
    }
    
    public static boolean deleteAndFlush(Yhbb yhbb){
    	delete(yhbb);
    	return YhbbDao.delete(yhbb);
    }
}