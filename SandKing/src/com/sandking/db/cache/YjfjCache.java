package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.dao.YjfjDao;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.db.bean.Yjfj;
import com.sandking.tools.SK_List;
public class YjfjCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yjfj> idCache = new Hashtable<String, Yjfj>();
	//列表关系
	static final Map<String, Set<String>> yjlx_idCache = new Hashtable<String, Set<String>>();
	//列表关系
	static final Map<String, Set<String>> yhyj_idCache = new Hashtable<String, Set<String>>();
	//列表关系
	static final Map<String, Set<String>> fjlx_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yjfj getById(int id){
		Yjfj yjfj = null;
		String key = SK_Plus.b(id).e();
		yjfj = idCache.get(key);
		
		if(yjfj==null){
			//查询数据库
			yjfj = YjfjDao.getById(id);
			if(yjfj!=null){
				//加入缓存
				loadCache(yjfj);
			}
		}
		return yjfj;
	}
	
	/**
	 * 根据( 邮件类型_id ) 查询
	 */
	public static List<Yjfj> getByYjlx_id(int yjlx_id){
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		String key = SK_Plus.b(yjlx_id).e();
		Set<String> keys = yjlx_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yjfj yjfj = null;
			for (String k : list) {
				yjfj = getById(Integer.valueOf(k));
				if (yjfj == null) continue;
					yjfjs.add(yjfj);
			}
		}else{
			yjfjs = YjfjDao.getByYjlx_id(yjlx_id);
			if(!yjfjs.isEmpty()){
				loadCaches(yjfjs);
			}
		}
		return yjfjs;
	}
	
	public static List<Yjfj> getByPageYjlx_id(int yjlx_id,int page,int pageCount){
		List<Yjfj> yjfjs = null;
		String key = SK_Plus.b(yjlx_id).e();
		Set<String> keys = yjlx_idCache.get(key);
		if(keys == null){
			yjfjs = getByYjlx_id(yjlx_id);
		}
		yjfjs = SK_List.getPage(yjfjs, page, pageCount);
		return yjfjs;
	}
	/**
	 * 根据( 用户邮件_id ) 查询
	 */
	public static List<Yjfj> getByYhyj_id(int yhyj_id){
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		String key = SK_Plus.b(yhyj_id).e();
		Set<String> keys = yhyj_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yjfj yjfj = null;
			for (String k : list) {
				yjfj = getById(Integer.valueOf(k));
				if (yjfj == null) continue;
					yjfjs.add(yjfj);
			}
		}else{
			yjfjs = YjfjDao.getByYhyj_id(yhyj_id);
			if(!yjfjs.isEmpty()){
				loadCaches(yjfjs);
			}
		}
		return yjfjs;
	}
	
	public static List<Yjfj> getByPageYhyj_id(int yhyj_id,int page,int pageCount){
		List<Yjfj> yjfjs = null;
		String key = SK_Plus.b(yhyj_id).e();
		Set<String> keys = yhyj_idCache.get(key);
		if(keys == null){
			yjfjs = getByYhyj_id(yhyj_id);
		}
		yjfjs = SK_List.getPage(yjfjs, page, pageCount);
		return yjfjs;
	}
	/**
	 * 根据( 附件类型_id ) 查询
	 */
	public static List<Yjfj> getByFjlx_id(int fjlx_id){
		List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		String key = SK_Plus.b(fjlx_id).e();
		Set<String> keys = fjlx_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yjfj yjfj = null;
			for (String k : list) {
				yjfj = getById(Integer.valueOf(k));
				if (yjfj == null) continue;
					yjfjs.add(yjfj);
			}
		}else{
			yjfjs = YjfjDao.getByFjlx_id(fjlx_id);
			if(!yjfjs.isEmpty()){
				loadCaches(yjfjs);
			}
		}
		return yjfjs;
	}
	
	public static List<Yjfj> getByPageFjlx_id(int fjlx_id,int page,int pageCount){
		List<Yjfj> yjfjs = null;
		String key = SK_Plus.b(fjlx_id).e();
		Set<String> keys = fjlx_idCache.get(key);
		if(keys == null){
			yjfjs = getByFjlx_id(fjlx_id);
		}
		yjfjs = SK_List.getPage(yjfjs, page, pageCount);
		return yjfjs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yjfj> yjfjs){
		for(Yjfj yjfj : yjfjs){
			loadCache(yjfj);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yjfj yjfj){
		idCache.put(SK_Plus.b(yjfj.getId()).e(),yjfj);
		Set<String> yjlx_idset = yjlx_idCache.get(String.valueOf(SK_Plus.b(yjfj.getYjlx_id()).e()));
		if(yjlx_idset == null){
			yjlx_idset = new HashSet<String>();
		}
		yjlx_idset.add(String.valueOf(yjfj.getId()));
		yjlx_idCache.put(SK_Plus.b(yjfj.getYjlx_id()).e(),yjlx_idset);
		Set<String> yhyj_idset = yhyj_idCache.get(String.valueOf(SK_Plus.b(yjfj.getYhyj_id()).e()));
		if(yhyj_idset == null){
			yhyj_idset = new HashSet<String>();
		}
		yhyj_idset.add(String.valueOf(yjfj.getId()));
		yhyj_idCache.put(SK_Plus.b(yjfj.getYhyj_id()).e(),yhyj_idset);
		Set<String> fjlx_idset = fjlx_idCache.get(String.valueOf(SK_Plus.b(yjfj.getFjlx_id()).e()));
		if(fjlx_idset == null){
			fjlx_idset = new HashSet<String>();
		}
		fjlx_idset.add(String.valueOf(yjfj.getId()));
		fjlx_idCache.put(SK_Plus.b(yjfj.getFjlx_id()).e(),fjlx_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yjfj yjfj){
		idCache.remove(SK_Plus.b(yjfj.getId()).e());
		Set<String> yjlx_idset = yjlx_idCache.get(String.valueOf(SK_Plus.b(yjfj.getYjlx_id()).e()));
		if(yjlx_idset == null){
			yjlx_idset = new HashSet<String>();
		}
		if (yjlx_idset.contains(String.valueOf(yjfj.getId()))) {
			yjlx_idset.remove(String.valueOf(yjfj.getId()));
		}
		yjlx_idCache.put(SK_Plus.b(yjfj.getYjlx_id()).e(),yjlx_idset);
		Set<String> yhyj_idset = yhyj_idCache.get(String.valueOf(SK_Plus.b(yjfj.getYhyj_id()).e()));
		if(yhyj_idset == null){
			yhyj_idset = new HashSet<String>();
		}
		if (yhyj_idset.contains(String.valueOf(yjfj.getId()))) {
			yhyj_idset.remove(String.valueOf(yjfj.getId()));
		}
		yhyj_idCache.put(SK_Plus.b(yjfj.getYhyj_id()).e(),yhyj_idset);
		Set<String> fjlx_idset = fjlx_idCache.get(String.valueOf(SK_Plus.b(yjfj.getFjlx_id()).e()));
		if(fjlx_idset == null){
			fjlx_idset = new HashSet<String>();
		}
		if (fjlx_idset.contains(String.valueOf(yjfj.getId()))) {
			fjlx_idset.remove(String.valueOf(yjfj.getId()));
		}
		fjlx_idCache.put(SK_Plus.b(yjfj.getFjlx_id()).e(),fjlx_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yjfj> yjfjs){
		for(Yjfj yjfj : yjfjs){
			clearCache(yjfj);
		}
	}
	
	public static Yjfj insert(Yjfj yjfj){
		int id = LASTID.get();
    	if(id < 1){
    		yjfj = YjfjDao.insert(yjfj);
    		LASTID.set(yjfj.getId());
    	}else{
    		yjfj.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yjfj);
    	return yjfj;
    }
    
    public static Yjfj update(Yjfj yjfj){
    	clearCache(yjfj);
    	loadCache(yjfj);
    	//加入定时器
    	return yjfj;
    }
    
    public static boolean delete(Yjfj yjfj){
    	clearCache(yjfj);
    	//加入定时器
    	return false;
    }
    
    public static Yjfj updateAndFlush(Yjfj yjfj){
    	update(yjfj);
    	return YjfjDao.update(yjfj);
    }
    
    public static Yjfj insertAndFlush(Yjfj yjfj){
    	int id = LASTID.get();
    	insert(yjfj);
    	if(id > 0){
    		yjfj = YjfjDao.insert(yjfj);
    	}
    	return yjfj;
    }
    
    public static boolean deleteAndFlush(Yjfj yjfj){
    	delete(yjfj);
    	return YjfjDao.delete(yjfj);
    }
}