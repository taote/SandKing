package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.dao.LmDao;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.db.bean.Lm;
import com.sandking.tools.SK_List;
public class LmCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Lm> idCache = new Hashtable<String, Lm>();
	//列表关系
	static final Map<String, Set<String>> cjridCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Lm getById(int id){
		Lm lm = null;
		String key = SK_Plus.b(id).e();
		lm = idCache.get(key);
		
		if(lm==null){
			//查询数据库
			lm = LmDao.getById(id);
			if(lm!=null){
				//加入缓存
				loadCache(lm);
			}
		}
		return lm;
	}
	
	/**
	 * 根据( 创建人id ) 查询
	 */
	public static List<Lm> getByCjrid(int cjrid){
		List<Lm> lms = new ArrayList<Lm>();
		String key = SK_Plus.b(cjrid).e();
		Set<String> keys = cjridCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Lm lm = null;
			for (String k : list) {
				lm = getById(Integer.valueOf(k));
				if (lm == null) continue;
					lms.add(lm);
			}
		}else{
			lms = LmDao.getByCjrid(cjrid);
			if(!lms.isEmpty()){
				loadCaches(lms);
			}
		}
		return lms;
	}
	
	public static List<Lm> getByPageCjrid(int cjrid,int page,int pageCount){
		List<Lm> lms = null;
		String key = SK_Plus.b(cjrid).e();
		Set<String> keys = cjridCache.get(key);
		if(keys == null){
			lms = getByCjrid(cjrid);
		}
		lms = SK_List.getPage(lms, page, pageCount);
		return lms;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Lm> lms){
		for(Lm lm : lms){
			loadCache(lm);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Lm lm){
		idCache.put(SK_Plus.b(lm.getId()).e(),lm);
		Set<String> cjridset = cjridCache.get(String.valueOf(SK_Plus.b(lm.getCjrid()).e()));
		if(cjridset == null){
			cjridset = new HashSet<String>();
		}
		cjridset.add(String.valueOf(lm.getId()));
		cjridCache.put(SK_Plus.b(lm.getCjrid()).e(),cjridset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Lm lm){
		idCache.remove(SK_Plus.b(lm.getId()).e());
		Set<String> cjridset = cjridCache.get(String.valueOf(SK_Plus.b(lm.getCjrid()).e()));
		if(cjridset == null){
			cjridset = new HashSet<String>();
		}
		if (cjridset.contains(String.valueOf(lm.getId()))) {
			cjridset.remove(String.valueOf(lm.getId()));
		}
		cjridCache.put(SK_Plus.b(lm.getCjrid()).e(),cjridset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Lm> lms){
		for(Lm lm : lms){
			clearCache(lm);
		}
	}
	
	public static Lm insert(Lm lm){
		int id = LASTID.get();
    	if(id < 1){
    		lm = LmDao.insert(lm);
    		LASTID.set(lm.getId());
    	}else{
    		lm.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(lm);
    	return lm;
    }
    
    public static Lm update(Lm lm){
    	clearCache(lm);
    	loadCache(lm);
    	//加入定时器
    	return lm;
    }
    
    public static boolean delete(Lm lm){
    	clearCache(lm);
    	//加入定时器
    	return false;
    }
    
    public static Lm updateAndFlush(Lm lm){
    	update(lm);
    	return LmDao.update(lm);
    }
    
    public static Lm insertAndFlush(Lm lm){
    	int id = LASTID.get();
    	insert(lm);
    	if(id > 0){
    		lm = LmDao.insert(lm);
    	}
    	return lm;
    }
    
    public static boolean deleteAndFlush(Lm lm){
    	delete(lm);
    	return LmDao.delete(lm);
    }
}