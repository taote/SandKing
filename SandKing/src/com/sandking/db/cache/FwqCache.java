package com.sandking.db.cache;

import java.util.Map;
import com.sandking.db.dao.FwqDao;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.List;
import java.util.Hashtable;
import com.sandking.db.bean.Fwq;
public class FwqCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Fwq> idCache = new Hashtable<String, Fwq>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Fwq getById(int id){
		Fwq fwq = null;
		String key = SK_Plus.b(id).e();
		fwq = idCache.get(key);
		
		if(fwq==null){
			//查询数据库
			fwq = FwqDao.getById(id);
			if(fwq!=null){
				//加入缓存
				loadCache(fwq);
			}
		}
		return fwq;
	}
	
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Fwq> fwqs){
		for(Fwq fwq : fwqs){
			loadCache(fwq);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Fwq fwq){
		idCache.put(SK_Plus.b(fwq.getId()).e(),fwq);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Fwq fwq){
		idCache.remove(SK_Plus.b(fwq.getId()).e());
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Fwq> fwqs){
		for(Fwq fwq : fwqs){
			clearCache(fwq);
		}
	}
	
	public static Fwq insert(Fwq fwq){
		int id = LASTID.get();
    	if(id < 1){
    		fwq = FwqDao.insert(fwq);
    		LASTID.set(fwq.getId());
    	}else{
    		fwq.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(fwq);
    	return fwq;
    }
    
    public static Fwq update(Fwq fwq){
    	clearCache(fwq);
    	loadCache(fwq);
    	//加入定时器
    	return fwq;
    }
    
    public static boolean delete(Fwq fwq){
    	clearCache(fwq);
    	//加入定时器
    	return false;
    }
    
    public static Fwq updateAndFlush(Fwq fwq){
    	update(fwq);
    	return FwqDao.update(fwq);
    }
    
    public static Fwq insertAndFlush(Fwq fwq){
    	int id = LASTID.get();
    	insert(fwq);
    	if(id > 0){
    		fwq = FwqDao.insert(fwq);
    	}
    	return fwq;
    }
    
    public static boolean deleteAndFlush(Fwq fwq){
    	delete(fwq);
    	return FwqDao.delete(fwq);
    }
}