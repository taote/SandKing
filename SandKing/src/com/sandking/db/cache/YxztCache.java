package com.sandking.db.cache;

import com.sandking.db.dao.YxztDao;
import com.sandking.db.bean.Yxzt;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.List;
import java.util.Hashtable;
public class YxztCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yxzt> idCache = new Hashtable<String, Yxzt>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yxzt getById(int id){
		Yxzt yxzt = null;
		String key = SK_Plus.b(id).e();
		yxzt = idCache.get(key);
		
		if(yxzt==null){
			//查询数据库
			yxzt = YxztDao.getById(id);
			if(yxzt!=null){
				//加入缓存
				loadCache(yxzt);
			}
		}
		return yxzt;
	}
	
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yxzt> yxzts){
		for(Yxzt yxzt : yxzts){
			loadCache(yxzt);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yxzt yxzt){
		idCache.put(SK_Plus.b(yxzt.getId()).e(),yxzt);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yxzt yxzt){
		idCache.remove(SK_Plus.b(yxzt.getId()).e());
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yxzt> yxzts){
		for(Yxzt yxzt : yxzts){
			clearCache(yxzt);
		}
	}
	
	public static Yxzt insert(Yxzt yxzt){
		int id = LASTID.get();
    	if(id < 1){
    		yxzt = YxztDao.insert(yxzt);
    		LASTID.set(yxzt.getId());
    	}else{
    		yxzt.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yxzt);
    	return yxzt;
    }
    
    public static Yxzt update(Yxzt yxzt){
    	clearCache(yxzt);
    	loadCache(yxzt);
    	//加入定时器
    	return yxzt;
    }
    
    public static boolean delete(Yxzt yxzt){
    	clearCache(yxzt);
    	//加入定时器
    	return false;
    }
    
    public static Yxzt updateAndFlush(Yxzt yxzt){
    	update(yxzt);
    	return YxztDao.update(yxzt);
    }
    
    public static Yxzt insertAndFlush(Yxzt yxzt){
    	int id = LASTID.get();
    	insert(yxzt);
    	if(id > 0){
    		yxzt = YxztDao.insert(yxzt);
    	}
    	return yxzt;
    }
    
    public static boolean deleteAndFlush(Yxzt yxzt){
    	delete(yxzt);
    	return YxztDao.delete(yxzt);
    }
}