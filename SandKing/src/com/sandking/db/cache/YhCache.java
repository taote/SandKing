package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.tools.SK_IndexMap;
import java.util.Map;
import com.sandking.tools.SK_Plus;
import java.util.List;
import java.util.Hashtable;
import com.sandking.db.dao.YhDao;
import com.sandking.db.bean.Yh;
import com.sandking.tools.SK_List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Vector;
import java.util.HashSet;
import java.util.Set;
public class YhCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yh> idCache = new Hashtable<String, Yh>();
	static final SK_IndexMap<String, String> ncCache = new SK_IndexMap<String, String>();
	static final SK_IndexMap<String, String> zhCache = new SK_IndexMap<String, String>();
	static final SK_IndexMap<String, String> zhmmCache = new SK_IndexMap<String, String>();
	//列表关系
	static final Map<String, Set<String>> yhlx_idCache = new Hashtable<String, Set<String>>();
	//列表关系
	static final Map<String, Set<String>> fwq_idCache = new Hashtable<String, Set<String>>();
	//列表关系
	static final Map<String, Set<String>> qdCache = new Hashtable<String, Set<String>>();
	//列表关系
	static final Map<String, Set<String>> lm_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yh getById(int id){
		Yh yh = null;
		String key = SK_Plus.b(id).e();
		yh = idCache.get(key);
		
		if(yh==null){
			//查询数据库
			yh = YhDao.getById(id);
			if(yh!=null){
				//加入缓存
				loadCache(yh);
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 昵称 ) 查询
	 */
	public static Yh getByNc(String nc){
		Yh yh = null;
		String key = SK_Plus.b(nc).e();
		key = ncCache.getValseByKey(key);
		if(key!=null){
			yh = idCache.get(key);	
		} 
		if(yh==null){
			//查询数据库
			yh = YhDao.getByNc(nc);
			if(yh!=null){
				//加入缓存
				loadCache(yh);
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 账号 ) 查询
	 */
	public static Yh getByZh(String zh){
		Yh yh = null;
		String key = SK_Plus.b(zh).e();
		key = zhCache.getValseByKey(key);
		if(key!=null){
			yh = idCache.get(key);	
		} 
		if(yh==null){
			//查询数据库
			yh = YhDao.getByZh(zh);
			if(yh!=null){
				//加入缓存
				loadCache(yh);
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 账号  密码 ) 查询
	 */
	public static Yh getByZhMm(String zh, String mm){
		Yh yh = null;
		String key = SK_Plus.b(zh,mm).e();
		key = zhmmCache.getValseByKey(key);
		if(key!=null){
			yh = idCache.get(key);	
		} 
		if(yh==null){
			//查询数据库
			yh = YhDao.getByZhMm(zh, mm);
			if(yh!=null){
				//加入缓存
				loadCache(yh);
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 用户类型_id ) 查询
	 */
	public static List<Yh> getByYhlx_id(int yhlx_id){
		List<Yh> yhs = new ArrayList<Yh>();
		String key = SK_Plus.b(yhlx_id).e();
		Set<String> keys = yhlx_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yh yh = null;
			for (String k : list) {
				yh = getById(Integer.valueOf(k));
				if (yh == null) continue;
					yhs.add(yh);
			}
		}else{
			yhs = YhDao.getByYhlx_id(yhlx_id);
			if(!yhs.isEmpty()){
				loadCaches(yhs);
			}
		}
		return yhs;
	}
	
	public static List<Yh> getByPageYhlx_id(int yhlx_id,int page,int pageCount){
		List<Yh> yhs = null;
		String key = SK_Plus.b(yhlx_id).e();
		Set<String> keys = yhlx_idCache.get(key);
		if(keys == null){
			yhs = getByYhlx_id(yhlx_id);
		}
		yhs = SK_List.getPage(yhs, page, pageCount);
		return yhs;
	}
	/**
	 * 根据( 服务器_id ) 查询
	 */
	public static List<Yh> getByFwq_id(int fwq_id){
		List<Yh> yhs = new ArrayList<Yh>();
		String key = SK_Plus.b(fwq_id).e();
		Set<String> keys = fwq_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yh yh = null;
			for (String k : list) {
				yh = getById(Integer.valueOf(k));
				if (yh == null) continue;
					yhs.add(yh);
			}
		}else{
			yhs = YhDao.getByFwq_id(fwq_id);
			if(!yhs.isEmpty()){
				loadCaches(yhs);
			}
		}
		return yhs;
	}
	
	public static List<Yh> getByPageFwq_id(int fwq_id,int page,int pageCount){
		List<Yh> yhs = null;
		String key = SK_Plus.b(fwq_id).e();
		Set<String> keys = fwq_idCache.get(key);
		if(keys == null){
			yhs = getByFwq_id(fwq_id);
		}
		yhs = SK_List.getPage(yhs, page, pageCount);
		return yhs;
	}
	/**
	 * 根据( 渠道 ) 查询
	 */
	public static List<Yh> getByQd(String qd){
		List<Yh> yhs = new ArrayList<Yh>();
		String key = SK_Plus.b(qd).e();
		Set<String> keys = qdCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yh yh = null;
			for (String k : list) {
				yh = getById(Integer.valueOf(k));
				if (yh == null) continue;
					yhs.add(yh);
			}
		}else{
			yhs = YhDao.getByQd(qd);
			if(!yhs.isEmpty()){
				loadCaches(yhs);
			}
		}
		return yhs;
	}
	
	public static List<Yh> getByPageQd(String qd,int page,int pageCount){
		List<Yh> yhs = null;
		String key = SK_Plus.b(qd).e();
		Set<String> keys = qdCache.get(key);
		if(keys == null){
			yhs = getByQd(qd);
		}
		yhs = SK_List.getPage(yhs, page, pageCount);
		return yhs;
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Yh> getByLm_id(int lm_id){
		List<Yh> yhs = new ArrayList<Yh>();
		String key = SK_Plus.b(lm_id).e();
		Set<String> keys = lm_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yh yh = null;
			for (String k : list) {
				yh = getById(Integer.valueOf(k));
				if (yh == null) continue;
					yhs.add(yh);
			}
		}else{
			yhs = YhDao.getByLm_id(lm_id);
			if(!yhs.isEmpty()){
				loadCaches(yhs);
			}
		}
		return yhs;
	}
	
	public static List<Yh> getByPageLm_id(int lm_id,int page,int pageCount){
		List<Yh> yhs = null;
		String key = SK_Plus.b(lm_id).e();
		Set<String> keys = lm_idCache.get(key);
		if(keys == null){
			yhs = getByLm_id(lm_id);
		}
		yhs = SK_List.getPage(yhs, page, pageCount);
		return yhs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yh> yhs){
		for(Yh yh : yhs){
			loadCache(yh);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yh yh){
		idCache.put(SK_Plus.b(yh.getId()).e(),yh);
		ncCache.put(SK_Plus.b(yh.getNc()).e(),String.valueOf(yh.getId()));
		zhCache.put(SK_Plus.b(yh.getZh()).e(),String.valueOf(yh.getId()));
		zhmmCache.put(SK_Plus.b(yh.getZh(),yh.getMm()).e(),String.valueOf(yh.getId()));
		Set<String> yhlx_idset = yhlx_idCache.get(String.valueOf(SK_Plus.b(yh.getYhlx_id()).e()));
		if(yhlx_idset == null){
			yhlx_idset = new HashSet<String>();
		}
		yhlx_idset.add(String.valueOf(yh.getId()));
		yhlx_idCache.put(SK_Plus.b(yh.getYhlx_id()).e(),yhlx_idset);
		Set<String> fwq_idset = fwq_idCache.get(String.valueOf(SK_Plus.b(yh.getFwq_id()).e()));
		if(fwq_idset == null){
			fwq_idset = new HashSet<String>();
		}
		fwq_idset.add(String.valueOf(yh.getId()));
		fwq_idCache.put(SK_Plus.b(yh.getFwq_id()).e(),fwq_idset);
		Set<String> qdset = qdCache.get(String.valueOf(SK_Plus.b(yh.getQd()).e()));
		if(qdset == null){
			qdset = new HashSet<String>();
		}
		qdset.add(String.valueOf(yh.getId()));
		qdCache.put(SK_Plus.b(yh.getQd()).e(),qdset);
		Set<String> lm_idset = lm_idCache.get(String.valueOf(SK_Plus.b(yh.getLm_id()).e()));
		if(lm_idset == null){
			lm_idset = new HashSet<String>();
		}
		lm_idset.add(String.valueOf(yh.getId()));
		lm_idCache.put(SK_Plus.b(yh.getLm_id()).e(),lm_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yh yh){
		idCache.remove(SK_Plus.b(yh.getId()).e());
		ncCache.removeByValue(String.valueOf(yh.getId()));
		zhCache.removeByValue(String.valueOf(yh.getId()));
		zhmmCache.removeByValue(String.valueOf(yh.getId()));
		Set<String> yhlx_idset = yhlx_idCache.get(String.valueOf(SK_Plus.b(yh.getYhlx_id()).e()));
		if(yhlx_idset == null){
			yhlx_idset = new HashSet<String>();
		}
		if (yhlx_idset.contains(String.valueOf(yh.getId()))) {
			yhlx_idset.remove(String.valueOf(yh.getId()));
		}
		yhlx_idCache.put(SK_Plus.b(yh.getYhlx_id()).e(),yhlx_idset);
		Set<String> fwq_idset = fwq_idCache.get(String.valueOf(SK_Plus.b(yh.getFwq_id()).e()));
		if(fwq_idset == null){
			fwq_idset = new HashSet<String>();
		}
		if (fwq_idset.contains(String.valueOf(yh.getId()))) {
			fwq_idset.remove(String.valueOf(yh.getId()));
		}
		fwq_idCache.put(SK_Plus.b(yh.getFwq_id()).e(),fwq_idset);
		Set<String> qdset = qdCache.get(String.valueOf(SK_Plus.b(yh.getQd()).e()));
		if(qdset == null){
			qdset = new HashSet<String>();
		}
		if (qdset.contains(String.valueOf(yh.getId()))) {
			qdset.remove(String.valueOf(yh.getId()));
		}
		qdCache.put(SK_Plus.b(yh.getQd()).e(),qdset);
		Set<String> lm_idset = lm_idCache.get(String.valueOf(SK_Plus.b(yh.getLm_id()).e()));
		if(lm_idset == null){
			lm_idset = new HashSet<String>();
		}
		if (lm_idset.contains(String.valueOf(yh.getId()))) {
			lm_idset.remove(String.valueOf(yh.getId()));
		}
		lm_idCache.put(SK_Plus.b(yh.getLm_id()).e(),lm_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yh> yhs){
		for(Yh yh : yhs){
			clearCache(yh);
		}
	}
	
	public static Yh insert(Yh yh){
		int id = LASTID.get();
    	if(id < 1){
    		yh = YhDao.insert(yh);
    		LASTID.set(yh.getId());
    	}else{
    		yh.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yh);
    	return yh;
    }
    
    public static Yh update(Yh yh){
    	clearCache(yh);
    	loadCache(yh);
    	//加入定时器
    	return yh;
    }
    
    public static boolean delete(Yh yh){
    	clearCache(yh);
    	//加入定时器
    	return false;
    }
    
    public static Yh updateAndFlush(Yh yh){
    	update(yh);
    	return YhDao.update(yh);
    }
    
    public static Yh insertAndFlush(Yh yh){
    	int id = LASTID.get();
    	insert(yh);
    	if(id > 0){
    		yh = YhDao.insert(yh);
    	}
    	return yh;
    }
    
    public static boolean deleteAndFlush(Yh yh){
    	delete(yh);
    	return YhDao.delete(yh);
    }
}