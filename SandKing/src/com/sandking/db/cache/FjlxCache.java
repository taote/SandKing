package com.sandking.db.cache;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.List;
import java.util.Hashtable;
import com.sandking.db.dao.FjlxDao;
import com.sandking.db.bean.Fjlx;
public class FjlxCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Fjlx> idCache = new Hashtable<String, Fjlx>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Fjlx getById(int id){
		Fjlx fjlx = null;
		String key = SK_Plus.b(id).e();
		fjlx = idCache.get(key);
		
		if(fjlx==null){
			//查询数据库
			fjlx = FjlxDao.getById(id);
			if(fjlx!=null){
				//加入缓存
				loadCache(fjlx);
			}
		}
		return fjlx;
	}
	
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Fjlx> fjlxs){
		for(Fjlx fjlx : fjlxs){
			loadCache(fjlx);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Fjlx fjlx){
		idCache.put(SK_Plus.b(fjlx.getId()).e(),fjlx);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Fjlx fjlx){
		idCache.remove(SK_Plus.b(fjlx.getId()).e());
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Fjlx> fjlxs){
		for(Fjlx fjlx : fjlxs){
			clearCache(fjlx);
		}
	}
	
	public static Fjlx insert(Fjlx fjlx){
		int id = LASTID.get();
    	if(id < 1){
    		fjlx = FjlxDao.insert(fjlx);
    		LASTID.set(fjlx.getId());
    	}else{
    		fjlx.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(fjlx);
    	return fjlx;
    }
    
    public static Fjlx update(Fjlx fjlx){
    	clearCache(fjlx);
    	loadCache(fjlx);
    	//加入定时器
    	return fjlx;
    }
    
    public static boolean delete(Fjlx fjlx){
    	clearCache(fjlx);
    	//加入定时器
    	return false;
    }
    
    public static Fjlx updateAndFlush(Fjlx fjlx){
    	update(fjlx);
    	return FjlxDao.update(fjlx);
    }
    
    public static Fjlx insertAndFlush(Fjlx fjlx){
    	int id = LASTID.get();
    	insert(fjlx);
    	if(id > 0){
    		fjlx = FjlxDao.insert(fjlx);
    	}
    	return fjlx;
    }
    
    public static boolean deleteAndFlush(Fjlx fjlx){
    	delete(fjlx);
    	return FjlxDao.delete(fjlx);
    }
}