package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import com.sandking.db.bean.Yhys;
import java.util.Set;
import com.sandking.db.dao.YhysDao;
import com.sandking.tools.SK_List;
public class YhysCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yhys> idCache = new Hashtable<String, Yhys>();
	//列表关系
	static final Map<String, Set<String>> yh_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhys getById(int id){
		Yhys yhys = null;
		String key = SK_Plus.b(id).e();
		yhys = idCache.get(key);
		
		if(yhys==null){
			//查询数据库
			yhys = YhysDao.getById(id);
			if(yhys!=null){
				//加入缓存
				loadCache(yhys);
			}
		}
		return yhys;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhys> getByYh_id(int yh_id){
		List<Yhys> yhyss = new ArrayList<Yhys>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yhys yhys = null;
			for (String k : list) {
				yhys = getById(Integer.valueOf(k));
				if (yhys == null) continue;
					yhyss.add(yhys);
			}
		}else{
			yhyss = YhysDao.getByYh_id(yh_id);
			if(!yhyss.isEmpty()){
				loadCaches(yhyss);
			}
		}
		return yhyss;
	}
	
	public static List<Yhys> getByPageYh_id(int yh_id,int page,int pageCount){
		List<Yhys> yhyss = null;
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys == null){
			yhyss = getByYh_id(yh_id);
		}
		yhyss = SK_List.getPage(yhyss, page, pageCount);
		return yhyss;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhys> yhyss){
		for(Yhys yhys : yhyss){
			loadCache(yhys);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhys yhys){
		idCache.put(SK_Plus.b(yhys.getId()).e(),yhys);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhys.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhys.getId()));
		yh_idCache.put(SK_Plus.b(yhys.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yhys yhys){
		idCache.remove(SK_Plus.b(yhys.getId()).e());
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhys.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhys.getId()))) {
			yh_idset.remove(String.valueOf(yhys.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhys.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yhys> yhyss){
		for(Yhys yhys : yhyss){
			clearCache(yhys);
		}
	}
	
	public static Yhys insert(Yhys yhys){
		int id = LASTID.get();
    	if(id < 1){
    		yhys = YhysDao.insert(yhys);
    		LASTID.set(yhys.getId());
    	}else{
    		yhys.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yhys);
    	return yhys;
    }
    
    public static Yhys update(Yhys yhys){
    	clearCache(yhys);
    	loadCache(yhys);
    	//加入定时器
    	return yhys;
    }
    
    public static boolean delete(Yhys yhys){
    	clearCache(yhys);
    	//加入定时器
    	return false;
    }
    
    public static Yhys updateAndFlush(Yhys yhys){
    	update(yhys);
    	return YhysDao.update(yhys);
    }
    
    public static Yhys insertAndFlush(Yhys yhys){
    	int id = LASTID.get();
    	insert(yhys);
    	if(id > 0){
    		yhys = YhysDao.insert(yhys);
    	}
    	return yhys;
    }
    
    public static boolean deleteAndFlush(Yhys yhys){
    	delete(yhys);
    	return YhysDao.delete(yhys);
    }
}