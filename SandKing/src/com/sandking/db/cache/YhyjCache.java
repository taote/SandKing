package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.bean.Yhyj;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.db.dao.YhyjDao;
import com.sandking.tools.SK_List;
public class YhyjCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yhyj> idCache = new Hashtable<String, Yhyj>();
	//列表关系
	static final Map<String, Set<String>> fjridCache = new Hashtable<String, Set<String>>();
	//列表关系
	static final Map<String, Set<String>> jsridCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhyj getById(int id){
		Yhyj yhyj = null;
		String key = SK_Plus.b(id).e();
		yhyj = idCache.get(key);
		
		if(yhyj==null){
			//查询数据库
			yhyj = YhyjDao.getById(id);
			if(yhyj!=null){
				//加入缓存
				loadCache(yhyj);
			}
		}
		return yhyj;
	}
	
	/**
	 * 根据( 发件人id ) 查询
	 */
	public static List<Yhyj> getByFjrid(int fjrid){
		List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		String key = SK_Plus.b(fjrid).e();
		Set<String> keys = fjridCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yhyj yhyj = null;
			for (String k : list) {
				yhyj = getById(Integer.valueOf(k));
				if (yhyj == null) continue;
					yhyjs.add(yhyj);
			}
		}else{
			yhyjs = YhyjDao.getByFjrid(fjrid);
			if(!yhyjs.isEmpty()){
				loadCaches(yhyjs);
			}
		}
		return yhyjs;
	}
	
	public static List<Yhyj> getByPageFjrid(int fjrid,int page,int pageCount){
		List<Yhyj> yhyjs = null;
		String key = SK_Plus.b(fjrid).e();
		Set<String> keys = fjridCache.get(key);
		if(keys == null){
			yhyjs = getByFjrid(fjrid);
		}
		yhyjs = SK_List.getPage(yhyjs, page, pageCount);
		return yhyjs;
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Yhyj> getByJsrid(int jsrid){
		List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		String key = SK_Plus.b(jsrid).e();
		Set<String> keys = jsridCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yhyj yhyj = null;
			for (String k : list) {
				yhyj = getById(Integer.valueOf(k));
				if (yhyj == null) continue;
					yhyjs.add(yhyj);
			}
		}else{
			yhyjs = YhyjDao.getByJsrid(jsrid);
			if(!yhyjs.isEmpty()){
				loadCaches(yhyjs);
			}
		}
		return yhyjs;
	}
	
	public static List<Yhyj> getByPageJsrid(int jsrid,int page,int pageCount){
		List<Yhyj> yhyjs = null;
		String key = SK_Plus.b(jsrid).e();
		Set<String> keys = jsridCache.get(key);
		if(keys == null){
			yhyjs = getByJsrid(jsrid);
		}
		yhyjs = SK_List.getPage(yhyjs, page, pageCount);
		return yhyjs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhyj> yhyjs){
		for(Yhyj yhyj : yhyjs){
			loadCache(yhyj);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhyj yhyj){
		idCache.put(SK_Plus.b(yhyj.getId()).e(),yhyj);
		Set<String> fjridset = fjridCache.get(String.valueOf(SK_Plus.b(yhyj.getFjrid()).e()));
		if(fjridset == null){
			fjridset = new HashSet<String>();
		}
		fjridset.add(String.valueOf(yhyj.getId()));
		fjridCache.put(SK_Plus.b(yhyj.getFjrid()).e(),fjridset);
		Set<String> jsridset = jsridCache.get(String.valueOf(SK_Plus.b(yhyj.getJsrid()).e()));
		if(jsridset == null){
			jsridset = new HashSet<String>();
		}
		jsridset.add(String.valueOf(yhyj.getId()));
		jsridCache.put(SK_Plus.b(yhyj.getJsrid()).e(),jsridset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yhyj yhyj){
		idCache.remove(SK_Plus.b(yhyj.getId()).e());
		Set<String> fjridset = fjridCache.get(String.valueOf(SK_Plus.b(yhyj.getFjrid()).e()));
		if(fjridset == null){
			fjridset = new HashSet<String>();
		}
		if (fjridset.contains(String.valueOf(yhyj.getId()))) {
			fjridset.remove(String.valueOf(yhyj.getId()));
		}
		fjridCache.put(SK_Plus.b(yhyj.getFjrid()).e(),fjridset);
		Set<String> jsridset = jsridCache.get(String.valueOf(SK_Plus.b(yhyj.getJsrid()).e()));
		if(jsridset == null){
			jsridset = new HashSet<String>();
		}
		if (jsridset.contains(String.valueOf(yhyj.getId()))) {
			jsridset.remove(String.valueOf(yhyj.getId()));
		}
		jsridCache.put(SK_Plus.b(yhyj.getJsrid()).e(),jsridset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yhyj> yhyjs){
		for(Yhyj yhyj : yhyjs){
			clearCache(yhyj);
		}
	}
	
	public static Yhyj insert(Yhyj yhyj){
		int id = LASTID.get();
    	if(id < 1){
    		yhyj = YhyjDao.insert(yhyj);
    		LASTID.set(yhyj.getId());
    	}else{
    		yhyj.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yhyj);
    	return yhyj;
    }
    
    public static Yhyj update(Yhyj yhyj){
    	clearCache(yhyj);
    	loadCache(yhyj);
    	//加入定时器
    	return yhyj;
    }
    
    public static boolean delete(Yhyj yhyj){
    	clearCache(yhyj);
    	//加入定时器
    	return false;
    }
    
    public static Yhyj updateAndFlush(Yhyj yhyj){
    	update(yhyj);
    	return YhyjDao.update(yhyj);
    }
    
    public static Yhyj insertAndFlush(Yhyj yhyj){
    	int id = LASTID.get();
    	insert(yhyj);
    	if(id > 0){
    		yhyj = YhyjDao.insert(yhyj);
    	}
    	return yhyj;
    }
    
    public static boolean deleteAndFlush(Yhyj yhyj){
    	delete(yhyj);
    	return YhyjDao.delete(yhyj);
    }
}