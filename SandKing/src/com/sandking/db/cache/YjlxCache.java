package com.sandking.db.cache;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.List;
import java.util.Hashtable;
import com.sandking.db.dao.YjlxDao;
import com.sandking.db.bean.Yjlx;
public class YjlxCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yjlx> idCache = new Hashtable<String, Yjlx>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yjlx getById(int id){
		Yjlx yjlx = null;
		String key = SK_Plus.b(id).e();
		yjlx = idCache.get(key);
		
		if(yjlx==null){
			//查询数据库
			yjlx = YjlxDao.getById(id);
			if(yjlx!=null){
				//加入缓存
				loadCache(yjlx);
			}
		}
		return yjlx;
	}
	
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yjlx> yjlxs){
		for(Yjlx yjlx : yjlxs){
			loadCache(yjlx);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yjlx yjlx){
		idCache.put(SK_Plus.b(yjlx.getId()).e(),yjlx);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yjlx yjlx){
		idCache.remove(SK_Plus.b(yjlx.getId()).e());
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yjlx> yjlxs){
		for(Yjlx yjlx : yjlxs){
			clearCache(yjlx);
		}
	}
	
	public static Yjlx insert(Yjlx yjlx){
		int id = LASTID.get();
    	if(id < 1){
    		yjlx = YjlxDao.insert(yjlx);
    		LASTID.set(yjlx.getId());
    	}else{
    		yjlx.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yjlx);
    	return yjlx;
    }
    
    public static Yjlx update(Yjlx yjlx){
    	clearCache(yjlx);
    	loadCache(yjlx);
    	//加入定时器
    	return yjlx;
    }
    
    public static boolean delete(Yjlx yjlx){
    	clearCache(yjlx);
    	//加入定时器
    	return false;
    }
    
    public static Yjlx updateAndFlush(Yjlx yjlx){
    	update(yjlx);
    	return YjlxDao.update(yjlx);
    }
    
    public static Yjlx insertAndFlush(Yjlx yjlx){
    	int id = LASTID.get();
    	insert(yjlx);
    	if(id > 0){
    		yjlx = YjlxDao.insert(yjlx);
    	}
    	return yjlx;
    }
    
    public static boolean deleteAndFlush(Yjlx yjlx){
    	delete(yjlx);
    	return YjlxDao.delete(yjlx);
    }
}