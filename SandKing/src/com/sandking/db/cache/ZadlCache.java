package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.dao.ZadlDao;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import com.sandking.db.bean.Zadl;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.tools.SK_List;
public class ZadlCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Zadl> idCache = new Hashtable<String, Zadl>();
	//列表关系
	static final Map<String, Set<String>> yhza_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Zadl getById(int id){
		Zadl zadl = null;
		String key = SK_Plus.b(id).e();
		zadl = idCache.get(key);
		
		if(zadl==null){
			//查询数据库
			zadl = ZadlDao.getById(id);
			if(zadl!=null){
				//加入缓存
				loadCache(zadl);
			}
		}
		return zadl;
	}
	
	/**
	 * 根据( 用户障碍_id ) 查询
	 */
	public static List<Zadl> getByYhza_id(int yhza_id){
		List<Zadl> zadls = new ArrayList<Zadl>();
		String key = SK_Plus.b(yhza_id).e();
		Set<String> keys = yhza_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Zadl zadl = null;
			for (String k : list) {
				zadl = getById(Integer.valueOf(k));
				if (zadl == null) continue;
					zadls.add(zadl);
			}
		}else{
			zadls = ZadlDao.getByYhza_id(yhza_id);
			if(!zadls.isEmpty()){
				loadCaches(zadls);
			}
		}
		return zadls;
	}
	
	public static List<Zadl> getByPageYhza_id(int yhza_id,int page,int pageCount){
		List<Zadl> zadls = null;
		String key = SK_Plus.b(yhza_id).e();
		Set<String> keys = yhza_idCache.get(key);
		if(keys == null){
			zadls = getByYhza_id(yhza_id);
		}
		zadls = SK_List.getPage(zadls, page, pageCount);
		return zadls;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Zadl> zadls){
		for(Zadl zadl : zadls){
			loadCache(zadl);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Zadl zadl){
		idCache.put(SK_Plus.b(zadl.getId()).e(),zadl);
		Set<String> yhza_idset = yhza_idCache.get(String.valueOf(SK_Plus.b(zadl.getYhza_id()).e()));
		if(yhza_idset == null){
			yhza_idset = new HashSet<String>();
		}
		yhza_idset.add(String.valueOf(zadl.getId()));
		yhza_idCache.put(SK_Plus.b(zadl.getYhza_id()).e(),yhza_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Zadl zadl){
		idCache.remove(SK_Plus.b(zadl.getId()).e());
		Set<String> yhza_idset = yhza_idCache.get(String.valueOf(SK_Plus.b(zadl.getYhza_id()).e()));
		if(yhza_idset == null){
			yhza_idset = new HashSet<String>();
		}
		if (yhza_idset.contains(String.valueOf(zadl.getId()))) {
			yhza_idset.remove(String.valueOf(zadl.getId()));
		}
		yhza_idCache.put(SK_Plus.b(zadl.getYhza_id()).e(),yhza_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Zadl> zadls){
		for(Zadl zadl : zadls){
			clearCache(zadl);
		}
	}
	
	public static Zadl insert(Zadl zadl){
		int id = LASTID.get();
    	if(id < 1){
    		zadl = ZadlDao.insert(zadl);
    		LASTID.set(zadl.getId());
    	}else{
    		zadl.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(zadl);
    	return zadl;
    }
    
    public static Zadl update(Zadl zadl){
    	clearCache(zadl);
    	loadCache(zadl);
    	//加入定时器
    	return zadl;
    }
    
    public static boolean delete(Zadl zadl){
    	clearCache(zadl);
    	//加入定时器
    	return false;
    }
    
    public static Zadl updateAndFlush(Zadl zadl){
    	update(zadl);
    	return ZadlDao.update(zadl);
    }
    
    public static Zadl insertAndFlush(Zadl zadl){
    	int id = LASTID.get();
    	insert(zadl);
    	if(id > 0){
    		zadl = ZadlDao.insert(zadl);
    	}
    	return zadl;
    }
    
    public static boolean deleteAndFlush(Zadl zadl){
    	delete(zadl);
    	return ZadlDao.delete(zadl);
    }
}