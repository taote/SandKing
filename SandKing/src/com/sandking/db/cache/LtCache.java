package com.sandking.db.cache;

import java.util.ArrayList;
import com.sandking.db.bean.Lt;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import com.sandking.db.dao.LtDao;
import java.util.Set;
import com.sandking.tools.SK_List;
public class LtCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Lt> idCache = new Hashtable<String, Lt>();
	//列表关系
	static final Map<String, Set<String>> ltlx_idCache = new Hashtable<String, Set<String>>();
	//列表关系
	static final Map<String, Set<String>> lm_idCache = new Hashtable<String, Set<String>>();
	//列表关系
	static final Map<String, Set<String>> jsridCache = new Hashtable<String, Set<String>>();
	//列表关系
	static final Map<String, Set<String>> fyridCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Lt getById(int id){
		Lt lt = null;
		String key = SK_Plus.b(id).e();
		lt = idCache.get(key);
		
		if(lt==null){
			//查询数据库
			lt = LtDao.getById(id);
			if(lt!=null){
				//加入缓存
				loadCache(lt);
			}
		}
		return lt;
	}
	
	/**
	 * 根据( 聊天类型_id ) 查询
	 */
	public static List<Lt> getByLtlx_id(int ltlx_id){
		List<Lt> lts = new ArrayList<Lt>();
		String key = SK_Plus.b(ltlx_id).e();
		Set<String> keys = ltlx_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Lt lt = null;
			for (String k : list) {
				lt = getById(Integer.valueOf(k));
				if (lt == null) continue;
					lts.add(lt);
			}
		}else{
			lts = LtDao.getByLtlx_id(ltlx_id);
			if(!lts.isEmpty()){
				loadCaches(lts);
			}
		}
		return lts;
	}
	
	public static List<Lt> getByPageLtlx_id(int ltlx_id,int page,int pageCount){
		List<Lt> lts = null;
		String key = SK_Plus.b(ltlx_id).e();
		Set<String> keys = ltlx_idCache.get(key);
		if(keys == null){
			lts = getByLtlx_id(ltlx_id);
		}
		lts = SK_List.getPage(lts, page, pageCount);
		return lts;
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Lt> getByLm_id(int lm_id){
		List<Lt> lts = new ArrayList<Lt>();
		String key = SK_Plus.b(lm_id).e();
		Set<String> keys = lm_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Lt lt = null;
			for (String k : list) {
				lt = getById(Integer.valueOf(k));
				if (lt == null) continue;
					lts.add(lt);
			}
		}else{
			lts = LtDao.getByLm_id(lm_id);
			if(!lts.isEmpty()){
				loadCaches(lts);
			}
		}
		return lts;
	}
	
	public static List<Lt> getByPageLm_id(int lm_id,int page,int pageCount){
		List<Lt> lts = null;
		String key = SK_Plus.b(lm_id).e();
		Set<String> keys = lm_idCache.get(key);
		if(keys == null){
			lts = getByLm_id(lm_id);
		}
		lts = SK_List.getPage(lts, page, pageCount);
		return lts;
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Lt> getByJsrid(int jsrid){
		List<Lt> lts = new ArrayList<Lt>();
		String key = SK_Plus.b(jsrid).e();
		Set<String> keys = jsridCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Lt lt = null;
			for (String k : list) {
				lt = getById(Integer.valueOf(k));
				if (lt == null) continue;
					lts.add(lt);
			}
		}else{
			lts = LtDao.getByJsrid(jsrid);
			if(!lts.isEmpty()){
				loadCaches(lts);
			}
		}
		return lts;
	}
	
	public static List<Lt> getByPageJsrid(int jsrid,int page,int pageCount){
		List<Lt> lts = null;
		String key = SK_Plus.b(jsrid).e();
		Set<String> keys = jsridCache.get(key);
		if(keys == null){
			lts = getByJsrid(jsrid);
		}
		lts = SK_List.getPage(lts, page, pageCount);
		return lts;
	}
	/**
	 * 根据( 发言人id ) 查询
	 */
	public static List<Lt> getByFyrid(int fyrid){
		List<Lt> lts = new ArrayList<Lt>();
		String key = SK_Plus.b(fyrid).e();
		Set<String> keys = fyridCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Lt lt = null;
			for (String k : list) {
				lt = getById(Integer.valueOf(k));
				if (lt == null) continue;
					lts.add(lt);
			}
		}else{
			lts = LtDao.getByFyrid(fyrid);
			if(!lts.isEmpty()){
				loadCaches(lts);
			}
		}
		return lts;
	}
	
	public static List<Lt> getByPageFyrid(int fyrid,int page,int pageCount){
		List<Lt> lts = null;
		String key = SK_Plus.b(fyrid).e();
		Set<String> keys = fyridCache.get(key);
		if(keys == null){
			lts = getByFyrid(fyrid);
		}
		lts = SK_List.getPage(lts, page, pageCount);
		return lts;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Lt> lts){
		for(Lt lt : lts){
			loadCache(lt);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Lt lt){
		idCache.put(SK_Plus.b(lt.getId()).e(),lt);
		Set<String> ltlx_idset = ltlx_idCache.get(String.valueOf(SK_Plus.b(lt.getLtlx_id()).e()));
		if(ltlx_idset == null){
			ltlx_idset = new HashSet<String>();
		}
		ltlx_idset.add(String.valueOf(lt.getId()));
		ltlx_idCache.put(SK_Plus.b(lt.getLtlx_id()).e(),ltlx_idset);
		Set<String> lm_idset = lm_idCache.get(String.valueOf(SK_Plus.b(lt.getLm_id()).e()));
		if(lm_idset == null){
			lm_idset = new HashSet<String>();
		}
		lm_idset.add(String.valueOf(lt.getId()));
		lm_idCache.put(SK_Plus.b(lt.getLm_id()).e(),lm_idset);
		Set<String> jsridset = jsridCache.get(String.valueOf(SK_Plus.b(lt.getJsrid()).e()));
		if(jsridset == null){
			jsridset = new HashSet<String>();
		}
		jsridset.add(String.valueOf(lt.getId()));
		jsridCache.put(SK_Plus.b(lt.getJsrid()).e(),jsridset);
		Set<String> fyridset = fyridCache.get(String.valueOf(SK_Plus.b(lt.getFyrid()).e()));
		if(fyridset == null){
			fyridset = new HashSet<String>();
		}
		fyridset.add(String.valueOf(lt.getId()));
		fyridCache.put(SK_Plus.b(lt.getFyrid()).e(),fyridset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Lt lt){
		idCache.remove(SK_Plus.b(lt.getId()).e());
		Set<String> ltlx_idset = ltlx_idCache.get(String.valueOf(SK_Plus.b(lt.getLtlx_id()).e()));
		if(ltlx_idset == null){
			ltlx_idset = new HashSet<String>();
		}
		if (ltlx_idset.contains(String.valueOf(lt.getId()))) {
			ltlx_idset.remove(String.valueOf(lt.getId()));
		}
		ltlx_idCache.put(SK_Plus.b(lt.getLtlx_id()).e(),ltlx_idset);
		Set<String> lm_idset = lm_idCache.get(String.valueOf(SK_Plus.b(lt.getLm_id()).e()));
		if(lm_idset == null){
			lm_idset = new HashSet<String>();
		}
		if (lm_idset.contains(String.valueOf(lt.getId()))) {
			lm_idset.remove(String.valueOf(lt.getId()));
		}
		lm_idCache.put(SK_Plus.b(lt.getLm_id()).e(),lm_idset);
		Set<String> jsridset = jsridCache.get(String.valueOf(SK_Plus.b(lt.getJsrid()).e()));
		if(jsridset == null){
			jsridset = new HashSet<String>();
		}
		if (jsridset.contains(String.valueOf(lt.getId()))) {
			jsridset.remove(String.valueOf(lt.getId()));
		}
		jsridCache.put(SK_Plus.b(lt.getJsrid()).e(),jsridset);
		Set<String> fyridset = fyridCache.get(String.valueOf(SK_Plus.b(lt.getFyrid()).e()));
		if(fyridset == null){
			fyridset = new HashSet<String>();
		}
		if (fyridset.contains(String.valueOf(lt.getId()))) {
			fyridset.remove(String.valueOf(lt.getId()));
		}
		fyridCache.put(SK_Plus.b(lt.getFyrid()).e(),fyridset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Lt> lts){
		for(Lt lt : lts){
			clearCache(lt);
		}
	}
	
	public static Lt insert(Lt lt){
		int id = LASTID.get();
    	if(id < 1){
    		lt = LtDao.insert(lt);
    		LASTID.set(lt.getId());
    	}else{
    		lt.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(lt);
    	return lt;
    }
    
    public static Lt update(Lt lt){
    	clearCache(lt);
    	loadCache(lt);
    	//加入定时器
    	return lt;
    }
    
    public static boolean delete(Lt lt){
    	clearCache(lt);
    	//加入定时器
    	return false;
    }
    
    public static Lt updateAndFlush(Lt lt){
    	update(lt);
    	return LtDao.update(lt);
    }
    
    public static Lt insertAndFlush(Lt lt){
    	int id = LASTID.get();
    	insert(lt);
    	if(id > 0){
    		lt = LtDao.insert(lt);
    	}
    	return lt;
    }
    
    public static boolean deleteAndFlush(Lt lt){
    	delete(lt);
    	return LtDao.delete(lt);
    }
}