package com.sandking.db.cache;

import java.util.ArrayList;
import java.util.Map;
import com.sandking.db.dao.YhjzDao;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import java.util.Vector;
import com.sandking.db.bean.Yhjz;
import java.util.List;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import com.sandking.tools.SK_List;
public class YhjzCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Yhjz> idCache = new Hashtable<String, Yhjz>();
	//列表关系
	static final Map<String, Set<String>> yh_idCache = new Hashtable<String, Set<String>>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Yhjz getById(int id){
		Yhjz yhjz = null;
		String key = SK_Plus.b(id).e();
		yhjz = idCache.get(key);
		
		if(yhjz==null){
			//查询数据库
			yhjz = YhjzDao.getById(id);
			if(yhjz!=null){
				//加入缓存
				loadCache(yhjz);
			}
		}
		return yhjz;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhjz> getByYh_id(int yh_id){
		List<Yhjz> yhjzs = new ArrayList<Yhjz>();
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys != null){
			List<String> list = new Vector<String>(keys);
			Yhjz yhjz = null;
			for (String k : list) {
				yhjz = getById(Integer.valueOf(k));
				if (yhjz == null) continue;
					yhjzs.add(yhjz);
			}
		}else{
			yhjzs = YhjzDao.getByYh_id(yh_id);
			if(!yhjzs.isEmpty()){
				loadCaches(yhjzs);
			}
		}
		return yhjzs;
	}
	
	public static List<Yhjz> getByPageYh_id(int yh_id,int page,int pageCount){
		List<Yhjz> yhjzs = null;
		String key = SK_Plus.b(yh_id).e();
		Set<String> keys = yh_idCache.get(key);
		if(keys == null){
			yhjzs = getByYh_id(yh_id);
		}
		yhjzs = SK_List.getPage(yhjzs, page, pageCount);
		return yhjzs;
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Yhjz> yhjzs){
		for(Yhjz yhjz : yhjzs){
			loadCache(yhjz);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Yhjz yhjz){
		idCache.put(SK_Plus.b(yhjz.getId()).e(),yhjz);
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhjz.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		yh_idset.add(String.valueOf(yhjz.getId()));
		yh_idCache.put(SK_Plus.b(yhjz.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Yhjz yhjz){
		idCache.remove(SK_Plus.b(yhjz.getId()).e());
		Set<String> yh_idset = yh_idCache.get(String.valueOf(SK_Plus.b(yhjz.getYh_id()).e()));
		if(yh_idset == null){
			yh_idset = new HashSet<String>();
		}
		if (yh_idset.contains(String.valueOf(yhjz.getId()))) {
			yh_idset.remove(String.valueOf(yhjz.getId()));
		}
		yh_idCache.put(SK_Plus.b(yhjz.getYh_id()).e(),yh_idset);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Yhjz> yhjzs){
		for(Yhjz yhjz : yhjzs){
			clearCache(yhjz);
		}
	}
	
	public static Yhjz insert(Yhjz yhjz){
		int id = LASTID.get();
    	if(id < 1){
    		yhjz = YhjzDao.insert(yhjz);
    		LASTID.set(yhjz.getId());
    	}else{
    		yhjz.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(yhjz);
    	return yhjz;
    }
    
    public static Yhjz update(Yhjz yhjz){
    	clearCache(yhjz);
    	loadCache(yhjz);
    	//加入定时器
    	return yhjz;
    }
    
    public static boolean delete(Yhjz yhjz){
    	clearCache(yhjz);
    	//加入定时器
    	return false;
    }
    
    public static Yhjz updateAndFlush(Yhjz yhjz){
    	update(yhjz);
    	return YhjzDao.update(yhjz);
    }
    
    public static Yhjz insertAndFlush(Yhjz yhjz){
    	int id = LASTID.get();
    	insert(yhjz);
    	if(id > 0){
    		yhjz = YhjzDao.insert(yhjz);
    	}
    	return yhjz;
    }
    
    public static boolean deleteAndFlush(Yhjz yhjz){
    	delete(yhjz);
    	return YhjzDao.delete(yhjz);
    }
}