package com.sandking.db.cache;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import com.sandking.tools.SK_Plus;
import com.sandking.db.bean.Ltlx;
import java.util.List;
import com.sandking.db.dao.LtlxDao;
import java.util.Hashtable;
public class LtlxCache {
	private static AtomicInteger LASTID = new AtomicInteger();
	//缓存
	static final Map<String, Ltlx> idCache = new Hashtable<String, Ltlx>();
	
	/**
	 * 根据( id ) 查询
	 */
	public static Ltlx getById(int id){
		Ltlx ltlx = null;
		String key = SK_Plus.b(id).e();
		ltlx = idCache.get(key);
		
		if(ltlx==null){
			//查询数据库
			ltlx = LtlxDao.getById(id);
			if(ltlx!=null){
				//加入缓存
				loadCache(ltlx);
			}
		}
		return ltlx;
	}
	
	
	/**
	 * 加载缓存
	 */
	public static void loadCaches(List<Ltlx> ltlxs){
		for(Ltlx ltlx : ltlxs){
			loadCache(ltlx);
		}
	}
	
	/**
	 * 加载缓存
	 */
	public static void loadCache(Ltlx ltlx){
		idCache.put(SK_Plus.b(ltlx.getId()).e(),ltlx);
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCache(Ltlx ltlx){
		idCache.remove(SK_Plus.b(ltlx.getId()).e());
	}
	
	/**
	 * 清空缓存
	 */
	public static void clearCaches(List<Ltlx> ltlxs){
		for(Ltlx ltlx : ltlxs){
			clearCache(ltlx);
		}
	}
	
	public static Ltlx insert(Ltlx ltlx){
		int id = LASTID.get();
    	if(id < 1){
    		ltlx = LtlxDao.insert(ltlx);
    		LASTID.set(ltlx.getId());
    	}else{
    		ltlx.setId(LASTID.incrementAndGet());
    		//加入定时器
    	}
    	loadCache(ltlx);
    	return ltlx;
    }
    
    public static Ltlx update(Ltlx ltlx){
    	clearCache(ltlx);
    	loadCache(ltlx);
    	//加入定时器
    	return ltlx;
    }
    
    public static boolean delete(Ltlx ltlx){
    	clearCache(ltlx);
    	//加入定时器
    	return false;
    }
    
    public static Ltlx updateAndFlush(Ltlx ltlx){
    	update(ltlx);
    	return LtlxDao.update(ltlx);
    }
    
    public static Ltlx insertAndFlush(Ltlx ltlx){
    	int id = LASTID.get();
    	insert(ltlx);
    	if(id > 0){
    		ltlx = LtlxDao.insert(ltlx);
    	}
    	return ltlx;
    }
    
    public static boolean deleteAndFlush(Ltlx ltlx){
    	delete(ltlx);
    	return LtlxDao.delete(ltlx);
    }
}