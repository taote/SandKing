package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.db.bean.Ltlx;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;

public class LtlxDao {
	public static Ltlx insert(Ltlx ltlx){
		Connection conn = SK_Config.getConnection();
		return insert(ltlx,conn);
	}
	
	public static Ltlx insert(Ltlx ltlx,Connection conn){
		return insert(ltlx,conn,Ltlx.TABLENAME);
	}
	
	public static Ltlx insert(Ltlx ltlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(ltlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Ltlx insert(Ltlx ltlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(ltlx,conn,tableName);
	}
	
	public static Ltlx insert(Ltlx ltlx,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,名称) VALUES (?,?)";
		try {
			int i = (int)sq.insert(conn,sql,ltlx.getId(),ltlx.getMc());
			if(ltlx.getId()==0){
				ltlx.setId(i);
			}
			return i > 0 ? ltlx : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Ltlx insert(Ltlx ltlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(ltlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Ltlx> ltlxs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(ltlxs,conn);
	}
	
	public static int[] insertBatch(List<Ltlx> ltlxs,Connection conn){
		return insertBatch(ltlxs,conn,Ltlx.TABLENAME);
	}
	
	public static int[] insertBatch(List<Ltlx> ltlxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(ltlxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Ltlx> ltlxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(ltlxs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Ltlx> ltlxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,名称) VALUES (?,?)";
		try {
			int columnSize = 2;
			int size = ltlxs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =ltlxs.get(i).getId();
				params[i][1] =ltlxs.get(i).getMc();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Ltlx> ltlxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(ltlxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Ltlx update(Ltlx ltlx){
		Connection conn = SK_Config.getConnection();
		return update(ltlx,conn);
	}
	
	public static Ltlx update(Ltlx ltlx,Connection conn){
		return update(ltlx,conn,Ltlx.TABLENAME);
	}
	
	public static Ltlx update(Ltlx ltlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(ltlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Ltlx update(Ltlx ltlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(ltlx,conn,tableName);
	}
	
	public static Ltlx update(Ltlx ltlx,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = ltlx.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return ltlx;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = ltlx.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? ltlx : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				ltlx.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Ltlx update(Ltlx ltlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(ltlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Ltlx ltlx){
		Connection conn = SK_Config.getConnection();
		return delete(ltlx,conn);
	}
	
	public static boolean delete(Ltlx ltlx,Connection conn){
		return delete(ltlx,conn,Ltlx.TABLENAME);
	}
	
	public static boolean delete(Ltlx ltlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(ltlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Ltlx ltlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(ltlx,conn,tableName);
	}
	
	public static boolean delete(Ltlx ltlx,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, ltlx.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Ltlx ltlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(ltlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Ltlx> ltlxs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(ltlxs,conn);
	}
	
	public static boolean deleteBatch(List<Ltlx> ltlxs,Connection conn){
		return deleteBatch(ltlxs,conn,Ltlx.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Ltlx> ltlxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(ltlxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Ltlx> ltlxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(ltlxs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Ltlx> ltlxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = ltlxs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = ltlxs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Ltlx> ltlxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(ltlxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Ltlx getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Ltlx getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Ltlx getById(int id,Connection conn){
		return getById(id,conn,Ltlx.TABLENAME);
	}
	
	public static Ltlx getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName + " WHERE " + "id = ?";
		Ltlx ltlx = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			ltlx = Ltlx.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return ltlx;
	}
	
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Ltlx getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Ltlx getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public static List<Ltlx> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Ltlx> getAll(Connection conn){
		return getAll(conn,Ltlx.TABLENAME);
	}
	
	public static List<Ltlx> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Ltlx> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Ltlx> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName;
		List<Ltlx> ltlxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			ltlxs = Ltlx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return ltlxs;
	}
	
	public static List<Ltlx> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Ltlx> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Ltlx> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Ltlx.TABLENAME,page,pageCount);
	}
	
	public static List<Ltlx> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Ltlx> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Ltlx> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,名称 FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Ltlx> ltlxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			ltlxs = Ltlx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return ltlxs;
	}
	
	public static List<Ltlx> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}