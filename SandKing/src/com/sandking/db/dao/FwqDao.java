package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.db.bean.Fwq;

public class FwqDao {
	public static Fwq insert(Fwq fwq){
		Connection conn = SK_Config.getConnection();
		return insert(fwq,conn);
	}
	
	public static Fwq insert(Fwq fwq,Connection conn){
		return insert(fwq,conn,Fwq.TABLENAME);
	}
	
	public static Fwq insert(Fwq fwq,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(fwq,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Fwq insert(Fwq fwq,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(fwq,conn,tableName);
	}
	
	public static Fwq insert(Fwq fwq,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,名称,开服时间,关服时间,serverkey,顺序) VALUES (?,?,?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,fwq.getId(),fwq.getMc(),fwq.getKfsj(),fwq.getGfsj(),fwq.getServerkey(),fwq.getSx());
			if(fwq.getId()==0){
				fwq.setId(i);
			}
			return i > 0 ? fwq : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Fwq insert(Fwq fwq,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(fwq,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Fwq> fwqs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(fwqs,conn);
	}
	
	public static int[] insertBatch(List<Fwq> fwqs,Connection conn){
		return insertBatch(fwqs,conn,Fwq.TABLENAME);
	}
	
	public static int[] insertBatch(List<Fwq> fwqs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(fwqs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Fwq> fwqs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(fwqs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Fwq> fwqs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,名称,开服时间,关服时间,serverkey,顺序) VALUES (?,?,?,?,?,?)";
		try {
			int columnSize = 6;
			int size = fwqs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =fwqs.get(i).getId();
				params[i][1] =fwqs.get(i).getMc();
				params[i][2] =fwqs.get(i).getKfsj();
				params[i][3] =fwqs.get(i).getGfsj();
				params[i][4] =fwqs.get(i).getServerkey();
				params[i][5] =fwqs.get(i).getSx();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Fwq> fwqs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(fwqs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Fwq update(Fwq fwq){
		Connection conn = SK_Config.getConnection();
		return update(fwq,conn);
	}
	
	public static Fwq update(Fwq fwq,Connection conn){
		return update(fwq,conn,Fwq.TABLENAME);
	}
	
	public static Fwq update(Fwq fwq,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(fwq,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Fwq update(Fwq fwq,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(fwq,conn,tableName);
	}
	
	public static Fwq update(Fwq fwq,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = fwq.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return fwq;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = fwq.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? fwq : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				fwq.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Fwq update(Fwq fwq,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(fwq,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Fwq fwq){
		Connection conn = SK_Config.getConnection();
		return delete(fwq,conn);
	}
	
	public static boolean delete(Fwq fwq,Connection conn){
		return delete(fwq,conn,Fwq.TABLENAME);
	}
	
	public static boolean delete(Fwq fwq,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(fwq,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Fwq fwq,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(fwq,conn,tableName);
	}
	
	public static boolean delete(Fwq fwq,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, fwq.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Fwq fwq,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(fwq,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Fwq> fwqs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(fwqs,conn);
	}
	
	public static boolean deleteBatch(List<Fwq> fwqs,Connection conn){
		return deleteBatch(fwqs,conn,Fwq.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Fwq> fwqs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(fwqs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Fwq> fwqs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(fwqs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Fwq> fwqs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = fwqs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = fwqs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Fwq> fwqs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(fwqs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Fwq getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Fwq getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Fwq getById(int id,Connection conn){
		return getById(id,conn,Fwq.TABLENAME);
	}
	
	public static Fwq getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称,开服时间,关服时间,serverkey,顺序 FROM " + tableName + " WHERE " + "id = ?";
		Fwq fwq = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			fwq = Fwq.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return fwq;
	}
	
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Fwq getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Fwq getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public static List<Fwq> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Fwq> getAll(Connection conn){
		return getAll(conn,Fwq.TABLENAME);
	}
	
	public static List<Fwq> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Fwq> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Fwq> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称,开服时间,关服时间,serverkey,顺序 FROM " + tableName;
		List<Fwq> fwqs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			fwqs = Fwq.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return fwqs;
	}
	
	public static List<Fwq> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Fwq> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Fwq> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Fwq.TABLENAME,page,pageCount);
	}
	
	public static List<Fwq> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Fwq> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Fwq> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,名称,开服时间,关服时间,serverkey,顺序 FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Fwq> fwqs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			fwqs = Fwq.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return fwqs;
	}
	
	public static List<Fwq> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}