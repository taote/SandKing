package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.db.bean.Yhzs;

public class YhzsDao {
	public static Yhzs insert(Yhzs yhzs){
		Connection conn = SK_Config.getConnection();
		return insert(yhzs,conn);
	}
	
	public static Yhzs insert(Yhzs yhzs,Connection conn){
		return insert(yhzs,conn,Yhzs.TABLENAME);
	}
	
	public static Yhzs insert(Yhzs yhzs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yhzs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhzs insert(Yhzs yhzs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yhzs,conn,tableName);
	}
	
	public static Yhzs insert(Yhzs yhzs,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,装饰id,x,y,用户_id) VALUES (?,?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yhzs.getId(),yhzs.getZsid(),yhzs.getX(),yhzs.getY(),yhzs.getYh_id());
			if(yhzs.getId()==0){
				yhzs.setId(i);
			}
			return i > 0 ? yhzs : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhzs insert(Yhzs yhzs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yhzs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yhzs> yhzss){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhzss,conn);
	}
	
	public static int[] insertBatch(List<Yhzs> yhzss,Connection conn){
		return insertBatch(yhzss,conn,Yhzs.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yhzs> yhzss,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhzss,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yhzs> yhzss,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhzss,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yhzs> yhzss,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,装饰id,x,y,用户_id) VALUES (?,?,?,?,?)";
		try {
			int columnSize = 5;
			int size = yhzss.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhzss.get(i).getId();
				params[i][1] =yhzss.get(i).getZsid();
				params[i][2] =yhzss.get(i).getX();
				params[i][3] =yhzss.get(i).getY();
				params[i][4] =yhzss.get(i).getYh_id();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yhzs> yhzss,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhzss,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yhzs update(Yhzs yhzs){
		Connection conn = SK_Config.getConnection();
		return update(yhzs,conn);
	}
	
	public static Yhzs update(Yhzs yhzs,Connection conn){
		return update(yhzs,conn,Yhzs.TABLENAME);
	}
	
	public static Yhzs update(Yhzs yhzs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yhzs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhzs update(Yhzs yhzs,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yhzs,conn,tableName);
	}
	
	public static Yhzs update(Yhzs yhzs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yhzs.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yhzs;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yhzs.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yhzs : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yhzs.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhzs update(Yhzs yhzs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yhzs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Yhzs yhzs){
		Connection conn = SK_Config.getConnection();
		return delete(yhzs,conn);
	}
	
	public static boolean delete(Yhzs yhzs,Connection conn){
		return delete(yhzs,conn,Yhzs.TABLENAME);
	}
	
	public static boolean delete(Yhzs yhzs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yhzs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yhzs yhzs,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yhzs,conn,tableName);
	}
	
	public static boolean delete(Yhzs yhzs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yhzs.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yhzs yhzs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yhzs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yhzs> yhzss){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhzss,conn);
	}
	
	public static boolean deleteBatch(List<Yhzs> yhzss,Connection conn){
		return deleteBatch(yhzss,conn,Yhzs.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yhzs> yhzss,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhzss,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yhzs> yhzss,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhzss,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yhzs> yhzss,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yhzss.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yhzss.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yhzs> yhzss,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhzss,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yhzs getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yhzs getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhzs> getByYh_id(int yh_id){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn);
	}
	
	public static List<Yhzs> getByYh_id(int yh_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYh_id(yh_id, conn,tableName);
	}
	
	public static List<Yhzs> getByPageYh_id(int yh_id,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,page,pageCount);
	}
	
	public static List<Yhzs> getByPageYh_id(int yh_id,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageYh_id(yh_id, conn,tableName,page,pageCount);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yhzs getById(int id,Connection conn){
		return getById(id,conn,Yhzs.TABLENAME);
	}
	
	public static Yhzs getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,装饰id,x,y,用户_id FROM " + tableName + " WHERE " + "id = ?";
		Yhzs yhzs = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yhzs = Yhzs.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhzs;
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhzs> getByYh_id(int yh_id,Connection conn){
		return getByYh_id(yh_id,conn,Yhzs.TABLENAME);
	}
	
	public static List<Yhzs> getByYh_id(int yh_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,装饰id,x,y,用户_id FROM " + tableName + " WHERE " + "yh_id = ?";
		List<Yhzs> yhzss = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhzss = Yhzs.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhzss;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yhzs> getByPageYh_id(int yh_id,Connection conn,int page,int pageCount){
		return getByPageYh_id(yh_id,conn,Yhzs.TABLENAME,page,pageCount);
	}
	
	public static List<Yhzs> getByPageYh_id(int yh_id,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,装饰id,x,y,用户_id FROM " + tableName + " WHERE " + "yh_id = ? LIMIT " + page + " , " +pageCount;
		List<Yhzs> yhzss = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yh_id);
			yhzss = Yhzs.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhzss;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yhzs getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhzs getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户_id ) 查询
	 */
	public static List<Yhzs> getByYh_id(int yh_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhzs> getByYh_id(int yh_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYh_id(yh_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yhzs> getByPageYh_id(int yh_id,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhzs> getByPageYh_id(int yh_id,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageYh_id(yh_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Yhzs> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yhzs> getAll(Connection conn){
		return getAll(conn,Yhzs.TABLENAME);
	}
	
	public static List<Yhzs> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhzs> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yhzs> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,装饰id,x,y,用户_id FROM " + tableName;
		List<Yhzs> yhzss = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhzss = Yhzs.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhzss;
	}
	
	public static List<Yhzs> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yhzs> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Yhzs> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Yhzs.TABLENAME,page,pageCount);
	}
	
	public static List<Yhzs> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhzs> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Yhzs> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,装饰id,x,y,用户_id FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Yhzs> yhzss = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhzss = Yhzs.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhzss;
	}
	
	public static List<Yhzs> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}