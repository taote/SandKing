package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.db.bean.Yjlx;

public class YjlxDao {
	public static Yjlx insert(Yjlx yjlx){
		Connection conn = SK_Config.getConnection();
		return insert(yjlx,conn);
	}
	
	public static Yjlx insert(Yjlx yjlx,Connection conn){
		return insert(yjlx,conn,Yjlx.TABLENAME);
	}
	
	public static Yjlx insert(Yjlx yjlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yjlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yjlx insert(Yjlx yjlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yjlx,conn,tableName);
	}
	
	public static Yjlx insert(Yjlx yjlx,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,名称) VALUES (?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yjlx.getId(),yjlx.getMc());
			if(yjlx.getId()==0){
				yjlx.setId(i);
			}
			return i > 0 ? yjlx : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yjlx insert(Yjlx yjlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yjlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yjlx> yjlxs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yjlxs,conn);
	}
	
	public static int[] insertBatch(List<Yjlx> yjlxs,Connection conn){
		return insertBatch(yjlxs,conn,Yjlx.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yjlx> yjlxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yjlxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yjlx> yjlxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yjlxs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yjlx> yjlxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,名称) VALUES (?,?)";
		try {
			int columnSize = 2;
			int size = yjlxs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yjlxs.get(i).getId();
				params[i][1] =yjlxs.get(i).getMc();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yjlx> yjlxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yjlxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yjlx update(Yjlx yjlx){
		Connection conn = SK_Config.getConnection();
		return update(yjlx,conn);
	}
	
	public static Yjlx update(Yjlx yjlx,Connection conn){
		return update(yjlx,conn,Yjlx.TABLENAME);
	}
	
	public static Yjlx update(Yjlx yjlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yjlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yjlx update(Yjlx yjlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yjlx,conn,tableName);
	}
	
	public static Yjlx update(Yjlx yjlx,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yjlx.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yjlx;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yjlx.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yjlx : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yjlx.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yjlx update(Yjlx yjlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yjlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Yjlx yjlx){
		Connection conn = SK_Config.getConnection();
		return delete(yjlx,conn);
	}
	
	public static boolean delete(Yjlx yjlx,Connection conn){
		return delete(yjlx,conn,Yjlx.TABLENAME);
	}
	
	public static boolean delete(Yjlx yjlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yjlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yjlx yjlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yjlx,conn,tableName);
	}
	
	public static boolean delete(Yjlx yjlx,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yjlx.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yjlx yjlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yjlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yjlx> yjlxs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yjlxs,conn);
	}
	
	public static boolean deleteBatch(List<Yjlx> yjlxs,Connection conn){
		return deleteBatch(yjlxs,conn,Yjlx.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yjlx> yjlxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yjlxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yjlx> yjlxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yjlxs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yjlx> yjlxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yjlxs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yjlxs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yjlx> yjlxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yjlxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yjlx getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yjlx getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yjlx getById(int id,Connection conn){
		return getById(id,conn,Yjlx.TABLENAME);
	}
	
	public static Yjlx getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName + " WHERE " + "id = ?";
		Yjlx yjlx = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yjlx = Yjlx.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjlx;
	}
	
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yjlx getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yjlx getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public static List<Yjlx> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yjlx> getAll(Connection conn){
		return getAll(conn,Yjlx.TABLENAME);
	}
	
	public static List<Yjlx> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yjlx> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yjlx> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName;
		List<Yjlx> yjlxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yjlxs = Yjlx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjlxs;
	}
	
	public static List<Yjlx> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yjlx> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Yjlx> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Yjlx.TABLENAME,page,pageCount);
	}
	
	public static List<Yjlx> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yjlx> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Yjlx> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,名称 FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Yjlx> yjlxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yjlxs = Yjlx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yjlxs;
	}
	
	public static List<Yjlx> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}