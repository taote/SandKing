package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.db.bean.Yxzt;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;

public class YxztDao {
	public static Yxzt insert(Yxzt yxzt){
		Connection conn = SK_Config.getConnection();
		return insert(yxzt,conn);
	}
	
	public static Yxzt insert(Yxzt yxzt,Connection conn){
		return insert(yxzt,conn,Yxzt.TABLENAME);
	}
	
	public static Yxzt insert(Yxzt yxzt,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yxzt,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yxzt insert(Yxzt yxzt,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yxzt,conn,tableName);
	}
	
	public static Yxzt insert(Yxzt yxzt,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,名称) VALUES (?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yxzt.getId(),yxzt.getMc());
			if(yxzt.getId()==0){
				yxzt.setId(i);
			}
			return i > 0 ? yxzt : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yxzt insert(Yxzt yxzt,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yxzt,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yxzt> yxzts){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yxzts,conn);
	}
	
	public static int[] insertBatch(List<Yxzt> yxzts,Connection conn){
		return insertBatch(yxzts,conn,Yxzt.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yxzt> yxzts,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yxzts,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yxzt> yxzts,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yxzts,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yxzt> yxzts,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,名称) VALUES (?,?)";
		try {
			int columnSize = 2;
			int size = yxzts.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yxzts.get(i).getId();
				params[i][1] =yxzts.get(i).getMc();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yxzt> yxzts,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yxzts,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yxzt update(Yxzt yxzt){
		Connection conn = SK_Config.getConnection();
		return update(yxzt,conn);
	}
	
	public static Yxzt update(Yxzt yxzt,Connection conn){
		return update(yxzt,conn,Yxzt.TABLENAME);
	}
	
	public static Yxzt update(Yxzt yxzt,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yxzt,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yxzt update(Yxzt yxzt,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yxzt,conn,tableName);
	}
	
	public static Yxzt update(Yxzt yxzt,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yxzt.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yxzt;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yxzt.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yxzt : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yxzt.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yxzt update(Yxzt yxzt,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yxzt,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Yxzt yxzt){
		Connection conn = SK_Config.getConnection();
		return delete(yxzt,conn);
	}
	
	public static boolean delete(Yxzt yxzt,Connection conn){
		return delete(yxzt,conn,Yxzt.TABLENAME);
	}
	
	public static boolean delete(Yxzt yxzt,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yxzt,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yxzt yxzt,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yxzt,conn,tableName);
	}
	
	public static boolean delete(Yxzt yxzt,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yxzt.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yxzt yxzt,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yxzt,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yxzt> yxzts){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yxzts,conn);
	}
	
	public static boolean deleteBatch(List<Yxzt> yxzts,Connection conn){
		return deleteBatch(yxzts,conn,Yxzt.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yxzt> yxzts,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yxzts,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yxzt> yxzts,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yxzts,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yxzt> yxzts,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yxzts.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yxzts.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yxzt> yxzts,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yxzts,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yxzt getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yxzt getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yxzt getById(int id,Connection conn){
		return getById(id,conn,Yxzt.TABLENAME);
	}
	
	public static Yxzt getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName + " WHERE " + "id = ?";
		Yxzt yxzt = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yxzt = Yxzt.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yxzt;
	}
	
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yxzt getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yxzt getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public static List<Yxzt> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yxzt> getAll(Connection conn){
		return getAll(conn,Yxzt.TABLENAME);
	}
	
	public static List<Yxzt> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yxzt> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yxzt> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName;
		List<Yxzt> yxzts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yxzts = Yxzt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yxzts;
	}
	
	public static List<Yxzt> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yxzt> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Yxzt> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Yxzt.TABLENAME,page,pageCount);
	}
	
	public static List<Yxzt> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yxzt> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Yxzt> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,名称 FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Yxzt> yxzts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yxzts = Yxzt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yxzts;
	}
	
	public static List<Yxzt> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}