package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.db.bean.Lm;

public class LmDao {
	public static Lm insert(Lm lm){
		Connection conn = SK_Config.getConnection();
		return insert(lm,conn);
	}
	
	public static Lm insert(Lm lm,Connection conn){
		return insert(lm,conn,Lm.TABLENAME);
	}
	
	public static Lm insert(Lm lm,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(lm,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Lm insert(Lm lm,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(lm,conn,tableName);
	}
	
	public static Lm insert(Lm lm,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,名称,创建人id) VALUES (?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,lm.getId(),lm.getMc(),lm.getCjrid());
			if(lm.getId()==0){
				lm.setId(i);
			}
			return i > 0 ? lm : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Lm insert(Lm lm,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(lm,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Lm> lms){
		Connection conn = SK_Config.getConnection();
		return insertBatch(lms,conn);
	}
	
	public static int[] insertBatch(List<Lm> lms,Connection conn){
		return insertBatch(lms,conn,Lm.TABLENAME);
	}
	
	public static int[] insertBatch(List<Lm> lms,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(lms,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Lm> lms,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(lms,conn,tableName);
	}
	
	public static int[] insertBatch(List<Lm> lms,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,名称,创建人id) VALUES (?,?,?)";
		try {
			int columnSize = 3;
			int size = lms.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =lms.get(i).getId();
				params[i][1] =lms.get(i).getMc();
				params[i][2] =lms.get(i).getCjrid();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Lm> lms,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(lms,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Lm update(Lm lm){
		Connection conn = SK_Config.getConnection();
		return update(lm,conn);
	}
	
	public static Lm update(Lm lm,Connection conn){
		return update(lm,conn,Lm.TABLENAME);
	}
	
	public static Lm update(Lm lm,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(lm,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Lm update(Lm lm,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(lm,conn,tableName);
	}
	
	public static Lm update(Lm lm,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = lm.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return lm;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = lm.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? lm : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				lm.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Lm update(Lm lm,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(lm,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Lm lm){
		Connection conn = SK_Config.getConnection();
		return delete(lm,conn);
	}
	
	public static boolean delete(Lm lm,Connection conn){
		return delete(lm,conn,Lm.TABLENAME);
	}
	
	public static boolean delete(Lm lm,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(lm,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Lm lm,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(lm,conn,tableName);
	}
	
	public static boolean delete(Lm lm,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, lm.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Lm lm,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(lm,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Lm> lms){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(lms,conn);
	}
	
	public static boolean deleteBatch(List<Lm> lms,Connection conn){
		return deleteBatch(lms,conn,Lm.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Lm> lms,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(lms,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Lm> lms,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(lms,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Lm> lms,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = lms.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = lms.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Lm> lms,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(lms,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Lm getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Lm getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 创建人id ) 查询
	 */
	public static List<Lm> getByCjrid(int cjrid){
		Connection conn = SK_Config.getConnection();
		return getByCjrid(cjrid, conn);
	}
	
	public static List<Lm> getByCjrid(int cjrid,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByCjrid(cjrid, conn,tableName);
	}
	
	public static List<Lm> getByPageCjrid(int cjrid,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageCjrid(cjrid, conn,page,pageCount);
	}
	
	public static List<Lm> getByPageCjrid(int cjrid,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageCjrid(cjrid, conn,tableName,page,pageCount);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Lm getById(int id,Connection conn){
		return getById(id,conn,Lm.TABLENAME);
	}
	
	public static Lm getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称,创建人id FROM " + tableName + " WHERE " + "id = ?";
		Lm lm = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			lm = Lm.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lm;
	}
	
	/**
	 * 根据( 创建人id ) 查询
	 */
	public static List<Lm> getByCjrid(int cjrid,Connection conn){
		return getByCjrid(cjrid,conn,Lm.TABLENAME);
	}
	
	public static List<Lm> getByCjrid(int cjrid,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称,创建人id FROM " + tableName + " WHERE " + "cjrid = ?";
		List<Lm> lms = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), cjrid);
			lms = Lm.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lms;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Lm> getByPageCjrid(int cjrid,Connection conn,int page,int pageCount){
		return getByPageCjrid(cjrid,conn,Lm.TABLENAME,page,pageCount);
	}
	
	public static List<Lm> getByPageCjrid(int cjrid,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,名称,创建人id FROM " + tableName + " WHERE " + "cjrid = ? LIMIT " + page + " , " +pageCount;
		List<Lm> lms = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), cjrid);
			lms = Lm.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lms;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Lm getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Lm getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 创建人id ) 查询
	 */
	public static List<Lm> getByCjrid(int cjrid,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByCjrid(cjrid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lm> getByCjrid(int cjrid,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByCjrid(cjrid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Lm> getByPageCjrid(int cjrid,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageCjrid(cjrid, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lm> getByPageCjrid(int cjrid,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageCjrid(cjrid, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Lm> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Lm> getAll(Connection conn){
		return getAll(conn,Lm.TABLENAME);
	}
	
	public static List<Lm> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lm> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Lm> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称,创建人id FROM " + tableName;
		List<Lm> lms = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			lms = Lm.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lms;
	}
	
	public static List<Lm> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Lm> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Lm> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Lm.TABLENAME,page,pageCount);
	}
	
	public static List<Lm> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lm> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Lm> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,名称,创建人id FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Lm> lms = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			lms = Lm.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lms;
	}
	
	public static List<Lm> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}