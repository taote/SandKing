package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.db.bean.Bydl;

public class BydlDao {
	public static Bydl insert(Bydl bydl){
		Connection conn = SK_Config.getConnection();
		return insert(bydl,conn);
	}
	
	public static Bydl insert(Bydl bydl,Connection conn){
		return insert(bydl,conn,Bydl.TABLENAME);
	}
	
	public static Bydl insert(Bydl bydl,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(bydl,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Bydl insert(Bydl bydl,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(bydl,conn,tableName);
	}
	
	public static Bydl insert(Bydl bydl,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,用户建筑_id) VALUES (?,?)";
		try {
			int i = (int)sq.insert(conn,sql,bydl.getId(),bydl.getYhjz_id());
			if(bydl.getId()==0){
				bydl.setId(i);
			}
			return i > 0 ? bydl : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Bydl insert(Bydl bydl,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(bydl,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Bydl> bydls){
		Connection conn = SK_Config.getConnection();
		return insertBatch(bydls,conn);
	}
	
	public static int[] insertBatch(List<Bydl> bydls,Connection conn){
		return insertBatch(bydls,conn,Bydl.TABLENAME);
	}
	
	public static int[] insertBatch(List<Bydl> bydls,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(bydls,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Bydl> bydls,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(bydls,conn,tableName);
	}
	
	public static int[] insertBatch(List<Bydl> bydls,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,用户建筑_id) VALUES (?,?)";
		try {
			int columnSize = 2;
			int size = bydls.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =bydls.get(i).getId();
				params[i][1] =bydls.get(i).getYhjz_id();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Bydl> bydls,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(bydls,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Bydl update(Bydl bydl){
		Connection conn = SK_Config.getConnection();
		return update(bydl,conn);
	}
	
	public static Bydl update(Bydl bydl,Connection conn){
		return update(bydl,conn,Bydl.TABLENAME);
	}
	
	public static Bydl update(Bydl bydl,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(bydl,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Bydl update(Bydl bydl,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(bydl,conn,tableName);
	}
	
	public static Bydl update(Bydl bydl,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = bydl.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return bydl;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = bydl.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? bydl : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				bydl.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Bydl update(Bydl bydl,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(bydl,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Bydl bydl){
		Connection conn = SK_Config.getConnection();
		return delete(bydl,conn);
	}
	
	public static boolean delete(Bydl bydl,Connection conn){
		return delete(bydl,conn,Bydl.TABLENAME);
	}
	
	public static boolean delete(Bydl bydl,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(bydl,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Bydl bydl,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(bydl,conn,tableName);
	}
	
	public static boolean delete(Bydl bydl,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, bydl.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Bydl bydl,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(bydl,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Bydl> bydls){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(bydls,conn);
	}
	
	public static boolean deleteBatch(List<Bydl> bydls,Connection conn){
		return deleteBatch(bydls,conn,Bydl.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Bydl> bydls,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(bydls,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Bydl> bydls,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(bydls,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Bydl> bydls,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = bydls.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = bydls.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Bydl> bydls,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(bydls,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Bydl getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Bydl getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Bydl> getByYhjz_id(int yhjz_id){
		Connection conn = SK_Config.getConnection();
		return getByYhjz_id(yhjz_id, conn);
	}
	
	public static List<Bydl> getByYhjz_id(int yhjz_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYhjz_id(yhjz_id, conn,tableName);
	}
	
	public static List<Bydl> getByPageYhjz_id(int yhjz_id,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageYhjz_id(yhjz_id, conn,page,pageCount);
	}
	
	public static List<Bydl> getByPageYhjz_id(int yhjz_id,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageYhjz_id(yhjz_id, conn,tableName,page,pageCount);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Bydl getById(int id,Connection conn){
		return getById(id,conn,Bydl.TABLENAME);
	}
	
	public static Bydl getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户建筑_id FROM " + tableName + " WHERE " + "id = ?";
		Bydl bydl = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			bydl = Bydl.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return bydl;
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Bydl> getByYhjz_id(int yhjz_id,Connection conn){
		return getByYhjz_id(yhjz_id,conn,Bydl.TABLENAME);
	}
	
	public static List<Bydl> getByYhjz_id(int yhjz_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户建筑_id FROM " + tableName + " WHERE " + "yhjz_id = ?";
		List<Bydl> bydls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhjz_id);
			bydls = Bydl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return bydls;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Bydl> getByPageYhjz_id(int yhjz_id,Connection conn,int page,int pageCount){
		return getByPageYhjz_id(yhjz_id,conn,Bydl.TABLENAME,page,pageCount);
	}
	
	public static List<Bydl> getByPageYhjz_id(int yhjz_id,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,用户建筑_id FROM " + tableName + " WHERE " + "yhjz_id = ? LIMIT " + page + " , " +pageCount;
		List<Bydl> bydls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhjz_id);
			bydls = Bydl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return bydls;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Bydl getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Bydl getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户建筑_id ) 查询
	 */
	public static List<Bydl> getByYhjz_id(int yhjz_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYhjz_id(yhjz_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Bydl> getByYhjz_id(int yhjz_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYhjz_id(yhjz_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Bydl> getByPageYhjz_id(int yhjz_id,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhjz_id(yhjz_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Bydl> getByPageYhjz_id(int yhjz_id,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhjz_id(yhjz_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Bydl> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Bydl> getAll(Connection conn){
		return getAll(conn,Bydl.TABLENAME);
	}
	
	public static List<Bydl> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Bydl> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Bydl> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户建筑_id FROM " + tableName;
		List<Bydl> bydls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			bydls = Bydl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return bydls;
	}
	
	public static List<Bydl> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Bydl> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Bydl> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Bydl.TABLENAME,page,pageCount);
	}
	
	public static List<Bydl> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Bydl> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Bydl> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,用户建筑_id FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Bydl> bydls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			bydls = Bydl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return bydls;
	}
	
	public static List<Bydl> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}