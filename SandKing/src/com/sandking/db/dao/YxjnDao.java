package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import com.sandking.db.bean.Yxjn;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;

public class YxjnDao {
	public static Yxjn insert(Yxjn yxjn){
		Connection conn = SK_Config.getConnection();
		return insert(yxjn,conn);
	}
	
	public static Yxjn insert(Yxjn yxjn,Connection conn){
		return insert(yxjn,conn,Yxjn.TABLENAME);
	}
	
	public static Yxjn insert(Yxjn yxjn,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yxjn,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yxjn insert(Yxjn yxjn,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yxjn,conn,tableName);
	}
	
	public static Yxjn insert(Yxjn yxjn,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,用户英雄_id,技能id,技能lvl) VALUES (?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yxjn.getId(),yxjn.getYhyx_id(),yxjn.getJnid(),yxjn.getJnlvl());
			if(yxjn.getId()==0){
				yxjn.setId(i);
			}
			return i > 0 ? yxjn : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yxjn insert(Yxjn yxjn,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yxjn,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yxjn> yxjns){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yxjns,conn);
	}
	
	public static int[] insertBatch(List<Yxjn> yxjns,Connection conn){
		return insertBatch(yxjns,conn,Yxjn.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yxjn> yxjns,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yxjns,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yxjn> yxjns,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yxjns,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yxjn> yxjns,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,用户英雄_id,技能id,技能lvl) VALUES (?,?,?,?)";
		try {
			int columnSize = 4;
			int size = yxjns.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yxjns.get(i).getId();
				params[i][1] =yxjns.get(i).getYhyx_id();
				params[i][2] =yxjns.get(i).getJnid();
				params[i][3] =yxjns.get(i).getJnlvl();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yxjn> yxjns,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yxjns,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yxjn update(Yxjn yxjn){
		Connection conn = SK_Config.getConnection();
		return update(yxjn,conn);
	}
	
	public static Yxjn update(Yxjn yxjn,Connection conn){
		return update(yxjn,conn,Yxjn.TABLENAME);
	}
	
	public static Yxjn update(Yxjn yxjn,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yxjn,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yxjn update(Yxjn yxjn,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yxjn,conn,tableName);
	}
	
	public static Yxjn update(Yxjn yxjn,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yxjn.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yxjn;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yxjn.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yxjn : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yxjn.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yxjn update(Yxjn yxjn,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yxjn,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Yxjn yxjn){
		Connection conn = SK_Config.getConnection();
		return delete(yxjn,conn);
	}
	
	public static boolean delete(Yxjn yxjn,Connection conn){
		return delete(yxjn,conn,Yxjn.TABLENAME);
	}
	
	public static boolean delete(Yxjn yxjn,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yxjn,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yxjn yxjn,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yxjn,conn,tableName);
	}
	
	public static boolean delete(Yxjn yxjn,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yxjn.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yxjn yxjn,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yxjn,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yxjn> yxjns){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yxjns,conn);
	}
	
	public static boolean deleteBatch(List<Yxjn> yxjns,Connection conn){
		return deleteBatch(yxjns,conn,Yxjn.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yxjn> yxjns,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yxjns,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yxjn> yxjns,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yxjns,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yxjn> yxjns,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yxjns.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yxjns.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yxjn> yxjns,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yxjns,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yxjn getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yxjn getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 用户英雄_id ) 查询
	 */
	public static List<Yxjn> getByYhyx_id(int yhyx_id){
		Connection conn = SK_Config.getConnection();
		return getByYhyx_id(yhyx_id, conn);
	}
	
	public static List<Yxjn> getByYhyx_id(int yhyx_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYhyx_id(yhyx_id, conn,tableName);
	}
	
	public static List<Yxjn> getByPageYhyx_id(int yhyx_id,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageYhyx_id(yhyx_id, conn,page,pageCount);
	}
	
	public static List<Yxjn> getByPageYhyx_id(int yhyx_id,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageYhyx_id(yhyx_id, conn,tableName,page,pageCount);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yxjn getById(int id,Connection conn){
		return getById(id,conn,Yxjn.TABLENAME);
	}
	
	public static Yxjn getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户英雄_id,技能id,技能lvl FROM " + tableName + " WHERE " + "id = ?";
		Yxjn yxjn = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yxjn = Yxjn.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yxjn;
	}
	
	/**
	 * 根据( 用户英雄_id ) 查询
	 */
	public static List<Yxjn> getByYhyx_id(int yhyx_id,Connection conn){
		return getByYhyx_id(yhyx_id,conn,Yxjn.TABLENAME);
	}
	
	public static List<Yxjn> getByYhyx_id(int yhyx_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户英雄_id,技能id,技能lvl FROM " + tableName + " WHERE " + "yhyx_id = ?";
		List<Yxjn> yxjns = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhyx_id);
			yxjns = Yxjn.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yxjns;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yxjn> getByPageYhyx_id(int yhyx_id,Connection conn,int page,int pageCount){
		return getByPageYhyx_id(yhyx_id,conn,Yxjn.TABLENAME,page,pageCount);
	}
	
	public static List<Yxjn> getByPageYhyx_id(int yhyx_id,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,用户英雄_id,技能id,技能lvl FROM " + tableName + " WHERE " + "yhyx_id = ? LIMIT " + page + " , " +pageCount;
		List<Yxjn> yxjns = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhyx_id);
			yxjns = Yxjn.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yxjns;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yxjn getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yxjn getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户英雄_id ) 查询
	 */
	public static List<Yxjn> getByYhyx_id(int yhyx_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYhyx_id(yhyx_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yxjn> getByYhyx_id(int yhyx_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYhyx_id(yhyx_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yxjn> getByPageYhyx_id(int yhyx_id,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhyx_id(yhyx_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yxjn> getByPageYhyx_id(int yhyx_id,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhyx_id(yhyx_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Yxjn> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yxjn> getAll(Connection conn){
		return getAll(conn,Yxjn.TABLENAME);
	}
	
	public static List<Yxjn> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yxjn> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yxjn> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户英雄_id,技能id,技能lvl FROM " + tableName;
		List<Yxjn> yxjns = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yxjns = Yxjn.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yxjns;
	}
	
	public static List<Yxjn> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yxjn> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Yxjn> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Yxjn.TABLENAME,page,pageCount);
	}
	
	public static List<Yxjn> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yxjn> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Yxjn> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,用户英雄_id,技能id,技能lvl FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Yxjn> yxjns = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yxjns = Yxjn.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yxjns;
	}
	
	public static List<Yxjn> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}