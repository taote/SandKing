package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.db.bean.Yh;

public class YhDao {
	public static Yh insert(Yh yh){
		Connection conn = SK_Config.getConnection();
		return insert(yh,conn);
	}
	
	public static Yh insert(Yh yh,Connection conn){
		return insert(yh,conn,Yh.TABLENAME);
	}
	
	public static Yh insert(Yh yh,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yh,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yh insert(Yh yh,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yh,conn,tableName);
	}
	
	public static Yh insert(Yh yh,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yh.getId(),yh.getNc(),yh.getCjsj(),yh.getZhdlsj(),yh.getYy(),yh.getZh(),yh.getMm(),yh.getBs(),yh.getJbs(),yh.getJb(),yh.getS(),yh.getXld(),yh.getSb(),yh.getSbxt(),yh.getFbl(),yh.getQd(),yh.getYhlx_id(),yh.getYhlvl(),yh.getYhlvl_exp(),yh.getVIP(),yh.getVIP_exp(),yh.getCzje(),yh.getXy(),yh.getKhdbb(),yh.getFwq_id(),yh.getZclvl(),yh.getSd(),yh.getLm_id());
			if(yh.getId()==0){
				yh.setId(i);
			}
			return i > 0 ? yh : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yh insert(Yh yh,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yh,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yh> yhs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhs,conn);
	}
	
	public static int[] insertBatch(List<Yh> yhs,Connection conn){
		return insertBatch(yhs,conn,Yh.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yh> yhs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yh> yhs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yh> yhs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			int columnSize = 28;
			int size = yhs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhs.get(i).getId();
				params[i][1] =yhs.get(i).getNc();
				params[i][2] =yhs.get(i).getCjsj();
				params[i][3] =yhs.get(i).getZhdlsj();
				params[i][4] =yhs.get(i).getYy();
				params[i][5] =yhs.get(i).getZh();
				params[i][6] =yhs.get(i).getMm();
				params[i][7] =yhs.get(i).getBs();
				params[i][8] =yhs.get(i).getJbs();
				params[i][9] =yhs.get(i).getJb();
				params[i][10] =yhs.get(i).getS();
				params[i][11] =yhs.get(i).getXld();
				params[i][12] =yhs.get(i).getSb();
				params[i][13] =yhs.get(i).getSbxt();
				params[i][14] =yhs.get(i).getFbl();
				params[i][15] =yhs.get(i).getQd();
				params[i][16] =yhs.get(i).getYhlx_id();
				params[i][17] =yhs.get(i).getYhlvl();
				params[i][18] =yhs.get(i).getYhlvl_exp();
				params[i][19] =yhs.get(i).getVIP();
				params[i][20] =yhs.get(i).getVIP_exp();
				params[i][21] =yhs.get(i).getCzje();
				params[i][22] =yhs.get(i).getXy();
				params[i][23] =yhs.get(i).getKhdbb();
				params[i][24] =yhs.get(i).getFwq_id();
				params[i][25] =yhs.get(i).getZclvl();
				params[i][26] =yhs.get(i).getSd();
				params[i][27] =yhs.get(i).getLm_id();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yh> yhs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yh update(Yh yh){
		Connection conn = SK_Config.getConnection();
		return update(yh,conn);
	}
	
	public static Yh update(Yh yh,Connection conn){
		return update(yh,conn,Yh.TABLENAME);
	}
	
	public static Yh update(Yh yh,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yh,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yh update(Yh yh,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yh,conn,tableName);
	}
	
	public static Yh update(Yh yh,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yh.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yh;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yh.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yh : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yh.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yh update(Yh yh,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yh,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Yh yh){
		Connection conn = SK_Config.getConnection();
		return delete(yh,conn);
	}
	
	public static boolean delete(Yh yh,Connection conn){
		return delete(yh,conn,Yh.TABLENAME);
	}
	
	public static boolean delete(Yh yh,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yh,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yh yh,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yh,conn,tableName);
	}
	
	public static boolean delete(Yh yh,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yh.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yh yh,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yh,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yh> yhs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhs,conn);
	}
	
	public static boolean deleteBatch(List<Yh> yhs,Connection conn){
		return deleteBatch(yhs,conn,Yh.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yh> yhs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yh> yhs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yh> yhs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yhs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yhs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yh> yhs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yh getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yh getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 昵称 ) 查询
	 */
	public static Yh getByNc(String nc){
		Connection conn = SK_Config.getConnection();
		return getByNc(nc, conn);
	}
	
	public static Yh getByNc(String nc,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByNc(nc, conn,tableName);
	}
	
	/**
	 * 根据( 账号 ) 查询
	 */
	public static Yh getByZh(String zh){
		Connection conn = SK_Config.getConnection();
		return getByZh(zh, conn);
	}
	
	public static Yh getByZh(String zh,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByZh(zh, conn,tableName);
	}
	
	/**
	 * 根据( 账号  密码 ) 查询
	 */
	public static Yh getByZhMm(String zh, String mm){
		Connection conn = SK_Config.getConnection();
		return getByZhMm(zh, mm, conn);
	}
	
	public static Yh getByZhMm(String zh, String mm,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByZhMm(zh, mm, conn,tableName);
	}
	
	/**
	 * 根据( 用户类型_id ) 查询
	 */
	public static List<Yh> getByYhlx_id(int yhlx_id){
		Connection conn = SK_Config.getConnection();
		return getByYhlx_id(yhlx_id, conn);
	}
	
	public static List<Yh> getByYhlx_id(int yhlx_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYhlx_id(yhlx_id, conn,tableName);
	}
	
	public static List<Yh> getByPageYhlx_id(int yhlx_id,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageYhlx_id(yhlx_id, conn,page,pageCount);
	}
	
	public static List<Yh> getByPageYhlx_id(int yhlx_id,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageYhlx_id(yhlx_id, conn,tableName,page,pageCount);
	}
	/**
	 * 根据( 服务器_id ) 查询
	 */
	public static List<Yh> getByFwq_id(int fwq_id){
		Connection conn = SK_Config.getConnection();
		return getByFwq_id(fwq_id, conn);
	}
	
	public static List<Yh> getByFwq_id(int fwq_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByFwq_id(fwq_id, conn,tableName);
	}
	
	public static List<Yh> getByPageFwq_id(int fwq_id,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageFwq_id(fwq_id, conn,page,pageCount);
	}
	
	public static List<Yh> getByPageFwq_id(int fwq_id,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageFwq_id(fwq_id, conn,tableName,page,pageCount);
	}
	/**
	 * 根据( 渠道 ) 查询
	 */
	public static List<Yh> getByQd(String qd){
		Connection conn = SK_Config.getConnection();
		return getByQd(qd, conn);
	}
	
	public static List<Yh> getByQd(String qd,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByQd(qd, conn,tableName);
	}
	
	public static List<Yh> getByPageQd(String qd,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageQd(qd, conn,page,pageCount);
	}
	
	public static List<Yh> getByPageQd(String qd,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageQd(qd, conn,tableName,page,pageCount);
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Yh> getByLm_id(int lm_id){
		Connection conn = SK_Config.getConnection();
		return getByLm_id(lm_id, conn);
	}
	
	public static List<Yh> getByLm_id(int lm_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByLm_id(lm_id, conn,tableName);
	}
	
	public static List<Yh> getByPageLm_id(int lm_id,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageLm_id(lm_id, conn,page,pageCount);
	}
	
	public static List<Yh> getByPageLm_id(int lm_id,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageLm_id(lm_id, conn,tableName,page,pageCount);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yh getById(int id,Connection conn){
		return getById(id,conn,Yh.TABLENAME);
	}
	
	public static Yh getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "id = ?";
		Yh yh = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yh = Yh.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 昵称 ) 查询
	 */
	public static Yh getByNc(String nc,Connection conn){
		return getByNc(nc,conn,Yh.TABLENAME);
	}
	
	public static Yh getByNc(String nc,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "nc = ?";
		Yh yh = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), nc);
			yh = Yh.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 账号 ) 查询
	 */
	public static Yh getByZh(String zh,Connection conn){
		return getByZh(zh,conn,Yh.TABLENAME);
	}
	
	public static Yh getByZh(String zh,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "zh = ?";
		Yh yh = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), zh);
			yh = Yh.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 账号  密码 ) 查询
	 */
	public static Yh getByZhMm(String zh, String mm,Connection conn){
		return getByZhMm(zh, mm,conn,Yh.TABLENAME);
	}
	
	public static Yh getByZhMm(String zh, String mm,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "zh = ? AND mm = ?";
		Yh yh = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), zh, mm);
			yh = Yh.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yh;
	}
	
	/**
	 * 根据( 用户类型_id ) 查询
	 */
	public static List<Yh> getByYhlx_id(int yhlx_id,Connection conn){
		return getByYhlx_id(yhlx_id,conn,Yh.TABLENAME);
	}
	
	public static List<Yh> getByYhlx_id(int yhlx_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "yhlx_id = ?";
		List<Yh> yhs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhlx_id);
			yhs = Yh.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yh> getByPageYhlx_id(int yhlx_id,Connection conn,int page,int pageCount){
		return getByPageYhlx_id(yhlx_id,conn,Yh.TABLENAME,page,pageCount);
	}
	
	public static List<Yh> getByPageYhlx_id(int yhlx_id,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "yhlx_id = ? LIMIT " + page + " , " +pageCount;
		List<Yh> yhs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhlx_id);
			yhs = Yh.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhs;
	}
	/**
	 * 根据( 服务器_id ) 查询
	 */
	public static List<Yh> getByFwq_id(int fwq_id,Connection conn){
		return getByFwq_id(fwq_id,conn,Yh.TABLENAME);
	}
	
	public static List<Yh> getByFwq_id(int fwq_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "fwq_id = ?";
		List<Yh> yhs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), fwq_id);
			yhs = Yh.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yh> getByPageFwq_id(int fwq_id,Connection conn,int page,int pageCount){
		return getByPageFwq_id(fwq_id,conn,Yh.TABLENAME,page,pageCount);
	}
	
	public static List<Yh> getByPageFwq_id(int fwq_id,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "fwq_id = ? LIMIT " + page + " , " +pageCount;
		List<Yh> yhs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), fwq_id);
			yhs = Yh.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhs;
	}
	/**
	 * 根据( 渠道 ) 查询
	 */
	public static List<Yh> getByQd(String qd,Connection conn){
		return getByQd(qd,conn,Yh.TABLENAME);
	}
	
	public static List<Yh> getByQd(String qd,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "qd = ?";
		List<Yh> yhs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), qd);
			yhs = Yh.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yh> getByPageQd(String qd,Connection conn,int page,int pageCount){
		return getByPageQd(qd,conn,Yh.TABLENAME,page,pageCount);
	}
	
	public static List<Yh> getByPageQd(String qd,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "qd = ? LIMIT " + page + " , " +pageCount;
		List<Yh> yhs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), qd);
			yhs = Yh.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhs;
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Yh> getByLm_id(int lm_id,Connection conn){
		return getByLm_id(lm_id,conn,Yh.TABLENAME);
	}
	
	public static List<Yh> getByLm_id(int lm_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "lm_id = ?";
		List<Yh> yhs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), lm_id);
			yhs = Yh.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhs;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Yh> getByPageLm_id(int lm_id,Connection conn,int page,int pageCount){
		return getByPageLm_id(lm_id,conn,Yh.TABLENAME,page,pageCount);
	}
	
	public static List<Yh> getByPageLm_id(int lm_id,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " WHERE " + "lm_id = ? LIMIT " + page + " , " +pageCount;
		List<Yh> yhs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), lm_id);
			yhs = Yh.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhs;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yh getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yh getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 昵称 ) 查询
	 */
	public static Yh getByNc(String nc,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByNc(nc, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yh getByNc(String nc,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByNc(nc, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 账号 ) 查询
	 */
	public static Yh getByZh(String zh,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByZh(zh, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yh getByZh(String zh,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByZh(zh, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 账号  密码 ) 查询
	 */
	public static Yh getByZhMm(String zh, String mm,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByZhMm(zh, mm, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yh getByZhMm(String zh, String mm,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByZhMm(zh, mm, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户类型_id ) 查询
	 */
	public static List<Yh> getByYhlx_id(int yhlx_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYhlx_id(yhlx_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yh> getByYhlx_id(int yhlx_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYhlx_id(yhlx_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yh> getByPageYhlx_id(int yhlx_id,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhlx_id(yhlx_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yh> getByPageYhlx_id(int yhlx_id,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhlx_id(yhlx_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据( 服务器_id ) 查询
	 */
	public static List<Yh> getByFwq_id(int fwq_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByFwq_id(fwq_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yh> getByFwq_id(int fwq_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByFwq_id(fwq_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yh> getByPageFwq_id(int fwq_id,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageFwq_id(fwq_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yh> getByPageFwq_id(int fwq_id,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageFwq_id(fwq_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据( 渠道 ) 查询
	 */
	public static List<Yh> getByQd(String qd,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByQd(qd, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yh> getByQd(String qd,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByQd(qd, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yh> getByPageQd(String qd,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageQd(qd, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yh> getByPageQd(String qd,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageQd(qd, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Yh> getByLm_id(int lm_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByLm_id(lm_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yh> getByLm_id(int lm_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByLm_id(lm_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Yh> getByPageLm_id(int lm_id,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageLm_id(lm_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yh> getByPageLm_id(int lm_id,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageLm_id(lm_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Yh> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yh> getAll(Connection conn){
		return getAll(conn,Yh.TABLENAME);
	}
	
	public static List<Yh> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yh> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yh> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName;
		List<Yh> yhs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhs = Yh.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhs;
	}
	
	public static List<Yh> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yh> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Yh> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Yh.TABLENAME,page,pageCount);
	}
	
	public static List<Yh> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yh> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Yh> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,昵称,创建时间,最后登录时间,语言,账号,密码,宝石,假宝石,金币,水,修炼点,设备,设备系统,分辨率,渠道,用户类型_id,用户lvl,用户lvl_exp,VIP,VIP_exp,充值金额,幸运,客户端版本,服务器_id,主城lvl,锁定,联盟_id FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Yh> yhs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhs = Yh.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhs;
	}
	
	public static List<Yh> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}