package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import java.util.List;
import com.sandking.db.bean.Zadl;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;

public class ZadlDao {
	public static Zadl insert(Zadl zadl){
		Connection conn = SK_Config.getConnection();
		return insert(zadl,conn);
	}
	
	public static Zadl insert(Zadl zadl,Connection conn){
		return insert(zadl,conn,Zadl.TABLENAME);
	}
	
	public static Zadl insert(Zadl zadl,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(zadl,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Zadl insert(Zadl zadl,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(zadl,conn,tableName);
	}
	
	public static Zadl insert(Zadl zadl,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,用户障碍_id) VALUES (?,?)";
		try {
			int i = (int)sq.insert(conn,sql,zadl.getId(),zadl.getYhza_id());
			if(zadl.getId()==0){
				zadl.setId(i);
			}
			return i > 0 ? zadl : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Zadl insert(Zadl zadl,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(zadl,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Zadl> zadls){
		Connection conn = SK_Config.getConnection();
		return insertBatch(zadls,conn);
	}
	
	public static int[] insertBatch(List<Zadl> zadls,Connection conn){
		return insertBatch(zadls,conn,Zadl.TABLENAME);
	}
	
	public static int[] insertBatch(List<Zadl> zadls,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(zadls,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Zadl> zadls,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(zadls,conn,tableName);
	}
	
	public static int[] insertBatch(List<Zadl> zadls,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,用户障碍_id) VALUES (?,?)";
		try {
			int columnSize = 2;
			int size = zadls.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =zadls.get(i).getId();
				params[i][1] =zadls.get(i).getYhza_id();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Zadl> zadls,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(zadls,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Zadl update(Zadl zadl){
		Connection conn = SK_Config.getConnection();
		return update(zadl,conn);
	}
	
	public static Zadl update(Zadl zadl,Connection conn){
		return update(zadl,conn,Zadl.TABLENAME);
	}
	
	public static Zadl update(Zadl zadl,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(zadl,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Zadl update(Zadl zadl,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(zadl,conn,tableName);
	}
	
	public static Zadl update(Zadl zadl,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = zadl.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return zadl;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = zadl.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? zadl : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				zadl.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Zadl update(Zadl zadl,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(zadl,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Zadl zadl){
		Connection conn = SK_Config.getConnection();
		return delete(zadl,conn);
	}
	
	public static boolean delete(Zadl zadl,Connection conn){
		return delete(zadl,conn,Zadl.TABLENAME);
	}
	
	public static boolean delete(Zadl zadl,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(zadl,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Zadl zadl,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(zadl,conn,tableName);
	}
	
	public static boolean delete(Zadl zadl,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, zadl.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Zadl zadl,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(zadl,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Zadl> zadls){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(zadls,conn);
	}
	
	public static boolean deleteBatch(List<Zadl> zadls,Connection conn){
		return deleteBatch(zadls,conn,Zadl.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Zadl> zadls,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(zadls,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Zadl> zadls,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(zadls,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Zadl> zadls,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = zadls.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = zadls.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Zadl> zadls,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(zadls,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Zadl getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Zadl getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 用户障碍_id ) 查询
	 */
	public static List<Zadl> getByYhza_id(int yhza_id){
		Connection conn = SK_Config.getConnection();
		return getByYhza_id(yhza_id, conn);
	}
	
	public static List<Zadl> getByYhza_id(int yhza_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByYhza_id(yhza_id, conn,tableName);
	}
	
	public static List<Zadl> getByPageYhza_id(int yhza_id,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageYhza_id(yhza_id, conn,page,pageCount);
	}
	
	public static List<Zadl> getByPageYhza_id(int yhza_id,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageYhza_id(yhza_id, conn,tableName,page,pageCount);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Zadl getById(int id,Connection conn){
		return getById(id,conn,Zadl.TABLENAME);
	}
	
	public static Zadl getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户障碍_id FROM " + tableName + " WHERE " + "id = ?";
		Zadl zadl = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			zadl = Zadl.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return zadl;
	}
	
	/**
	 * 根据( 用户障碍_id ) 查询
	 */
	public static List<Zadl> getByYhza_id(int yhza_id,Connection conn){
		return getByYhza_id(yhza_id,conn,Zadl.TABLENAME);
	}
	
	public static List<Zadl> getByYhza_id(int yhza_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户障碍_id FROM " + tableName + " WHERE " + "yhza_id = ?";
		List<Zadl> zadls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhza_id);
			zadls = Zadl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return zadls;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Zadl> getByPageYhza_id(int yhza_id,Connection conn,int page,int pageCount){
		return getByPageYhza_id(yhza_id,conn,Zadl.TABLENAME,page,pageCount);
	}
	
	public static List<Zadl> getByPageYhza_id(int yhza_id,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,用户障碍_id FROM " + tableName + " WHERE " + "yhza_id = ? LIMIT " + page + " , " +pageCount;
		List<Zadl> zadls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), yhza_id);
			zadls = Zadl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return zadls;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Zadl getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Zadl getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 用户障碍_id ) 查询
	 */
	public static List<Zadl> getByYhza_id(int yhza_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByYhza_id(yhza_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Zadl> getByYhza_id(int yhza_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByYhza_id(yhza_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Zadl> getByPageYhza_id(int yhza_id,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhza_id(yhza_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Zadl> getByPageYhza_id(int yhza_id,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageYhza_id(yhza_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Zadl> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Zadl> getAll(Connection conn){
		return getAll(conn,Zadl.TABLENAME);
	}
	
	public static List<Zadl> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Zadl> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Zadl> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,用户障碍_id FROM " + tableName;
		List<Zadl> zadls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			zadls = Zadl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return zadls;
	}
	
	public static List<Zadl> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Zadl> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Zadl> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Zadl.TABLENAME,page,pageCount);
	}
	
	public static List<Zadl> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Zadl> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Zadl> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,用户障碍_id FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Zadl> zadls = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			zadls = Zadl.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return zadls;
	}
	
	public static List<Zadl> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}