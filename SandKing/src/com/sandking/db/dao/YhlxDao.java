package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;
import com.sandking.db.bean.Yhlx;

public class YhlxDao {
	public static Yhlx insert(Yhlx yhlx){
		Connection conn = SK_Config.getConnection();
		return insert(yhlx,conn);
	}
	
	public static Yhlx insert(Yhlx yhlx,Connection conn){
		return insert(yhlx,conn,Yhlx.TABLENAME);
	}
	
	public static Yhlx insert(Yhlx yhlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(yhlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhlx insert(Yhlx yhlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(yhlx,conn,tableName);
	}
	
	public static Yhlx insert(Yhlx yhlx,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,名称) VALUES (?,?)";
		try {
			int i = (int)sq.insert(conn,sql,yhlx.getId(),yhlx.getMc());
			if(yhlx.getId()==0){
				yhlx.setId(i);
			}
			return i > 0 ? yhlx : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhlx insert(Yhlx yhlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(yhlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Yhlx> yhlxs){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhlxs,conn);
	}
	
	public static int[] insertBatch(List<Yhlx> yhlxs,Connection conn){
		return insertBatch(yhlxs,conn,Yhlx.TABLENAME);
	}
	
	public static int[] insertBatch(List<Yhlx> yhlxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhlxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Yhlx> yhlxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(yhlxs,conn,tableName);
	}
	
	public static int[] insertBatch(List<Yhlx> yhlxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,名称) VALUES (?,?)";
		try {
			int columnSize = 2;
			int size = yhlxs.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =yhlxs.get(i).getId();
				params[i][1] =yhlxs.get(i).getMc();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Yhlx> yhlxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(yhlxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Yhlx update(Yhlx yhlx){
		Connection conn = SK_Config.getConnection();
		return update(yhlx,conn);
	}
	
	public static Yhlx update(Yhlx yhlx,Connection conn){
		return update(yhlx,conn,Yhlx.TABLENAME);
	}
	
	public static Yhlx update(Yhlx yhlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(yhlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhlx update(Yhlx yhlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(yhlx,conn,tableName);
	}
	
	public static Yhlx update(Yhlx yhlx,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = yhlx.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return yhlx;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = yhlx.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? yhlx : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				yhlx.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Yhlx update(Yhlx yhlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(yhlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Yhlx yhlx){
		Connection conn = SK_Config.getConnection();
		return delete(yhlx,conn);
	}
	
	public static boolean delete(Yhlx yhlx,Connection conn){
		return delete(yhlx,conn,Yhlx.TABLENAME);
	}
	
	public static boolean delete(Yhlx yhlx,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(yhlx,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Yhlx yhlx,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(yhlx,conn,tableName);
	}
	
	public static boolean delete(Yhlx yhlx,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, yhlx.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Yhlx yhlx,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(yhlx,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Yhlx> yhlxs){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhlxs,conn);
	}
	
	public static boolean deleteBatch(List<Yhlx> yhlxs,Connection conn){
		return deleteBatch(yhlxs,conn,Yhlx.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Yhlx> yhlxs,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhlxs,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Yhlx> yhlxs,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(yhlxs,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Yhlx> yhlxs,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = yhlxs.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = yhlxs.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Yhlx> yhlxs,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(yhlxs,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Yhlx getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Yhlx getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Yhlx getById(int id,Connection conn){
		return getById(id,conn,Yhlx.TABLENAME);
	}
	
	public static Yhlx getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName + " WHERE " + "id = ?";
		Yhlx yhlx = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			yhlx = Yhlx.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhlx;
	}
	
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Yhlx getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Yhlx getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	public static List<Yhlx> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Yhlx> getAll(Connection conn){
		return getAll(conn,Yhlx.TABLENAME);
	}
	
	public static List<Yhlx> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhlx> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Yhlx> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,名称 FROM " + tableName;
		List<Yhlx> yhlxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhlxs = Yhlx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhlxs;
	}
	
	public static List<Yhlx> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Yhlx> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Yhlx> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Yhlx.TABLENAME,page,pageCount);
	}
	
	public static List<Yhlx> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Yhlx> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Yhlx> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,名称 FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Yhlx> yhlxs = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			yhlxs = Yhlx.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return yhlxs;
	}
	
	public static List<Yhlx> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}