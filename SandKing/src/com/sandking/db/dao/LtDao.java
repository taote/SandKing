package com.sandking.db.dao;

import javax.sql.DataSource;
import org.apache.commons.dbutils.handlers.MapHandler;
import org.apache.commons.dbutils.QueryRunner;
import com.sandking.db.bean.Lt;
import com.sandking.config.SK_Config;
import java.sql.Connection;
import com.sandking.metadata.jdbc.SK_Query;
import java.util.Map;
import java.util.List;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.handlers.MapListHandler;

public class LtDao {
	public static Lt insert(Lt lt){
		Connection conn = SK_Config.getConnection();
		return insert(lt,conn);
	}
	
	public static Lt insert(Lt lt,Connection conn){
		return insert(lt,conn,Lt.TABLENAME);
	}
	
	public static Lt insert(Lt lt,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insert(lt,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Lt insert(Lt lt,String tableName){
		Connection conn = SK_Config.getConnection();
		return insert(lt,conn,tableName);
	}
	
	public static Lt insert(Lt lt,Connection conn,String tableName){
		
		SK_Query sq = new SK_Query();
		String sql = "INSERT INTO " +tableName+ " (id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间) VALUES (?,?,?,?,?,?,?,?)";
		try {
			int i = (int)sq.insert(conn,sql,lt.getId(),lt.getLmid(),lt.getNr(),lt.getLtlx_id(),lt.getLm_id(),lt.getJsrid(),lt.getFyrid(),lt.getCjsj());
			if(lt.getId()==0){
				lt.setId(i);
			}
			return i > 0 ? lt : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Lt insert(Lt lt,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insert(lt,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static int[] insertBatch(List<Lt> lts){
		Connection conn = SK_Config.getConnection();
		return insertBatch(lts,conn);
	}
	
	public static int[] insertBatch(List<Lt> lts,Connection conn){
		return insertBatch(lts,conn,Lt.TABLENAME);
	}
	
	public static int[] insertBatch(List<Lt> lts,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(lts,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int[] insertBatch(List<Lt> lts,String tableName){
		Connection conn = SK_Config.getConnection();
		return insertBatch(lts,conn,tableName);
	}
	
	public static int[] insertBatch(List<Lt> lts,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "INSERT INTO " +tableName+ " (id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间) VALUES (?,?,?,?,?,?,?,?)";
		try {
			int columnSize = 8;
			int size = lts.size();
			Object[][] params = new Object[size][columnSize];
			for (int i = 0; i < size; i++) {
				params[i][0] =lts.get(i).getId();
				params[i][1] =lts.get(i).getLmid();
				params[i][2] =lts.get(i).getNr();
				params[i][3] =lts.get(i).getLtlx_id();
				params[i][4] =lts.get(i).getLm_id();
				params[i][5] =lts.get(i).getJsrid();
				params[i][6] =lts.get(i).getFyrid();
				params[i][7] =lts.get(i).getCjsj();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 1 ? is : new int[]{};
		} catch (Exception e) {
			e.printStackTrace();
			return new int[]{};
		} finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static int[] insertBatch(List<Lt> lts,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return insertBatch(lts,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static Lt update(Lt lt){
		Connection conn = SK_Config.getConnection();
		return update(lt,conn);
	}
	
	public static Lt update(Lt lt,Connection conn){
		return update(lt,conn,Lt.TABLENAME);
	}
	
	public static Lt update(Lt lt,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return update(lt,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Lt update(Lt lt,String tableName){
		Connection conn = SK_Config.getConnection();
		return update(lt,conn,tableName);
	}
	
	public static Lt update(Lt lt,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		StringBuffer sb = new StringBuffer();
		Map<String, Object> updateColumns = lt.getUpdateColumns();
		int columnSize = updateColumns.size();
		if (updateColumns.isEmpty()) {
			return lt;
		}
		sb.append("UPDATE ");
		sb.append(tableName);
		sb.append(" SET ");
		Object[] values = new Object[(columnSize + 1)];
		int i = 0;
		for (Map.Entry<String, Object> updateColumn : updateColumns.entrySet()) {
			String key = updateColumn.getKey();
			values[i] = updateColumn.getValue();
			i++;
			sb.append(key);
			sb.append("=");
			sb.append("?");
			if (i < columnSize) {
				sb.append(",");
			}
		}
		sb.append(" WHERE ");
		sb.append("id");
		sb.append(" = ?");
		values[columnSize] = lt.getId();
		String sql = sb.toString();
		try {
			i = run.update(conn, sql, values);			
			return i == 1 ? lt : null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			try{
				lt.clearUpdateColumn();
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
	}
	
	public static Lt update(Lt lt,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return update(lt,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static boolean delete(Lt lt){
		Connection conn = SK_Config.getConnection();
		return delete(lt,conn);
	}
	
	public static boolean delete(Lt lt,Connection conn){
		return delete(lt,conn,Lt.TABLENAME);
	}
	
	public static boolean delete(Lt lt,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return delete(lt,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean delete(Lt lt,String tableName){
		Connection conn = SK_Config.getConnection();
		return delete(lt,conn,tableName);
	}
	
	public static boolean delete(Lt lt,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int i = run.update(conn,sql, lt.getId());
			return i > 0 ? true : false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean delete(Lt lt,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return delete(lt,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	
	public static boolean deleteBatch(List<Lt> lts){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(lts,conn);
	}
	
	public static boolean deleteBatch(List<Lt> lts,Connection conn){
		return deleteBatch(lts,conn,Lt.TABLENAME);
	}
	
	public static boolean deleteBatch(List<Lt> lts,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(lts,conn);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public static boolean deleteBatch(List<Lt> lts,String tableName){
		Connection conn = SK_Config.getConnection();
		return deleteBatch(lts,conn,tableName);
	}
	
	public static boolean deleteBatch(List<Lt> lts,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "DELETE FROM " + tableName + " WHERE id = ?";
		try {
			int size = lts.size();
			Object[][] params = new Object[size][1];
			for (int i = 0; i < size; i++) {
				params[i][0] = lts.get(i).getId();
			}
			int[] is = run.batch(conn,sql,params);
			return is.length > 0;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return false;
			}
		}
	}
	
	public static boolean deleteBatch(List<Lt> lts,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return deleteBatch(lts,conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 根据( id ) 查询
	 */
	public static Lt getById(int id){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn);
	}
	
	public static Lt getById(int id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getById(id, conn,tableName);
	}
	
	/**
	 * 根据( 聊天类型_id ) 查询
	 */
	public static List<Lt> getByLtlx_id(int ltlx_id){
		Connection conn = SK_Config.getConnection();
		return getByLtlx_id(ltlx_id, conn);
	}
	
	public static List<Lt> getByLtlx_id(int ltlx_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByLtlx_id(ltlx_id, conn,tableName);
	}
	
	public static List<Lt> getByPageLtlx_id(int ltlx_id,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageLtlx_id(ltlx_id, conn,page,pageCount);
	}
	
	public static List<Lt> getByPageLtlx_id(int ltlx_id,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageLtlx_id(ltlx_id, conn,tableName,page,pageCount);
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Lt> getByLm_id(int lm_id){
		Connection conn = SK_Config.getConnection();
		return getByLm_id(lm_id, conn);
	}
	
	public static List<Lt> getByLm_id(int lm_id,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByLm_id(lm_id, conn,tableName);
	}
	
	public static List<Lt> getByPageLm_id(int lm_id,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageLm_id(lm_id, conn,page,pageCount);
	}
	
	public static List<Lt> getByPageLm_id(int lm_id,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageLm_id(lm_id, conn,tableName,page,pageCount);
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Lt> getByJsrid(int jsrid){
		Connection conn = SK_Config.getConnection();
		return getByJsrid(jsrid, conn);
	}
	
	public static List<Lt> getByJsrid(int jsrid,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByJsrid(jsrid, conn,tableName);
	}
	
	public static List<Lt> getByPageJsrid(int jsrid,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageJsrid(jsrid, conn,page,pageCount);
	}
	
	public static List<Lt> getByPageJsrid(int jsrid,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageJsrid(jsrid, conn,tableName,page,pageCount);
	}
	/**
	 * 根据( 发言人id ) 查询
	 */
	public static List<Lt> getByFyrid(int fyrid){
		Connection conn = SK_Config.getConnection();
		return getByFyrid(fyrid, conn);
	}
	
	public static List<Lt> getByFyrid(int fyrid,String tableName){
		Connection conn = SK_Config.getConnection();
		return getByFyrid(fyrid, conn,tableName);
	}
	
	public static List<Lt> getByPageFyrid(int fyrid,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageFyrid(fyrid, conn,page,pageCount);
	}
	
	public static List<Lt> getByPageFyrid(int fyrid,String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getByPageFyrid(fyrid, conn,tableName,page,pageCount);
	}
	
	//Connection
	/**
	 * 根据( id ) 查询
	 */
	public static Lt getById(int id,Connection conn){
		return getById(id,conn,Lt.TABLENAME);
	}
	
	public static Lt getById(int id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName + " WHERE " + "id = ?";
		Lt lt = null; 
		try {
			Map<String, Object> map = run.query(conn,sql, new MapHandler(), id);
			lt = Lt.createForColumnNameMap(map);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lt;
	}
	
	/**
	 * 根据( 聊天类型_id ) 查询
	 */
	public static List<Lt> getByLtlx_id(int ltlx_id,Connection conn){
		return getByLtlx_id(ltlx_id,conn,Lt.TABLENAME);
	}
	
	public static List<Lt> getByLtlx_id(int ltlx_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName + " WHERE " + "ltlx_id = ?";
		List<Lt> lts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), ltlx_id);
			lts = Lt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lts;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Lt> getByPageLtlx_id(int ltlx_id,Connection conn,int page,int pageCount){
		return getByPageLtlx_id(ltlx_id,conn,Lt.TABLENAME,page,pageCount);
	}
	
	public static List<Lt> getByPageLtlx_id(int ltlx_id,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName + " WHERE " + "ltlx_id = ? LIMIT " + page + " , " +pageCount;
		List<Lt> lts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), ltlx_id);
			lts = Lt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lts;
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Lt> getByLm_id(int lm_id,Connection conn){
		return getByLm_id(lm_id,conn,Lt.TABLENAME);
	}
	
	public static List<Lt> getByLm_id(int lm_id,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName + " WHERE " + "lm_id = ?";
		List<Lt> lts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), lm_id);
			lts = Lt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lts;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Lt> getByPageLm_id(int lm_id,Connection conn,int page,int pageCount){
		return getByPageLm_id(lm_id,conn,Lt.TABLENAME,page,pageCount);
	}
	
	public static List<Lt> getByPageLm_id(int lm_id,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName + " WHERE " + "lm_id = ? LIMIT " + page + " , " +pageCount;
		List<Lt> lts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), lm_id);
			lts = Lt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lts;
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Lt> getByJsrid(int jsrid,Connection conn){
		return getByJsrid(jsrid,conn,Lt.TABLENAME);
	}
	
	public static List<Lt> getByJsrid(int jsrid,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName + " WHERE " + "jsrid = ?";
		List<Lt> lts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), jsrid);
			lts = Lt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lts;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Lt> getByPageJsrid(int jsrid,Connection conn,int page,int pageCount){
		return getByPageJsrid(jsrid,conn,Lt.TABLENAME,page,pageCount);
	}
	
	public static List<Lt> getByPageJsrid(int jsrid,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName + " WHERE " + "jsrid = ? LIMIT " + page + " , " +pageCount;
		List<Lt> lts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), jsrid);
			lts = Lt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lts;
	}
	/**
	 * 根据( 发言人id ) 查询
	 */
	public static List<Lt> getByFyrid(int fyrid,Connection conn){
		return getByFyrid(fyrid,conn,Lt.TABLENAME);
	}
	
	public static List<Lt> getByFyrid(int fyrid,Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName + " WHERE " + "fyrid = ?";
		List<Lt> lts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), fyrid);
			lts = Lt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lts;
	}
	
	//-----------------------------------page-----------------------------------
	public static List<Lt> getByPageFyrid(int fyrid,Connection conn,int page,int pageCount){
		return getByPageFyrid(fyrid,conn,Lt.TABLENAME,page,pageCount);
	}
	
	public static List<Lt> getByPageFyrid(int fyrid,Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName + " WHERE " + "fyrid = ? LIMIT " + page + " , " +pageCount;
		List<Lt> lts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler(), fyrid);
			lts = Lt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lts;
	}
	
	//DataSource
	/**
	 * 根据( id ) 查询
	 */
	public static Lt getById(int id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static Lt getById(int id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getById(id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * 根据( 聊天类型_id ) 查询
	 */
	public static List<Lt> getByLtlx_id(int ltlx_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByLtlx_id(ltlx_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lt> getByLtlx_id(int ltlx_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByLtlx_id(ltlx_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Lt> getByPageLtlx_id(int ltlx_id,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageLtlx_id(ltlx_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lt> getByPageLtlx_id(int ltlx_id,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageLtlx_id(ltlx_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据( 联盟_id ) 查询
	 */
	public static List<Lt> getByLm_id(int lm_id,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByLm_id(lm_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lt> getByLm_id(int lm_id,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByLm_id(lm_id, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Lt> getByPageLm_id(int lm_id,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageLm_id(lm_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lt> getByPageLm_id(int lm_id,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageLm_id(lm_id, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据( 接收人id ) 查询
	 */
	public static List<Lt> getByJsrid(int jsrid,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByJsrid(jsrid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lt> getByJsrid(int jsrid,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByJsrid(jsrid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Lt> getByPageJsrid(int jsrid,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageJsrid(jsrid, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lt> getByPageJsrid(int jsrid,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageJsrid(jsrid, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/**
	 * 根据( 发言人id ) 查询
	 */
	public static List<Lt> getByFyrid(int fyrid,DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getByFyrid(fyrid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lt> getByFyrid(int fyrid,DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getByFyrid(fyrid, conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	//-----------------------------------page-----------------------------------
	public static List<Lt> getByPageFyrid(int fyrid,DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageFyrid(fyrid, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lt> getByPageFyrid(int fyrid,DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getByPageFyrid(fyrid, conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static List<Lt> getAll(){
		Connection conn = SK_Config.getConnection();
		return getAll(conn);
	}
	
	public static List<Lt> getAll(Connection conn){
		return getAll(conn,Lt.TABLENAME);
	}
	
	public static List<Lt> getAll(DataSource ds){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lt> getAll(String tableName){
		Connection conn = SK_Config.getConnection();
		return getAll(conn,tableName);
	}
	
	public static List<Lt> getAll(Connection conn,String tableName){
		QueryRunner run = new QueryRunner();
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName;
		List<Lt> lts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			lts = Lt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lts;
	}
	
	public static List<Lt> getAll(DataSource ds,String tableName){
		try {
			Connection conn = ds.getConnection();
			return getAll(conn,tableName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
	
	public static List<Lt> getAllPage(int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,page,pageCount);
	}
	
	public static List<Lt> getAllPage(Connection conn,int page,int pageCount){
		return getAllPage(conn,Lt.TABLENAME,page,pageCount);
	}
	
	public static List<Lt> getAllPage(DataSource ds,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<Lt> getAllPage(String tableName,int page,int pageCount){
		Connection conn = SK_Config.getConnection();
		return getAllPage(conn,tableName,page,pageCount);
	}
	
	public static List<Lt> getAllPage(Connection conn,String tableName,int page,int pageCount){
		QueryRunner run = new QueryRunner();
		page = ((page-1) * pageCount);
		String sql = "SELECT id,联盟id,内容,聊天类型_id,联盟_id,接收人id,发言人id,创建时间 FROM " + tableName + " LIMIT " + page + " , " +pageCount;
		List<Lt> lts = null; 
		try {
			List<Map<String,Object>> list = run.query(conn, sql, new MapListHandler());
			lts = Lt.createForColumnNameList(list);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try{
				DbUtils.close(conn);
			}catch (Exception e1) {
				e1.printStackTrace();
				return null;
			}
		}
		return lts;
	}
	
	public static List<Lt> getAllPage(DataSource ds,String tableName,int page,int pageCount){
		try {
			Connection conn = ds.getConnection();
			return getAllPage(conn,tableName,page,pageCount);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}		
	}
}