package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.YhCache;
import java.util.Map;
import com.sandking.db.cache.YhxjCache;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 用户陷阱
 */
public class Yhxj {

	public static final String TABLENAME = "用户陷阱";
	public static final String CLASSNAME = "Yhxj"; 
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 陷阱id */
	private int xjid;
	
	/** x */
	private int x;
	
	/** y */
	private int y;
	
	/** 用户_id */
	private int yh_id;
	
	
	public Yhxj() {
		super();
	}
	
	public Yhxj(int id, int xjid, int x, int y, int yh_id) {
		super();
		this.id = id;
		this.xjid = xjid;
		this.x = x;
		this.y = y;
		this.yh_id = yh_id;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public int getXjid() {
		return xjid;
	}
	
	public void setXjid(int xjid) {
		this.xjid = xjid;
		addUpdateColumn("陷阱id",xjid);
	}
	
	public void changeXjidWith(int xjid){
		this.xjid += xjid;
		setXjid(this.xjid);
	}
	
	public void changeXjidWithMin(int xjid,int min){
		this.xjid += xjid;
		this.xjid = this.xjid < min ? min : this.xjid;
		setXjid(this.xjid);
	}
	
	public void changeXjidWithMax(int xjid,int max){
		this.xjid += xjid;
		this.xjid = this.xjid > max ? max : this.xjid;
		setXjid(this.xjid);
	}
	
	public void changeXjidWithMaxMin(int xjid,int max,int min){
		this.xjid += xjid;
		this.xjid = this.xjid < min ? min : this.xjid;
		this.xjid = this.xjid > max ? max : this.xjid;
		setXjid(this.xjid);
	}	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
		addUpdateColumn("x",x);
	}
	
	public void changeXWith(int x){
		this.x += x;
		setX(this.x);
	}
	
	public void changeXWithMin(int x,int min){
		this.x += x;
		this.x = this.x < min ? min : this.x;
		setX(this.x);
	}
	
	public void changeXWithMax(int x,int max){
		this.x += x;
		this.x = this.x > max ? max : this.x;
		setX(this.x);
	}
	
	public void changeXWithMaxMin(int x,int max,int min){
		this.x += x;
		this.x = this.x < min ? min : this.x;
		this.x = this.x > max ? max : this.x;
		setX(this.x);
	}	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
		addUpdateColumn("y",y);
	}
	
	public void changeYWith(int y){
		this.y += y;
		setY(this.y);
	}
	
	public void changeYWithMin(int y,int min){
		this.y += y;
		this.y = this.y < min ? min : this.y;
		setY(this.y);
	}
	
	public void changeYWithMax(int y,int max){
		this.y += y;
		this.y = this.y > max ? max : this.y;
		setY(this.y);
	}
	
	public void changeYWithMaxMin(int y,int max,int min){
		this.y += y;
		this.y = this.y < min ? min : this.y;
		this.y = this.y > max ? max : this.y;
		setY(this.y);
	}	
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
		addUpdateColumn("用户_id",yh_id);
	}
	
	public void changeYh_idWith(int yh_id){
		this.yh_id += yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMin(int yh_id,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMax(int yh_id,int max){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMaxMin(int yh_id,int max,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}	
	
	//用户_id
	public Yh getYhPkYh_id(){
		return YhCache.getById(yh_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("xjid", this.xjid);
        result.put("x", this.x);
        result.put("y", this.y);
        result.put("yh_id", this.yh_id);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("陷阱id", this.xjid);
        result.put("x", this.x);
        result.put("y", this.y);
        result.put("用户_id", this.yh_id);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.xjid);
		    SK_OutputStream.writeInt(out,this.x);
		    SK_OutputStream.writeInt(out,this.y);
		    SK_OutputStream.writeInt(out,this.yh_id);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public Yhxj createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yhxj yhxj = new Yhxj();
		    yhxj.id = SK_InputStream.readInt(in,null);
		    yhxj.xjid = SK_InputStream.readInt(in,null);
		    yhxj.x = SK_InputStream.readInt(in,null);
		    yhxj.y = SK_InputStream.readInt(in,null);
		    yhxj.yh_id = SK_InputStream.readInt(in,null);
		    return yhxj;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhxj> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yhxj> yhxjs = new ArrayList<Yhxj>();
		for (Map<String, Object> map : list) {
			yhxjs.add(createForColumnNameMap(map));
		}
		return yhxjs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhxj createForColumnNameMap(Map<String, Object> map){
    	Yhxj obj = new Yhxj();
	    obj.id = SK_Map.getInt("id", map);
	    obj.xjid = SK_Map.getInt("陷阱id", map);
	    obj.x = SK_Map.getInt("x", map);
	    obj.y = SK_Map.getInt("y", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhxj> createForList(List<Map<String, Object>> list){
    	List<Yhxj> yhxjs = new ArrayList<Yhxj>();
		for (Map<String, Object> map : list) {
			yhxjs.add(createForColumnNameMap(map));
		}
		return yhxjs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhxj createForMap(Map<String, Object> map){
    	Yhxj obj = new Yhxj();
	    obj.id = SK_Map.getInt("id", map);
	    obj.xjid = SK_Map.getInt("xjid", map);
	    obj.x = SK_Map.getInt("x", map);
	    obj.y = SK_Map.getInt("y", map);
	    obj.yh_id = SK_Map.getInt("yh_id", map);
        return obj;
    }
    
    /** 延迟插入数据库 */
    public Yhxj insert(){
    	return YhxjCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yhxj update(){
    	return YhxjCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YhxjCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yhxj insertAndFlush(){
    	return YhxjCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yhxj updateAndFlush(){
    	return YhxjCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YhxjCache.deleteAndFlush(this);
    }
}