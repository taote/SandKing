package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.YhCache;
import com.sandking.db.cache.LtCache;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import com.sandking.db.cache.LmCache;
import java.io.ByteArrayOutputStream;

/**
 * 联盟
 */
public class Lm {

	public static final String TABLENAME = "联盟";
	public static final String CLASSNAME = "Lm"; 
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	/** 创建人id */
	private int cjrid;
	
	
	public Lm() {
		super();
	}
	
	public Lm(int id, String mc, int cjrid) {
		super();
		this.id = id;
		this.mc = mc;
		this.cjrid = cjrid;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
		addUpdateColumn("名称",mc);
	}
	
	public int getCjrid() {
		return cjrid;
	}
	
	public void setCjrid(int cjrid) {
		this.cjrid = cjrid;
		addUpdateColumn("创建人id",cjrid);
	}
	
	public void changeCjridWith(int cjrid){
		this.cjrid += cjrid;
		setCjrid(this.cjrid);
	}
	
	public void changeCjridWithMin(int cjrid,int min){
		this.cjrid += cjrid;
		this.cjrid = this.cjrid < min ? min : this.cjrid;
		setCjrid(this.cjrid);
	}
	
	public void changeCjridWithMax(int cjrid,int max){
		this.cjrid += cjrid;
		this.cjrid = this.cjrid > max ? max : this.cjrid;
		setCjrid(this.cjrid);
	}
	
	public void changeCjridWithMaxMin(int cjrid,int max,int min){
		this.cjrid += cjrid;
		this.cjrid = this.cjrid < min ? min : this.cjrid;
		this.cjrid = this.cjrid > max ? max : this.cjrid;
		setCjrid(this.cjrid);
	}	
	
	//id
	public List<Yh> getYhsFkLm_id(){
		return YhCache.getByLm_id(id);
	}
	
	//id
	public List<Lt> getLtsFkLm_id(){
		return LtCache.getByLm_id(id);
	}
	
	//创建人id
	public Yh getYhPkCjrid(){
		return YhCache.getById(cjrid);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("mc", this.mc);
        result.put("cjrid", this.cjrid);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("名称", this.mc);
        result.put("创建人id", this.cjrid);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeString(out,this.mc);
		    SK_OutputStream.writeInt(out,this.cjrid);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public Lm createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Lm lm = new Lm();
		    lm.id = SK_InputStream.readInt(in,null);
		    lm.mc = SK_InputStream.readString(in,null);
		    lm.cjrid = SK_InputStream.readInt(in,null);
		    return lm;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Lm> createForColumnNameList(List<Map<String, Object>> list){
    	List<Lm> lms = new ArrayList<Lm>();
		for (Map<String, Object> map : list) {
			lms.add(createForColumnNameMap(map));
		}
		return lms;
    }
    
    /**
     * 根据map创建对象
     */
    public static Lm createForColumnNameMap(Map<String, Object> map){
    	Lm obj = new Lm();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
	    obj.cjrid = SK_Map.getInt("创建人id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Lm> createForList(List<Map<String, Object>> list){
    	List<Lm> lms = new ArrayList<Lm>();
		for (Map<String, Object> map : list) {
			lms.add(createForColumnNameMap(map));
		}
		return lms;
    }
    
    /**
     * 根据map创建对象
     */
    public static Lm createForMap(Map<String, Object> map){
    	Lm obj = new Lm();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("mc", map);
	    obj.cjrid = SK_Map.getInt("cjrid", map);
        return obj;
    }
    
    /** 延迟插入数据库 */
    public Lm insert(){
    	return LmCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Lm update(){
    	return LmCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return LmCache.delete(this);
    }
    /** 即时插入数据库 */
    public Lm insertAndFlush(){
    	return LmCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Lm updateAndFlush(){
    	return LmCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return LmCache.deleteAndFlush(this);
    }
}