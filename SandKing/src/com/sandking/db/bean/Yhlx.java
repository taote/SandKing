package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.YhCache;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.YhlxCache;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 用户类型
 */
public class Yhlx {

	public static final String TABLENAME = "用户类型";
	public static final String CLASSNAME = "Yhlx"; 
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	
	public Yhlx() {
		super();
	}
	
	public Yhlx(int id, String mc) {
		super();
		this.id = id;
		this.mc = mc;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
		addUpdateColumn("名称",mc);
	}
	
	
	//id
	public List<Yh> getYhsFkYhlx_id(){
		return YhCache.getByYhlx_id(id);
	}
	
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("mc", this.mc);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("名称", this.mc);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeString(out,this.mc);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public Yhlx createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yhlx yhlx = new Yhlx();
		    yhlx.id = SK_InputStream.readInt(in,null);
		    yhlx.mc = SK_InputStream.readString(in,null);
		    return yhlx;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhlx> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yhlx> yhlxs = new ArrayList<Yhlx>();
		for (Map<String, Object> map : list) {
			yhlxs.add(createForColumnNameMap(map));
		}
		return yhlxs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhlx createForColumnNameMap(Map<String, Object> map){
    	Yhlx obj = new Yhlx();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhlx> createForList(List<Map<String, Object>> list){
    	List<Yhlx> yhlxs = new ArrayList<Yhlx>();
		for (Map<String, Object> map : list) {
			yhlxs.add(createForColumnNameMap(map));
		}
		return yhlxs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhlx createForMap(Map<String, Object> map){
    	Yhlx obj = new Yhlx();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("mc", map);
        return obj;
    }
    
    /** 延迟插入数据库 */
    public Yhlx insert(){
    	return YhlxCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yhlx update(){
    	return YhlxCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YhlxCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yhlx insertAndFlush(){
    	return YhlxCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yhlx updateAndFlush(){
    	return YhlxCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YhlxCache.deleteAndFlush(this);
    }
}