package com.sandking.db.bean;

import java.util.ArrayList;
import com.sandking.tools.SK_Map;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.db.cache.LtlxCache;
import java.util.HashMap;
import com.sandking.db.cache.YhCache;
import com.sandking.db.cache.LtCache;
import com.sandking.io.SK_InputStream;
import java.io.ByteArrayInputStream;
import com.sandking.db.cache.LmCache;
import java.io.ByteArrayOutputStream;

/**
 * 聊天
 */
public class Lt {

	public static final String TABLENAME = "聊天";
	public static final String CLASSNAME = "Lt"; 
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 联盟id */
	private String lmid;
	
	/** 内容 */
	private String nr;
	
	/** 聊天类型_id */
	private int ltlx_id;
	
	/** 联盟_id */
	private int lm_id;
	
	/** 接收人id */
	private int jsrid;
	
	/** 发言人id */
	private int fyrid;
	
	/** 创建时间 */
	private java.util.Date cjsj;
	
	
	public Lt() {
		super();
	}
	
	public Lt(int id, String lmid, String nr, int ltlx_id, int lm_id, int jsrid, int fyrid, java.util.Date cjsj) {
		super();
		this.id = id;
		this.lmid = lmid;
		this.nr = nr;
		this.ltlx_id = ltlx_id;
		this.lm_id = lm_id;
		this.jsrid = jsrid;
		this.fyrid = fyrid;
		this.cjsj = cjsj;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public String getLmid() {
		return lmid;
	}
	
	public void setLmid(String lmid) {
		this.lmid = lmid;
		addUpdateColumn("联盟id",lmid);
	}
	
	public String getNr() {
		return nr;
	}
	
	public void setNr(String nr) {
		this.nr = nr;
		addUpdateColumn("内容",nr);
	}
	
	public int getLtlx_id() {
		return ltlx_id;
	}
	
	public void setLtlx_id(int ltlx_id) {
		this.ltlx_id = ltlx_id;
		addUpdateColumn("聊天类型_id",ltlx_id);
	}
	
	public void changeLtlx_idWith(int ltlx_id){
		this.ltlx_id += ltlx_id;
		setLtlx_id(this.ltlx_id);
	}
	
	public void changeLtlx_idWithMin(int ltlx_id,int min){
		this.ltlx_id += ltlx_id;
		this.ltlx_id = this.ltlx_id < min ? min : this.ltlx_id;
		setLtlx_id(this.ltlx_id);
	}
	
	public void changeLtlx_idWithMax(int ltlx_id,int max){
		this.ltlx_id += ltlx_id;
		this.ltlx_id = this.ltlx_id > max ? max : this.ltlx_id;
		setLtlx_id(this.ltlx_id);
	}
	
	public void changeLtlx_idWithMaxMin(int ltlx_id,int max,int min){
		this.ltlx_id += ltlx_id;
		this.ltlx_id = this.ltlx_id < min ? min : this.ltlx_id;
		this.ltlx_id = this.ltlx_id > max ? max : this.ltlx_id;
		setLtlx_id(this.ltlx_id);
	}	
	public int getLm_id() {
		return lm_id;
	}
	
	public void setLm_id(int lm_id) {
		this.lm_id = lm_id;
		addUpdateColumn("联盟_id",lm_id);
	}
	
	public void changeLm_idWith(int lm_id){
		this.lm_id += lm_id;
		setLm_id(this.lm_id);
	}
	
	public void changeLm_idWithMin(int lm_id,int min){
		this.lm_id += lm_id;
		this.lm_id = this.lm_id < min ? min : this.lm_id;
		setLm_id(this.lm_id);
	}
	
	public void changeLm_idWithMax(int lm_id,int max){
		this.lm_id += lm_id;
		this.lm_id = this.lm_id > max ? max : this.lm_id;
		setLm_id(this.lm_id);
	}
	
	public void changeLm_idWithMaxMin(int lm_id,int max,int min){
		this.lm_id += lm_id;
		this.lm_id = this.lm_id < min ? min : this.lm_id;
		this.lm_id = this.lm_id > max ? max : this.lm_id;
		setLm_id(this.lm_id);
	}	
	public int getJsrid() {
		return jsrid;
	}
	
	public void setJsrid(int jsrid) {
		this.jsrid = jsrid;
		addUpdateColumn("接收人id",jsrid);
	}
	
	public void changeJsridWith(int jsrid){
		this.jsrid += jsrid;
		setJsrid(this.jsrid);
	}
	
	public void changeJsridWithMin(int jsrid,int min){
		this.jsrid += jsrid;
		this.jsrid = this.jsrid < min ? min : this.jsrid;
		setJsrid(this.jsrid);
	}
	
	public void changeJsridWithMax(int jsrid,int max){
		this.jsrid += jsrid;
		this.jsrid = this.jsrid > max ? max : this.jsrid;
		setJsrid(this.jsrid);
	}
	
	public void changeJsridWithMaxMin(int jsrid,int max,int min){
		this.jsrid += jsrid;
		this.jsrid = this.jsrid < min ? min : this.jsrid;
		this.jsrid = this.jsrid > max ? max : this.jsrid;
		setJsrid(this.jsrid);
	}	
	public int getFyrid() {
		return fyrid;
	}
	
	public void setFyrid(int fyrid) {
		this.fyrid = fyrid;
		addUpdateColumn("发言人id",fyrid);
	}
	
	public void changeFyridWith(int fyrid){
		this.fyrid += fyrid;
		setFyrid(this.fyrid);
	}
	
	public void changeFyridWithMin(int fyrid,int min){
		this.fyrid += fyrid;
		this.fyrid = this.fyrid < min ? min : this.fyrid;
		setFyrid(this.fyrid);
	}
	
	public void changeFyridWithMax(int fyrid,int max){
		this.fyrid += fyrid;
		this.fyrid = this.fyrid > max ? max : this.fyrid;
		setFyrid(this.fyrid);
	}
	
	public void changeFyridWithMaxMin(int fyrid,int max,int min){
		this.fyrid += fyrid;
		this.fyrid = this.fyrid < min ? min : this.fyrid;
		this.fyrid = this.fyrid > max ? max : this.fyrid;
		setFyrid(this.fyrid);
	}	
	public java.util.Date getCjsj() {
		return cjsj;
	}
	
	public void setCjsj(java.util.Date cjsj) {
		this.cjsj = cjsj;
		addUpdateColumn("创建时间",cjsj);
	}
	
	
	//接收人id
	public Yh getYhPkJsrid(){
		return YhCache.getById(jsrid);
	}
	//发言人id
	public Yh getYhPkFyrid(){
		return YhCache.getById(fyrid);
	}
	//聊天类型_id
	public Ltlx getLtlxPkLtlx_id(){
		return LtlxCache.getById(ltlx_id);
	}
	//联盟_id
	public Lm getLmPkLm_id(){
		return LmCache.getById(lm_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("lmid", this.lmid);
        result.put("nr", this.nr);
        result.put("ltlx_id", this.ltlx_id);
        result.put("lm_id", this.lm_id);
        result.put("jsrid", this.jsrid);
        result.put("fyrid", this.fyrid);
        result.put("cjsj", this.cjsj);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("联盟id", this.lmid);
        result.put("内容", this.nr);
        result.put("聊天类型_id", this.ltlx_id);
        result.put("联盟_id", this.lm_id);
        result.put("接收人id", this.jsrid);
        result.put("发言人id", this.fyrid);
        result.put("创建时间", this.cjsj);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeString(out,this.lmid);
		    SK_OutputStream.writeString(out,this.nr);
		    SK_OutputStream.writeInt(out,this.ltlx_id);
		    SK_OutputStream.writeInt(out,this.lm_id);
		    SK_OutputStream.writeInt(out,this.jsrid);
		    SK_OutputStream.writeInt(out,this.fyrid);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public Lt createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Lt lt = new Lt();
		    lt.id = SK_InputStream.readInt(in,null);
		    lt.lmid = SK_InputStream.readString(in,null);
		    lt.nr = SK_InputStream.readString(in,null);
		    lt.ltlx_id = SK_InputStream.readInt(in,null);
		    lt.lm_id = SK_InputStream.readInt(in,null);
		    lt.jsrid = SK_InputStream.readInt(in,null);
		    lt.fyrid = SK_InputStream.readInt(in,null);
		    return lt;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Lt> createForColumnNameList(List<Map<String, Object>> list){
    	List<Lt> lts = new ArrayList<Lt>();
		for (Map<String, Object> map : list) {
			lts.add(createForColumnNameMap(map));
		}
		return lts;
    }
    
    /**
     * 根据map创建对象
     */
    public static Lt createForColumnNameMap(Map<String, Object> map){
    	Lt obj = new Lt();
	    obj.id = SK_Map.getInt("id", map);
	    obj.lmid = SK_Map.getString("联盟id", map);
	    obj.nr = SK_Map.getString("内容", map);
	    obj.ltlx_id = SK_Map.getInt("聊天类型_id", map);
	    obj.lm_id = SK_Map.getInt("联盟_id", map);
	    obj.jsrid = SK_Map.getInt("接收人id", map);
	    obj.fyrid = SK_Map.getInt("发言人id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Lt> createForList(List<Map<String, Object>> list){
    	List<Lt> lts = new ArrayList<Lt>();
		for (Map<String, Object> map : list) {
			lts.add(createForColumnNameMap(map));
		}
		return lts;
    }
    
    /**
     * 根据map创建对象
     */
    public static Lt createForMap(Map<String, Object> map){
    	Lt obj = new Lt();
	    obj.id = SK_Map.getInt("id", map);
	    obj.lmid = SK_Map.getString("lmid", map);
	    obj.nr = SK_Map.getString("nr", map);
	    obj.ltlx_id = SK_Map.getInt("ltlx_id", map);
	    obj.lm_id = SK_Map.getInt("lm_id", map);
	    obj.jsrid = SK_Map.getInt("jsrid", map);
	    obj.fyrid = SK_Map.getInt("fyrid", map);
        return obj;
    }
    
    /** 延迟插入数据库 */
    public Lt insert(){
    	return LtCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Lt update(){
    	return LtCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return LtCache.delete(this);
    }
    /** 即时插入数据库 */
    public Lt insertAndFlush(){
    	return LtCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Lt updateAndFlush(){
    	return LtCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return LtCache.deleteAndFlush(this);
    }
}