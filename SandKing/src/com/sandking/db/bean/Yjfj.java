package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.db.cache.YjlxCache;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.YjfjCache;
import com.sandking.db.cache.YhyjCache;
import java.io.ByteArrayInputStream;
import com.sandking.db.cache.FjlxCache;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 邮件附件
 */
public class Yjfj {

	public static final String TABLENAME = "邮件附件";
	public static final String CLASSNAME = "Yjfj"; 
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 附件id */
	private int fjid;
	
	/** 邮件类型_id */
	private int yjlx_id;
	
	/** 用户邮件_id */
	private int yhyj_id;
	
	/** 附件类型_id */
	private int fjlx_id;
	
	/** 附件数量 */
	private int fjsl;
	
	
	public Yjfj() {
		super();
	}
	
	public Yjfj(int id, int fjid, int yjlx_id, int yhyj_id, int fjlx_id, int fjsl) {
		super();
		this.id = id;
		this.fjid = fjid;
		this.yjlx_id = yjlx_id;
		this.yhyj_id = yhyj_id;
		this.fjlx_id = fjlx_id;
		this.fjsl = fjsl;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public int getFjid() {
		return fjid;
	}
	
	public void setFjid(int fjid) {
		this.fjid = fjid;
		addUpdateColumn("附件id",fjid);
	}
	
	public void changeFjidWith(int fjid){
		this.fjid += fjid;
		setFjid(this.fjid);
	}
	
	public void changeFjidWithMin(int fjid,int min){
		this.fjid += fjid;
		this.fjid = this.fjid < min ? min : this.fjid;
		setFjid(this.fjid);
	}
	
	public void changeFjidWithMax(int fjid,int max){
		this.fjid += fjid;
		this.fjid = this.fjid > max ? max : this.fjid;
		setFjid(this.fjid);
	}
	
	public void changeFjidWithMaxMin(int fjid,int max,int min){
		this.fjid += fjid;
		this.fjid = this.fjid < min ? min : this.fjid;
		this.fjid = this.fjid > max ? max : this.fjid;
		setFjid(this.fjid);
	}	
	public int getYjlx_id() {
		return yjlx_id;
	}
	
	public void setYjlx_id(int yjlx_id) {
		this.yjlx_id = yjlx_id;
		addUpdateColumn("邮件类型_id",yjlx_id);
	}
	
	public void changeYjlx_idWith(int yjlx_id){
		this.yjlx_id += yjlx_id;
		setYjlx_id(this.yjlx_id);
	}
	
	public void changeYjlx_idWithMin(int yjlx_id,int min){
		this.yjlx_id += yjlx_id;
		this.yjlx_id = this.yjlx_id < min ? min : this.yjlx_id;
		setYjlx_id(this.yjlx_id);
	}
	
	public void changeYjlx_idWithMax(int yjlx_id,int max){
		this.yjlx_id += yjlx_id;
		this.yjlx_id = this.yjlx_id > max ? max : this.yjlx_id;
		setYjlx_id(this.yjlx_id);
	}
	
	public void changeYjlx_idWithMaxMin(int yjlx_id,int max,int min){
		this.yjlx_id += yjlx_id;
		this.yjlx_id = this.yjlx_id < min ? min : this.yjlx_id;
		this.yjlx_id = this.yjlx_id > max ? max : this.yjlx_id;
		setYjlx_id(this.yjlx_id);
	}	
	public int getYhyj_id() {
		return yhyj_id;
	}
	
	public void setYhyj_id(int yhyj_id) {
		this.yhyj_id = yhyj_id;
		addUpdateColumn("用户邮件_id",yhyj_id);
	}
	
	public void changeYhyj_idWith(int yhyj_id){
		this.yhyj_id += yhyj_id;
		setYhyj_id(this.yhyj_id);
	}
	
	public void changeYhyj_idWithMin(int yhyj_id,int min){
		this.yhyj_id += yhyj_id;
		this.yhyj_id = this.yhyj_id < min ? min : this.yhyj_id;
		setYhyj_id(this.yhyj_id);
	}
	
	public void changeYhyj_idWithMax(int yhyj_id,int max){
		this.yhyj_id += yhyj_id;
		this.yhyj_id = this.yhyj_id > max ? max : this.yhyj_id;
		setYhyj_id(this.yhyj_id);
	}
	
	public void changeYhyj_idWithMaxMin(int yhyj_id,int max,int min){
		this.yhyj_id += yhyj_id;
		this.yhyj_id = this.yhyj_id < min ? min : this.yhyj_id;
		this.yhyj_id = this.yhyj_id > max ? max : this.yhyj_id;
		setYhyj_id(this.yhyj_id);
	}	
	public int getFjlx_id() {
		return fjlx_id;
	}
	
	public void setFjlx_id(int fjlx_id) {
		this.fjlx_id = fjlx_id;
		addUpdateColumn("附件类型_id",fjlx_id);
	}
	
	public void changeFjlx_idWith(int fjlx_id){
		this.fjlx_id += fjlx_id;
		setFjlx_id(this.fjlx_id);
	}
	
	public void changeFjlx_idWithMin(int fjlx_id,int min){
		this.fjlx_id += fjlx_id;
		this.fjlx_id = this.fjlx_id < min ? min : this.fjlx_id;
		setFjlx_id(this.fjlx_id);
	}
	
	public void changeFjlx_idWithMax(int fjlx_id,int max){
		this.fjlx_id += fjlx_id;
		this.fjlx_id = this.fjlx_id > max ? max : this.fjlx_id;
		setFjlx_id(this.fjlx_id);
	}
	
	public void changeFjlx_idWithMaxMin(int fjlx_id,int max,int min){
		this.fjlx_id += fjlx_id;
		this.fjlx_id = this.fjlx_id < min ? min : this.fjlx_id;
		this.fjlx_id = this.fjlx_id > max ? max : this.fjlx_id;
		setFjlx_id(this.fjlx_id);
	}	
	public int getFjsl() {
		return fjsl;
	}
	
	public void setFjsl(int fjsl) {
		this.fjsl = fjsl;
		addUpdateColumn("附件数量",fjsl);
	}
	
	public void changeFjslWith(int fjsl){
		this.fjsl += fjsl;
		setFjsl(this.fjsl);
	}
	
	public void changeFjslWithMin(int fjsl,int min){
		this.fjsl += fjsl;
		this.fjsl = this.fjsl < min ? min : this.fjsl;
		setFjsl(this.fjsl);
	}
	
	public void changeFjslWithMax(int fjsl,int max){
		this.fjsl += fjsl;
		this.fjsl = this.fjsl > max ? max : this.fjsl;
		setFjsl(this.fjsl);
	}
	
	public void changeFjslWithMaxMin(int fjsl,int max,int min){
		this.fjsl += fjsl;
		this.fjsl = this.fjsl < min ? min : this.fjsl;
		this.fjsl = this.fjsl > max ? max : this.fjsl;
		setFjsl(this.fjsl);
	}	
	
	//用户邮件_id
	public Yhyj getYhyjPkYhyj_id(){
		return YhyjCache.getById(yhyj_id);
	}
	//邮件类型_id
	public Yjlx getYjlxPkYjlx_id(){
		return YjlxCache.getById(yjlx_id);
	}
	//附件类型_id
	public Fjlx getFjlxPkFjlx_id(){
		return FjlxCache.getById(fjlx_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("fjid", this.fjid);
        result.put("yjlx_id", this.yjlx_id);
        result.put("yhyj_id", this.yhyj_id);
        result.put("fjlx_id", this.fjlx_id);
        result.put("fjsl", this.fjsl);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("附件id", this.fjid);
        result.put("邮件类型_id", this.yjlx_id);
        result.put("用户邮件_id", this.yhyj_id);
        result.put("附件类型_id", this.fjlx_id);
        result.put("附件数量", this.fjsl);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.fjid);
		    SK_OutputStream.writeInt(out,this.yjlx_id);
		    SK_OutputStream.writeInt(out,this.yhyj_id);
		    SK_OutputStream.writeInt(out,this.fjlx_id);
		    SK_OutputStream.writeInt(out,this.fjsl);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public Yjfj createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yjfj yjfj = new Yjfj();
		    yjfj.id = SK_InputStream.readInt(in,null);
		    yjfj.fjid = SK_InputStream.readInt(in,null);
		    yjfj.yjlx_id = SK_InputStream.readInt(in,null);
		    yjfj.yhyj_id = SK_InputStream.readInt(in,null);
		    yjfj.fjlx_id = SK_InputStream.readInt(in,null);
		    yjfj.fjsl = SK_InputStream.readInt(in,null);
		    return yjfj;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yjfj> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		for (Map<String, Object> map : list) {
			yjfjs.add(createForColumnNameMap(map));
		}
		return yjfjs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yjfj createForColumnNameMap(Map<String, Object> map){
    	Yjfj obj = new Yjfj();
	    obj.id = SK_Map.getInt("id", map);
	    obj.fjid = SK_Map.getInt("附件id", map);
	    obj.yjlx_id = SK_Map.getInt("邮件类型_id", map);
	    obj.yhyj_id = SK_Map.getInt("用户邮件_id", map);
	    obj.fjlx_id = SK_Map.getInt("附件类型_id", map);
	    obj.fjsl = SK_Map.getInt("附件数量", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yjfj> createForList(List<Map<String, Object>> list){
    	List<Yjfj> yjfjs = new ArrayList<Yjfj>();
		for (Map<String, Object> map : list) {
			yjfjs.add(createForColumnNameMap(map));
		}
		return yjfjs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yjfj createForMap(Map<String, Object> map){
    	Yjfj obj = new Yjfj();
	    obj.id = SK_Map.getInt("id", map);
	    obj.fjid = SK_Map.getInt("fjid", map);
	    obj.yjlx_id = SK_Map.getInt("yjlx_id", map);
	    obj.yhyj_id = SK_Map.getInt("yhyj_id", map);
	    obj.fjlx_id = SK_Map.getInt("fjlx_id", map);
	    obj.fjsl = SK_Map.getInt("fjsl", map);
        return obj;
    }
    
    /** 延迟插入数据库 */
    public Yjfj insert(){
    	return YjfjCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yjfj update(){
    	return YjfjCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YjfjCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yjfj insertAndFlush(){
    	return YjfjCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yjfj updateAndFlush(){
    	return YjfjCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YjfjCache.deleteAndFlush(this);
    }
}