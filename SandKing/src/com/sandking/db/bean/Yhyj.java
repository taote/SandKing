package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.YhCache;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.YjfjCache;
import com.sandking.db.cache.YhyjCache;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 用户邮件
 */
public class Yhyj {

	public static final String TABLENAME = "用户邮件";
	public static final String CLASSNAME = "Yhyj"; 
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 创建时间 */
	private java.util.Date cjsj;
	
	/** 读取时间 */
	private java.util.Date dqsj;
	
	/** 是否读取 */
	private java.util.Date sfdq;
	
	/** 发件人id */
	private int fjrid;
	
	/** 接收人id */
	private int jsrid;
	
	
	public Yhyj() {
		super();
	}
	
	public Yhyj(int id, java.util.Date cjsj, java.util.Date dqsj, java.util.Date sfdq, int fjrid, int jsrid) {
		super();
		this.id = id;
		this.cjsj = cjsj;
		this.dqsj = dqsj;
		this.sfdq = sfdq;
		this.fjrid = fjrid;
		this.jsrid = jsrid;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public java.util.Date getCjsj() {
		return cjsj;
	}
	
	public void setCjsj(java.util.Date cjsj) {
		this.cjsj = cjsj;
		addUpdateColumn("创建时间",cjsj);
	}
	
	public java.util.Date getDqsj() {
		return dqsj;
	}
	
	public void setDqsj(java.util.Date dqsj) {
		this.dqsj = dqsj;
		addUpdateColumn("读取时间",dqsj);
	}
	
	public java.util.Date getSfdq() {
		return sfdq;
	}
	
	public void setSfdq(java.util.Date sfdq) {
		this.sfdq = sfdq;
		addUpdateColumn("是否读取",sfdq);
	}
	
	public int getFjrid() {
		return fjrid;
	}
	
	public void setFjrid(int fjrid) {
		this.fjrid = fjrid;
		addUpdateColumn("发件人id",fjrid);
	}
	
	public void changeFjridWith(int fjrid){
		this.fjrid += fjrid;
		setFjrid(this.fjrid);
	}
	
	public void changeFjridWithMin(int fjrid,int min){
		this.fjrid += fjrid;
		this.fjrid = this.fjrid < min ? min : this.fjrid;
		setFjrid(this.fjrid);
	}
	
	public void changeFjridWithMax(int fjrid,int max){
		this.fjrid += fjrid;
		this.fjrid = this.fjrid > max ? max : this.fjrid;
		setFjrid(this.fjrid);
	}
	
	public void changeFjridWithMaxMin(int fjrid,int max,int min){
		this.fjrid += fjrid;
		this.fjrid = this.fjrid < min ? min : this.fjrid;
		this.fjrid = this.fjrid > max ? max : this.fjrid;
		setFjrid(this.fjrid);
	}	
	public int getJsrid() {
		return jsrid;
	}
	
	public void setJsrid(int jsrid) {
		this.jsrid = jsrid;
		addUpdateColumn("接收人id",jsrid);
	}
	
	public void changeJsridWith(int jsrid){
		this.jsrid += jsrid;
		setJsrid(this.jsrid);
	}
	
	public void changeJsridWithMin(int jsrid,int min){
		this.jsrid += jsrid;
		this.jsrid = this.jsrid < min ? min : this.jsrid;
		setJsrid(this.jsrid);
	}
	
	public void changeJsridWithMax(int jsrid,int max){
		this.jsrid += jsrid;
		this.jsrid = this.jsrid > max ? max : this.jsrid;
		setJsrid(this.jsrid);
	}
	
	public void changeJsridWithMaxMin(int jsrid,int max,int min){
		this.jsrid += jsrid;
		this.jsrid = this.jsrid < min ? min : this.jsrid;
		this.jsrid = this.jsrid > max ? max : this.jsrid;
		setJsrid(this.jsrid);
	}	
	
	//id
	public List<Yjfj> getYjfjsFkYhyj_id(){
		return YjfjCache.getByYhyj_id(id);
	}
	
	//发件人id
	public Yh getYhPkFjrid(){
		return YhCache.getById(fjrid);
	}
	//接收人id
	public Yh getYhPkJsrid(){
		return YhCache.getById(jsrid);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("cjsj", this.cjsj);
        result.put("dqsj", this.dqsj);
        result.put("sfdq", this.sfdq);
        result.put("fjrid", this.fjrid);
        result.put("jsrid", this.jsrid);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("创建时间", this.cjsj);
        result.put("读取时间", this.dqsj);
        result.put("是否读取", this.sfdq);
        result.put("发件人id", this.fjrid);
        result.put("接收人id", this.jsrid);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.fjrid);
		    SK_OutputStream.writeInt(out,this.jsrid);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public Yhyj createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yhyj yhyj = new Yhyj();
		    yhyj.id = SK_InputStream.readInt(in,null);
		    yhyj.fjrid = SK_InputStream.readInt(in,null);
		    yhyj.jsrid = SK_InputStream.readInt(in,null);
		    return yhyj;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhyj> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		for (Map<String, Object> map : list) {
			yhyjs.add(createForColumnNameMap(map));
		}
		return yhyjs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhyj createForColumnNameMap(Map<String, Object> map){
    	Yhyj obj = new Yhyj();
	    obj.id = SK_Map.getInt("id", map);
	    obj.fjrid = SK_Map.getInt("发件人id", map);
	    obj.jsrid = SK_Map.getInt("接收人id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhyj> createForList(List<Map<String, Object>> list){
    	List<Yhyj> yhyjs = new ArrayList<Yhyj>();
		for (Map<String, Object> map : list) {
			yhyjs.add(createForColumnNameMap(map));
		}
		return yhyjs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhyj createForMap(Map<String, Object> map){
    	Yhyj obj = new Yhyj();
	    obj.id = SK_Map.getInt("id", map);
	    obj.fjrid = SK_Map.getInt("fjrid", map);
	    obj.jsrid = SK_Map.getInt("jsrid", map);
        return obj;
    }
    
    /** 延迟插入数据库 */
    public Yhyj insert(){
    	return YhyjCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yhyj update(){
    	return YhyjCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YhyjCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yhyj insertAndFlush(){
    	return YhyjCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yhyj updateAndFlush(){
    	return YhyjCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YhyjCache.deleteAndFlush(this);
    }
}