package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import java.util.Map;
import com.sandking.db.cache.YhxjCache;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.db.cache.FwqCache;
import com.sandking.db.cache.YhlxCache;
import com.sandking.db.cache.YhjzCache;
import java.util.HashMap;
import com.sandking.db.cache.YhzaCache;
import com.sandking.db.cache.YhCache;
import com.sandking.db.cache.LtCache;
import com.sandking.db.cache.YhzsCache;
import com.sandking.db.cache.YhysCache;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.YhyxCache;
import com.sandking.db.cache.YhyjCache;
import com.sandking.db.cache.YhbbCache;
import java.io.ByteArrayInputStream;
import com.sandking.db.cache.LmCache;
import java.io.ByteArrayOutputStream;

/**
 * 用户
 */
public class Yh {

	public static final String TABLENAME = "用户";
	public static final String CLASSNAME = "Yh"; 
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 昵称 */
	private String nc;
	
	/** 创建时间 */
	private java.util.Date cjsj;
	
	/** 最后登录时间 */
	private java.util.Date zhdlsj;
	
	/** 语言 */
	private String yy;
	
	/** 账号 */
	private String zh;
	
	/** 密码 */
	private String mm;
	
	/** 宝石 */
	private int bs;
	
	/** 假宝石 */
	private int jbs;
	
	/** 金币 */
	private int jb;
	
	/** 水 */
	private int s;
	
	/** 修炼点 */
	private int xld;
	
	/** 设备 */
	private String sb;
	
	/** 设备系统 */
	private String sbxt;
	
	/** 分辨率 */
	private String fbl;
	
	/** 渠道 */
	private String qd;
	
	/** 用户类型_id */
	private int yhlx_id;
	
	/** 用户lvl */
	private int yhlvl;
	
	/** 用户lvl_exp */
	private int yhlvl_exp;
	
	/** VIP */
	private int vIP;
	
	/** VIP_exp */
	private int vIP_exp;
	
	/** 充值金额 */
	private int czje;
	
	/** 幸运 */
	private int xy;
	
	/** 客户端版本 */
	private String khdbb;
	
	/** 服务器_id */
	private int fwq_id;
	
	/** 主城lvl */
	private int zclvl;
	
	/** 锁定 */
	private boolean sd;
	
	/** 联盟_id */
	private int lm_id;
	
	
	public Yh() {
		super();
	}
	
	public Yh(int id, String nc, java.util.Date cjsj, java.util.Date zhdlsj, String yy, String zh, String mm, int bs, int jbs, int jb, int s, int xld, String sb, String sbxt, String fbl, String qd, int yhlx_id, int yhlvl, int yhlvl_exp, int vIP, int vIP_exp, int czje, int xy, String khdbb, int fwq_id, int zclvl, boolean sd, int lm_id) {
		super();
		this.id = id;
		this.nc = nc;
		this.cjsj = cjsj;
		this.zhdlsj = zhdlsj;
		this.yy = yy;
		this.zh = zh;
		this.mm = mm;
		this.bs = bs;
		this.jbs = jbs;
		this.jb = jb;
		this.s = s;
		this.xld = xld;
		this.sb = sb;
		this.sbxt = sbxt;
		this.fbl = fbl;
		this.qd = qd;
		this.yhlx_id = yhlx_id;
		this.yhlvl = yhlvl;
		this.yhlvl_exp = yhlvl_exp;
		this.vIP = vIP;
		this.vIP_exp = vIP_exp;
		this.czje = czje;
		this.xy = xy;
		this.khdbb = khdbb;
		this.fwq_id = fwq_id;
		this.zclvl = zclvl;
		this.sd = sd;
		this.lm_id = lm_id;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public String getNc() {
		return nc;
	}
	
	public void setNc(String nc) {
		this.nc = nc;
		addUpdateColumn("昵称",nc);
	}
	
	public java.util.Date getCjsj() {
		return cjsj;
	}
	
	public void setCjsj(java.util.Date cjsj) {
		this.cjsj = cjsj;
		addUpdateColumn("创建时间",cjsj);
	}
	
	public java.util.Date getZhdlsj() {
		return zhdlsj;
	}
	
	public void setZhdlsj(java.util.Date zhdlsj) {
		this.zhdlsj = zhdlsj;
		addUpdateColumn("最后登录时间",zhdlsj);
	}
	
	public String getYy() {
		return yy;
	}
	
	public void setYy(String yy) {
		this.yy = yy;
		addUpdateColumn("语言",yy);
	}
	
	public String getZh() {
		return zh;
	}
	
	public void setZh(String zh) {
		this.zh = zh;
		addUpdateColumn("账号",zh);
	}
	
	public String getMm() {
		return mm;
	}
	
	public void setMm(String mm) {
		this.mm = mm;
		addUpdateColumn("密码",mm);
	}
	
	public int getBs() {
		return bs;
	}
	
	public void setBs(int bs) {
		this.bs = bs;
		addUpdateColumn("宝石",bs);
	}
	
	public void changeBsWith(int bs){
		this.bs += bs;
		setBs(this.bs);
	}
	
	public void changeBsWithMin(int bs,int min){
		this.bs += bs;
		this.bs = this.bs < min ? min : this.bs;
		setBs(this.bs);
	}
	
	public void changeBsWithMax(int bs,int max){
		this.bs += bs;
		this.bs = this.bs > max ? max : this.bs;
		setBs(this.bs);
	}
	
	public void changeBsWithMaxMin(int bs,int max,int min){
		this.bs += bs;
		this.bs = this.bs < min ? min : this.bs;
		this.bs = this.bs > max ? max : this.bs;
		setBs(this.bs);
	}	
	public int getJbs() {
		return jbs;
	}
	
	public void setJbs(int jbs) {
		this.jbs = jbs;
		addUpdateColumn("假宝石",jbs);
	}
	
	public void changeJbsWith(int jbs){
		this.jbs += jbs;
		setJbs(this.jbs);
	}
	
	public void changeJbsWithMin(int jbs,int min){
		this.jbs += jbs;
		this.jbs = this.jbs < min ? min : this.jbs;
		setJbs(this.jbs);
	}
	
	public void changeJbsWithMax(int jbs,int max){
		this.jbs += jbs;
		this.jbs = this.jbs > max ? max : this.jbs;
		setJbs(this.jbs);
	}
	
	public void changeJbsWithMaxMin(int jbs,int max,int min){
		this.jbs += jbs;
		this.jbs = this.jbs < min ? min : this.jbs;
		this.jbs = this.jbs > max ? max : this.jbs;
		setJbs(this.jbs);
	}	
	public int getJb() {
		return jb;
	}
	
	public void setJb(int jb) {
		this.jb = jb;
		addUpdateColumn("金币",jb);
	}
	
	public void changeJbWith(int jb){
		this.jb += jb;
		setJb(this.jb);
	}
	
	public void changeJbWithMin(int jb,int min){
		this.jb += jb;
		this.jb = this.jb < min ? min : this.jb;
		setJb(this.jb);
	}
	
	public void changeJbWithMax(int jb,int max){
		this.jb += jb;
		this.jb = this.jb > max ? max : this.jb;
		setJb(this.jb);
	}
	
	public void changeJbWithMaxMin(int jb,int max,int min){
		this.jb += jb;
		this.jb = this.jb < min ? min : this.jb;
		this.jb = this.jb > max ? max : this.jb;
		setJb(this.jb);
	}	
	public int getS() {
		return s;
	}
	
	public void setS(int s) {
		this.s = s;
		addUpdateColumn("水",s);
	}
	
	public void changeSWith(int s){
		this.s += s;
		setS(this.s);
	}
	
	public void changeSWithMin(int s,int min){
		this.s += s;
		this.s = this.s < min ? min : this.s;
		setS(this.s);
	}
	
	public void changeSWithMax(int s,int max){
		this.s += s;
		this.s = this.s > max ? max : this.s;
		setS(this.s);
	}
	
	public void changeSWithMaxMin(int s,int max,int min){
		this.s += s;
		this.s = this.s < min ? min : this.s;
		this.s = this.s > max ? max : this.s;
		setS(this.s);
	}	
	public int getXld() {
		return xld;
	}
	
	public void setXld(int xld) {
		this.xld = xld;
		addUpdateColumn("修炼点",xld);
	}
	
	public void changeXldWith(int xld){
		this.xld += xld;
		setXld(this.xld);
	}
	
	public void changeXldWithMin(int xld,int min){
		this.xld += xld;
		this.xld = this.xld < min ? min : this.xld;
		setXld(this.xld);
	}
	
	public void changeXldWithMax(int xld,int max){
		this.xld += xld;
		this.xld = this.xld > max ? max : this.xld;
		setXld(this.xld);
	}
	
	public void changeXldWithMaxMin(int xld,int max,int min){
		this.xld += xld;
		this.xld = this.xld < min ? min : this.xld;
		this.xld = this.xld > max ? max : this.xld;
		setXld(this.xld);
	}	
	public String getSb() {
		return sb;
	}
	
	public void setSb(String sb) {
		this.sb = sb;
		addUpdateColumn("设备",sb);
	}
	
	public String getSbxt() {
		return sbxt;
	}
	
	public void setSbxt(String sbxt) {
		this.sbxt = sbxt;
		addUpdateColumn("设备系统",sbxt);
	}
	
	public String getFbl() {
		return fbl;
	}
	
	public void setFbl(String fbl) {
		this.fbl = fbl;
		addUpdateColumn("分辨率",fbl);
	}
	
	public String getQd() {
		return qd;
	}
	
	public void setQd(String qd) {
		this.qd = qd;
		addUpdateColumn("渠道",qd);
	}
	
	public int getYhlx_id() {
		return yhlx_id;
	}
	
	public void setYhlx_id(int yhlx_id) {
		this.yhlx_id = yhlx_id;
		addUpdateColumn("用户类型_id",yhlx_id);
	}
	
	public void changeYhlx_idWith(int yhlx_id){
		this.yhlx_id += yhlx_id;
		setYhlx_id(this.yhlx_id);
	}
	
	public void changeYhlx_idWithMin(int yhlx_id,int min){
		this.yhlx_id += yhlx_id;
		this.yhlx_id = this.yhlx_id < min ? min : this.yhlx_id;
		setYhlx_id(this.yhlx_id);
	}
	
	public void changeYhlx_idWithMax(int yhlx_id,int max){
		this.yhlx_id += yhlx_id;
		this.yhlx_id = this.yhlx_id > max ? max : this.yhlx_id;
		setYhlx_id(this.yhlx_id);
	}
	
	public void changeYhlx_idWithMaxMin(int yhlx_id,int max,int min){
		this.yhlx_id += yhlx_id;
		this.yhlx_id = this.yhlx_id < min ? min : this.yhlx_id;
		this.yhlx_id = this.yhlx_id > max ? max : this.yhlx_id;
		setYhlx_id(this.yhlx_id);
	}	
	public int getYhlvl() {
		return yhlvl;
	}
	
	public void setYhlvl(int yhlvl) {
		this.yhlvl = yhlvl;
		addUpdateColumn("用户lvl",yhlvl);
	}
	
	public void changeYhlvlWith(int yhlvl){
		this.yhlvl += yhlvl;
		setYhlvl(this.yhlvl);
	}
	
	public void changeYhlvlWithMin(int yhlvl,int min){
		this.yhlvl += yhlvl;
		this.yhlvl = this.yhlvl < min ? min : this.yhlvl;
		setYhlvl(this.yhlvl);
	}
	
	public void changeYhlvlWithMax(int yhlvl,int max){
		this.yhlvl += yhlvl;
		this.yhlvl = this.yhlvl > max ? max : this.yhlvl;
		setYhlvl(this.yhlvl);
	}
	
	public void changeYhlvlWithMaxMin(int yhlvl,int max,int min){
		this.yhlvl += yhlvl;
		this.yhlvl = this.yhlvl < min ? min : this.yhlvl;
		this.yhlvl = this.yhlvl > max ? max : this.yhlvl;
		setYhlvl(this.yhlvl);
	}	
	public int getYhlvl_exp() {
		return yhlvl_exp;
	}
	
	public void setYhlvl_exp(int yhlvl_exp) {
		this.yhlvl_exp = yhlvl_exp;
		addUpdateColumn("用户lvl_exp",yhlvl_exp);
	}
	
	public void changeYhlvl_expWith(int yhlvl_exp){
		this.yhlvl_exp += yhlvl_exp;
		setYhlvl_exp(this.yhlvl_exp);
	}
	
	public void changeYhlvl_expWithMin(int yhlvl_exp,int min){
		this.yhlvl_exp += yhlvl_exp;
		this.yhlvl_exp = this.yhlvl_exp < min ? min : this.yhlvl_exp;
		setYhlvl_exp(this.yhlvl_exp);
	}
	
	public void changeYhlvl_expWithMax(int yhlvl_exp,int max){
		this.yhlvl_exp += yhlvl_exp;
		this.yhlvl_exp = this.yhlvl_exp > max ? max : this.yhlvl_exp;
		setYhlvl_exp(this.yhlvl_exp);
	}
	
	public void changeYhlvl_expWithMaxMin(int yhlvl_exp,int max,int min){
		this.yhlvl_exp += yhlvl_exp;
		this.yhlvl_exp = this.yhlvl_exp < min ? min : this.yhlvl_exp;
		this.yhlvl_exp = this.yhlvl_exp > max ? max : this.yhlvl_exp;
		setYhlvl_exp(this.yhlvl_exp);
	}	
	public int getVIP() {
		return vIP;
	}
	
	public void setVIP(int vIP) {
		this.vIP = vIP;
		addUpdateColumn("VIP",vIP);
	}
	
	public void changeVIPWith(int vIP){
		this.vIP += vIP;
		setVIP(this.vIP);
	}
	
	public void changeVIPWithMin(int vIP,int min){
		this.vIP += vIP;
		this.vIP = this.vIP < min ? min : this.vIP;
		setVIP(this.vIP);
	}
	
	public void changeVIPWithMax(int vIP,int max){
		this.vIP += vIP;
		this.vIP = this.vIP > max ? max : this.vIP;
		setVIP(this.vIP);
	}
	
	public void changeVIPWithMaxMin(int vIP,int max,int min){
		this.vIP += vIP;
		this.vIP = this.vIP < min ? min : this.vIP;
		this.vIP = this.vIP > max ? max : this.vIP;
		setVIP(this.vIP);
	}	
	public int getVIP_exp() {
		return vIP_exp;
	}
	
	public void setVIP_exp(int vIP_exp) {
		this.vIP_exp = vIP_exp;
		addUpdateColumn("VIP_exp",vIP_exp);
	}
	
	public void changeVIP_expWith(int vIP_exp){
		this.vIP_exp += vIP_exp;
		setVIP_exp(this.vIP_exp);
	}
	
	public void changeVIP_expWithMin(int vIP_exp,int min){
		this.vIP_exp += vIP_exp;
		this.vIP_exp = this.vIP_exp < min ? min : this.vIP_exp;
		setVIP_exp(this.vIP_exp);
	}
	
	public void changeVIP_expWithMax(int vIP_exp,int max){
		this.vIP_exp += vIP_exp;
		this.vIP_exp = this.vIP_exp > max ? max : this.vIP_exp;
		setVIP_exp(this.vIP_exp);
	}
	
	public void changeVIP_expWithMaxMin(int vIP_exp,int max,int min){
		this.vIP_exp += vIP_exp;
		this.vIP_exp = this.vIP_exp < min ? min : this.vIP_exp;
		this.vIP_exp = this.vIP_exp > max ? max : this.vIP_exp;
		setVIP_exp(this.vIP_exp);
	}	
	public int getCzje() {
		return czje;
	}
	
	public void setCzje(int czje) {
		this.czje = czje;
		addUpdateColumn("充值金额",czje);
	}
	
	public void changeCzjeWith(int czje){
		this.czje += czje;
		setCzje(this.czje);
	}
	
	public void changeCzjeWithMin(int czje,int min){
		this.czje += czje;
		this.czje = this.czje < min ? min : this.czje;
		setCzje(this.czje);
	}
	
	public void changeCzjeWithMax(int czje,int max){
		this.czje += czje;
		this.czje = this.czje > max ? max : this.czje;
		setCzje(this.czje);
	}
	
	public void changeCzjeWithMaxMin(int czje,int max,int min){
		this.czje += czje;
		this.czje = this.czje < min ? min : this.czje;
		this.czje = this.czje > max ? max : this.czje;
		setCzje(this.czje);
	}	
	public int getXy() {
		return xy;
	}
	
	public void setXy(int xy) {
		this.xy = xy;
		addUpdateColumn("幸运",xy);
	}
	
	public void changeXyWith(int xy){
		this.xy += xy;
		setXy(this.xy);
	}
	
	public void changeXyWithMin(int xy,int min){
		this.xy += xy;
		this.xy = this.xy < min ? min : this.xy;
		setXy(this.xy);
	}
	
	public void changeXyWithMax(int xy,int max){
		this.xy += xy;
		this.xy = this.xy > max ? max : this.xy;
		setXy(this.xy);
	}
	
	public void changeXyWithMaxMin(int xy,int max,int min){
		this.xy += xy;
		this.xy = this.xy < min ? min : this.xy;
		this.xy = this.xy > max ? max : this.xy;
		setXy(this.xy);
	}	
	public String getKhdbb() {
		return khdbb;
	}
	
	public void setKhdbb(String khdbb) {
		this.khdbb = khdbb;
		addUpdateColumn("客户端版本",khdbb);
	}
	
	public int getFwq_id() {
		return fwq_id;
	}
	
	public void setFwq_id(int fwq_id) {
		this.fwq_id = fwq_id;
		addUpdateColumn("服务器_id",fwq_id);
	}
	
	public void changeFwq_idWith(int fwq_id){
		this.fwq_id += fwq_id;
		setFwq_id(this.fwq_id);
	}
	
	public void changeFwq_idWithMin(int fwq_id,int min){
		this.fwq_id += fwq_id;
		this.fwq_id = this.fwq_id < min ? min : this.fwq_id;
		setFwq_id(this.fwq_id);
	}
	
	public void changeFwq_idWithMax(int fwq_id,int max){
		this.fwq_id += fwq_id;
		this.fwq_id = this.fwq_id > max ? max : this.fwq_id;
		setFwq_id(this.fwq_id);
	}
	
	public void changeFwq_idWithMaxMin(int fwq_id,int max,int min){
		this.fwq_id += fwq_id;
		this.fwq_id = this.fwq_id < min ? min : this.fwq_id;
		this.fwq_id = this.fwq_id > max ? max : this.fwq_id;
		setFwq_id(this.fwq_id);
	}	
	public int getZclvl() {
		return zclvl;
	}
	
	public void setZclvl(int zclvl) {
		this.zclvl = zclvl;
		addUpdateColumn("主城lvl",zclvl);
	}
	
	public void changeZclvlWith(int zclvl){
		this.zclvl += zclvl;
		setZclvl(this.zclvl);
	}
	
	public void changeZclvlWithMin(int zclvl,int min){
		this.zclvl += zclvl;
		this.zclvl = this.zclvl < min ? min : this.zclvl;
		setZclvl(this.zclvl);
	}
	
	public void changeZclvlWithMax(int zclvl,int max){
		this.zclvl += zclvl;
		this.zclvl = this.zclvl > max ? max : this.zclvl;
		setZclvl(this.zclvl);
	}
	
	public void changeZclvlWithMaxMin(int zclvl,int max,int min){
		this.zclvl += zclvl;
		this.zclvl = this.zclvl < min ? min : this.zclvl;
		this.zclvl = this.zclvl > max ? max : this.zclvl;
		setZclvl(this.zclvl);
	}	
	public boolean getSd() {
		return sd;
	}
	
	public void setSd(boolean sd) {
		this.sd = sd;
		addUpdateColumn("锁定",sd);
	}
	
	public int getLm_id() {
		return lm_id;
	}
	
	public void setLm_id(int lm_id) {
		this.lm_id = lm_id;
		addUpdateColumn("联盟_id",lm_id);
	}
	
	public void changeLm_idWith(int lm_id){
		this.lm_id += lm_id;
		setLm_id(this.lm_id);
	}
	
	public void changeLm_idWithMin(int lm_id,int min){
		this.lm_id += lm_id;
		this.lm_id = this.lm_id < min ? min : this.lm_id;
		setLm_id(this.lm_id);
	}
	
	public void changeLm_idWithMax(int lm_id,int max){
		this.lm_id += lm_id;
		this.lm_id = this.lm_id > max ? max : this.lm_id;
		setLm_id(this.lm_id);
	}
	
	public void changeLm_idWithMaxMin(int lm_id,int max,int min){
		this.lm_id += lm_id;
		this.lm_id = this.lm_id < min ? min : this.lm_id;
		this.lm_id = this.lm_id > max ? max : this.lm_id;
		setLm_id(this.lm_id);
	}	
	
	//id
	public List<Yhjz> getYhjzsFkYh_id(){
		return YhjzCache.getByYh_id(id);
	}
	
	//id
	public List<Yhys> getYhyssFkYh_id(){
		return YhysCache.getByYh_id(id);
	}
	
	//id
	public List<Yhbb> getYhbbsFkYh_id(){
		return YhbbCache.getByYh_id(id);
	}
	
	//id
	public List<Yhyx> getYhyxsFkYh_id(){
		return YhyxCache.getByYh_id(id);
	}
	
	//id
	public List<Yhzs> getYhzssFkYh_id(){
		return YhzsCache.getByYh_id(id);
	}
	
	//id
	public List<Yhyj> getYhyjsFkFjrid(){
		return YhyjCache.getByFjrid(id);
	}
	
	//id
	public List<Yhyj> getYhyjsFkJsrid(){
		return YhyjCache.getByJsrid(id);
	}
	
	//id
	public List<Yhxj> getYhxjsFkYh_id(){
		return YhxjCache.getByYh_id(id);
	}
	
	//id
	public List<Yhza> getYhzasFkYh_id(){
		return YhzaCache.getByYh_id(id);
	}
	
	//id
	public List<Lt> getLtsFkJsrid(){
		return LtCache.getByJsrid(id);
	}
	
	//id
	public List<Lt> getLtsFkFyrid(){
		return LtCache.getByFyrid(id);
	}
	
	//id
	public List<Lm> getLmsFkCjrid(){
		return LmCache.getByCjrid(id);
	}
	
	//用户类型_id
	public Yhlx getYhlxPkYhlx_id(){
		return YhlxCache.getById(yhlx_id);
	}
	//联盟_id
	public Lm getLmPkLm_id(){
		return LmCache.getById(lm_id);
	}
	//服务器_id
	public Fwq getFwqPkFwq_id(){
		return FwqCache.getById(fwq_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("nc", this.nc);
        result.put("cjsj", this.cjsj);
        result.put("zhdlsj", this.zhdlsj);
        result.put("yy", this.yy);
        result.put("zh", this.zh);
        result.put("mm", this.mm);
        result.put("bs", this.bs);
        result.put("jbs", this.jbs);
        result.put("jb", this.jb);
        result.put("s", this.s);
        result.put("xld", this.xld);
        result.put("sb", this.sb);
        result.put("sbxt", this.sbxt);
        result.put("fbl", this.fbl);
        result.put("qd", this.qd);
        result.put("yhlx_id", this.yhlx_id);
        result.put("yhlvl", this.yhlvl);
        result.put("yhlvl_exp", this.yhlvl_exp);
        result.put("vIP", this.vIP);
        result.put("vIP_exp", this.vIP_exp);
        result.put("czje", this.czje);
        result.put("xy", this.xy);
        result.put("khdbb", this.khdbb);
        result.put("fwq_id", this.fwq_id);
        result.put("zclvl", this.zclvl);
        result.put("sd", this.sd);
        result.put("lm_id", this.lm_id);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("昵称", this.nc);
        result.put("创建时间", this.cjsj);
        result.put("最后登录时间", this.zhdlsj);
        result.put("语言", this.yy);
        result.put("账号", this.zh);
        result.put("密码", this.mm);
        result.put("宝石", this.bs);
        result.put("假宝石", this.jbs);
        result.put("金币", this.jb);
        result.put("水", this.s);
        result.put("修炼点", this.xld);
        result.put("设备", this.sb);
        result.put("设备系统", this.sbxt);
        result.put("分辨率", this.fbl);
        result.put("渠道", this.qd);
        result.put("用户类型_id", this.yhlx_id);
        result.put("用户lvl", this.yhlvl);
        result.put("用户lvl_exp", this.yhlvl_exp);
        result.put("VIP", this.vIP);
        result.put("VIP_exp", this.vIP_exp);
        result.put("充值金额", this.czje);
        result.put("幸运", this.xy);
        result.put("客户端版本", this.khdbb);
        result.put("服务器_id", this.fwq_id);
        result.put("主城lvl", this.zclvl);
        result.put("锁定", this.sd);
        result.put("联盟_id", this.lm_id);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeString(out,this.nc);
		    SK_OutputStream.writeString(out,this.yy);
		    SK_OutputStream.writeString(out,this.zh);
		    SK_OutputStream.writeString(out,this.mm);
		    SK_OutputStream.writeInt(out,this.bs);
		    SK_OutputStream.writeInt(out,this.jbs);
		    SK_OutputStream.writeInt(out,this.jb);
		    SK_OutputStream.writeInt(out,this.s);
		    SK_OutputStream.writeInt(out,this.xld);
		    SK_OutputStream.writeString(out,this.sb);
		    SK_OutputStream.writeString(out,this.sbxt);
		    SK_OutputStream.writeString(out,this.fbl);
		    SK_OutputStream.writeString(out,this.qd);
		    SK_OutputStream.writeInt(out,this.yhlx_id);
		    SK_OutputStream.writeInt(out,this.yhlvl);
		    SK_OutputStream.writeInt(out,this.yhlvl_exp);
		    SK_OutputStream.writeInt(out,this.vIP);
		    SK_OutputStream.writeInt(out,this.vIP_exp);
		    SK_OutputStream.writeInt(out,this.czje);
		    SK_OutputStream.writeInt(out,this.xy);
		    SK_OutputStream.writeString(out,this.khdbb);
		    SK_OutputStream.writeInt(out,this.fwq_id);
		    SK_OutputStream.writeInt(out,this.zclvl);
		    SK_OutputStream.writeInt(out,this.lm_id);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public Yh createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yh yh = new Yh();
		    yh.id = SK_InputStream.readInt(in,null);
		    yh.nc = SK_InputStream.readString(in,null);
		    yh.yy = SK_InputStream.readString(in,null);
		    yh.zh = SK_InputStream.readString(in,null);
		    yh.mm = SK_InputStream.readString(in,null);
		    yh.bs = SK_InputStream.readInt(in,null);
		    yh.jbs = SK_InputStream.readInt(in,null);
		    yh.jb = SK_InputStream.readInt(in,null);
		    yh.s = SK_InputStream.readInt(in,null);
		    yh.xld = SK_InputStream.readInt(in,null);
		    yh.sb = SK_InputStream.readString(in,null);
		    yh.sbxt = SK_InputStream.readString(in,null);
		    yh.fbl = SK_InputStream.readString(in,null);
		    yh.qd = SK_InputStream.readString(in,null);
		    yh.yhlx_id = SK_InputStream.readInt(in,null);
		    yh.yhlvl = SK_InputStream.readInt(in,null);
		    yh.yhlvl_exp = SK_InputStream.readInt(in,null);
		    yh.vIP = SK_InputStream.readInt(in,null);
		    yh.vIP_exp = SK_InputStream.readInt(in,null);
		    yh.czje = SK_InputStream.readInt(in,null);
		    yh.xy = SK_InputStream.readInt(in,null);
		    yh.khdbb = SK_InputStream.readString(in,null);
		    yh.fwq_id = SK_InputStream.readInt(in,null);
		    yh.zclvl = SK_InputStream.readInt(in,null);
		    yh.lm_id = SK_InputStream.readInt(in,null);
		    return yh;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yh> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yh> yhs = new ArrayList<Yh>();
		for (Map<String, Object> map : list) {
			yhs.add(createForColumnNameMap(map));
		}
		return yhs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yh createForColumnNameMap(Map<String, Object> map){
    	Yh obj = new Yh();
	    obj.id = SK_Map.getInt("id", map);
	    obj.nc = SK_Map.getString("昵称", map);
	    obj.yy = SK_Map.getString("语言", map);
	    obj.zh = SK_Map.getString("账号", map);
	    obj.mm = SK_Map.getString("密码", map);
	    obj.bs = SK_Map.getInt("宝石", map);
	    obj.jbs = SK_Map.getInt("假宝石", map);
	    obj.jb = SK_Map.getInt("金币", map);
	    obj.s = SK_Map.getInt("水", map);
	    obj.xld = SK_Map.getInt("修炼点", map);
	    obj.sb = SK_Map.getString("设备", map);
	    obj.sbxt = SK_Map.getString("设备系统", map);
	    obj.fbl = SK_Map.getString("分辨率", map);
	    obj.qd = SK_Map.getString("渠道", map);
	    obj.yhlx_id = SK_Map.getInt("用户类型_id", map);
	    obj.yhlvl = SK_Map.getInt("用户lvl", map);
	    obj.yhlvl_exp = SK_Map.getInt("用户lvl_exp", map);
	    obj.vIP = SK_Map.getInt("VIP", map);
	    obj.vIP_exp = SK_Map.getInt("VIP_exp", map);
	    obj.czje = SK_Map.getInt("充值金额", map);
	    obj.xy = SK_Map.getInt("幸运", map);
	    obj.khdbb = SK_Map.getString("客户端版本", map);
	    obj.fwq_id = SK_Map.getInt("服务器_id", map);
	    obj.zclvl = SK_Map.getInt("主城lvl", map);
	    obj.lm_id = SK_Map.getInt("联盟_id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yh> createForList(List<Map<String, Object>> list){
    	List<Yh> yhs = new ArrayList<Yh>();
		for (Map<String, Object> map : list) {
			yhs.add(createForColumnNameMap(map));
		}
		return yhs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yh createForMap(Map<String, Object> map){
    	Yh obj = new Yh();
	    obj.id = SK_Map.getInt("id", map);
	    obj.nc = SK_Map.getString("nc", map);
	    obj.yy = SK_Map.getString("yy", map);
	    obj.zh = SK_Map.getString("zh", map);
	    obj.mm = SK_Map.getString("mm", map);
	    obj.bs = SK_Map.getInt("bs", map);
	    obj.jbs = SK_Map.getInt("jbs", map);
	    obj.jb = SK_Map.getInt("jb", map);
	    obj.s = SK_Map.getInt("s", map);
	    obj.xld = SK_Map.getInt("xld", map);
	    obj.sb = SK_Map.getString("sb", map);
	    obj.sbxt = SK_Map.getString("sbxt", map);
	    obj.fbl = SK_Map.getString("fbl", map);
	    obj.qd = SK_Map.getString("qd", map);
	    obj.yhlx_id = SK_Map.getInt("yhlx_id", map);
	    obj.yhlvl = SK_Map.getInt("yhlvl", map);
	    obj.yhlvl_exp = SK_Map.getInt("yhlvl_exp", map);
	    obj.vIP = SK_Map.getInt("vIP", map);
	    obj.vIP_exp = SK_Map.getInt("vIP_exp", map);
	    obj.czje = SK_Map.getInt("czje", map);
	    obj.xy = SK_Map.getInt("xy", map);
	    obj.khdbb = SK_Map.getString("khdbb", map);
	    obj.fwq_id = SK_Map.getInt("fwq_id", map);
	    obj.zclvl = SK_Map.getInt("zclvl", map);
	    obj.lm_id = SK_Map.getInt("lm_id", map);
        return obj;
    }
    
    /** 延迟插入数据库 */
    public Yh insert(){
    	return YhCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yh update(){
    	return YhCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YhCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yh insertAndFlush(){
    	return YhCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yh updateAndFlush(){
    	return YhCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YhCache.deleteAndFlush(this);
    }
}