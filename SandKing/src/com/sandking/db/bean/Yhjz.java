package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.YhCache;
import com.sandking.db.cache.JzdlCache;
import com.sandking.db.cache.BydlCache;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import java.io.ByteArrayInputStream;
import com.sandking.db.cache.YhjzCache;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 用户建筑
 */
public class Yhjz {

	public static final String TABLENAME = "用户建筑";
	public static final String CLASSNAME = "Yhjz"; 
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 建筑id */
	private int jzid;
	
	/** 建筑lvl */
	private int jzlvl;
	
	/** x */
	private int x;
	
	/** y */
	private int y;
	
	/** 用户_id */
	private int yh_id;
	
	
	public Yhjz() {
		super();
	}
	
	public Yhjz(int id, int jzid, int jzlvl, int x, int y, int yh_id) {
		super();
		this.id = id;
		this.jzid = jzid;
		this.jzlvl = jzlvl;
		this.x = x;
		this.y = y;
		this.yh_id = yh_id;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public int getJzid() {
		return jzid;
	}
	
	public void setJzid(int jzid) {
		this.jzid = jzid;
		addUpdateColumn("建筑id",jzid);
	}
	
	public void changeJzidWith(int jzid){
		this.jzid += jzid;
		setJzid(this.jzid);
	}
	
	public void changeJzidWithMin(int jzid,int min){
		this.jzid += jzid;
		this.jzid = this.jzid < min ? min : this.jzid;
		setJzid(this.jzid);
	}
	
	public void changeJzidWithMax(int jzid,int max){
		this.jzid += jzid;
		this.jzid = this.jzid > max ? max : this.jzid;
		setJzid(this.jzid);
	}
	
	public void changeJzidWithMaxMin(int jzid,int max,int min){
		this.jzid += jzid;
		this.jzid = this.jzid < min ? min : this.jzid;
		this.jzid = this.jzid > max ? max : this.jzid;
		setJzid(this.jzid);
	}	
	public int getJzlvl() {
		return jzlvl;
	}
	
	public void setJzlvl(int jzlvl) {
		this.jzlvl = jzlvl;
		addUpdateColumn("建筑lvl",jzlvl);
	}
	
	public void changeJzlvlWith(int jzlvl){
		this.jzlvl += jzlvl;
		setJzlvl(this.jzlvl);
	}
	
	public void changeJzlvlWithMin(int jzlvl,int min){
		this.jzlvl += jzlvl;
		this.jzlvl = this.jzlvl < min ? min : this.jzlvl;
		setJzlvl(this.jzlvl);
	}
	
	public void changeJzlvlWithMax(int jzlvl,int max){
		this.jzlvl += jzlvl;
		this.jzlvl = this.jzlvl > max ? max : this.jzlvl;
		setJzlvl(this.jzlvl);
	}
	
	public void changeJzlvlWithMaxMin(int jzlvl,int max,int min){
		this.jzlvl += jzlvl;
		this.jzlvl = this.jzlvl < min ? min : this.jzlvl;
		this.jzlvl = this.jzlvl > max ? max : this.jzlvl;
		setJzlvl(this.jzlvl);
	}	
	public int getX() {
		return x;
	}
	
	public void setX(int x) {
		this.x = x;
		addUpdateColumn("x",x);
	}
	
	public void changeXWith(int x){
		this.x += x;
		setX(this.x);
	}
	
	public void changeXWithMin(int x,int min){
		this.x += x;
		this.x = this.x < min ? min : this.x;
		setX(this.x);
	}
	
	public void changeXWithMax(int x,int max){
		this.x += x;
		this.x = this.x > max ? max : this.x;
		setX(this.x);
	}
	
	public void changeXWithMaxMin(int x,int max,int min){
		this.x += x;
		this.x = this.x < min ? min : this.x;
		this.x = this.x > max ? max : this.x;
		setX(this.x);
	}	
	public int getY() {
		return y;
	}
	
	public void setY(int y) {
		this.y = y;
		addUpdateColumn("y",y);
	}
	
	public void changeYWith(int y){
		this.y += y;
		setY(this.y);
	}
	
	public void changeYWithMin(int y,int min){
		this.y += y;
		this.y = this.y < min ? min : this.y;
		setY(this.y);
	}
	
	public void changeYWithMax(int y,int max){
		this.y += y;
		this.y = this.y > max ? max : this.y;
		setY(this.y);
	}
	
	public void changeYWithMaxMin(int y,int max,int min){
		this.y += y;
		this.y = this.y < min ? min : this.y;
		this.y = this.y > max ? max : this.y;
		setY(this.y);
	}	
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
		addUpdateColumn("用户_id",yh_id);
	}
	
	public void changeYh_idWith(int yh_id){
		this.yh_id += yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMin(int yh_id,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMax(int yh_id,int max){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMaxMin(int yh_id,int max,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}	
	
	//id
	public List<Bydl> getBydlsFkYhjz_id(){
		return BydlCache.getByYhjz_id(id);
	}
	
	//id
	public List<Jzdl> getJzdlsFkYhjz_id(){
		return JzdlCache.getByYhjz_id(id);
	}
	
	//用户_id
	public Yh getYhPkYh_id(){
		return YhCache.getById(yh_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("jzid", this.jzid);
        result.put("jzlvl", this.jzlvl);
        result.put("x", this.x);
        result.put("y", this.y);
        result.put("yh_id", this.yh_id);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("建筑id", this.jzid);
        result.put("建筑lvl", this.jzlvl);
        result.put("x", this.x);
        result.put("y", this.y);
        result.put("用户_id", this.yh_id);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeInt(out,this.jzid);
		    SK_OutputStream.writeInt(out,this.jzlvl);
		    SK_OutputStream.writeInt(out,this.x);
		    SK_OutputStream.writeInt(out,this.y);
		    SK_OutputStream.writeInt(out,this.yh_id);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public Yhjz createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yhjz yhjz = new Yhjz();
		    yhjz.id = SK_InputStream.readInt(in,null);
		    yhjz.jzid = SK_InputStream.readInt(in,null);
		    yhjz.jzlvl = SK_InputStream.readInt(in,null);
		    yhjz.x = SK_InputStream.readInt(in,null);
		    yhjz.y = SK_InputStream.readInt(in,null);
		    yhjz.yh_id = SK_InputStream.readInt(in,null);
		    return yhjz;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhjz> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yhjz> yhjzs = new ArrayList<Yhjz>();
		for (Map<String, Object> map : list) {
			yhjzs.add(createForColumnNameMap(map));
		}
		return yhjzs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhjz createForColumnNameMap(Map<String, Object> map){
    	Yhjz obj = new Yhjz();
	    obj.id = SK_Map.getInt("id", map);
	    obj.jzid = SK_Map.getInt("建筑id", map);
	    obj.jzlvl = SK_Map.getInt("建筑lvl", map);
	    obj.x = SK_Map.getInt("x", map);
	    obj.y = SK_Map.getInt("y", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhjz> createForList(List<Map<String, Object>> list){
    	List<Yhjz> yhjzs = new ArrayList<Yhjz>();
		for (Map<String, Object> map : list) {
			yhjzs.add(createForColumnNameMap(map));
		}
		return yhjzs;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhjz createForMap(Map<String, Object> map){
    	Yhjz obj = new Yhjz();
	    obj.id = SK_Map.getInt("id", map);
	    obj.jzid = SK_Map.getInt("jzid", map);
	    obj.jzlvl = SK_Map.getInt("jzlvl", map);
	    obj.x = SK_Map.getInt("x", map);
	    obj.y = SK_Map.getInt("y", map);
	    obj.yh_id = SK_Map.getInt("yh_id", map);
        return obj;
    }
    
    /** 延迟插入数据库 */
    public Yhjz insert(){
    	return YhjzCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yhjz update(){
    	return YhjzCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YhjzCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yhjz insertAndFlush(){
    	return YhjzCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yhjz updateAndFlush(){
    	return YhjzCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YhjzCache.deleteAndFlush(this);
    }
}