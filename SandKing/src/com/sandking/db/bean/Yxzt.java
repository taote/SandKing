package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import java.util.Map;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import com.sandking.db.cache.YxztCache;
import com.sandking.db.cache.YhyxCache;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 英雄状态
 */
public class Yxzt {

	public static final String TABLENAME = "英雄状态";
	public static final String CLASSNAME = "Yxzt"; 
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 名称 */
	private String mc;
	
	
	public Yxzt() {
		super();
	}
	
	public Yxzt(int id, String mc) {
		super();
		this.id = id;
		this.mc = mc;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public String getMc() {
		return mc;
	}
	
	public void setMc(String mc) {
		this.mc = mc;
		addUpdateColumn("名称",mc);
	}
	
	
	//id
	public List<Yhyx> getYhyxsFkYxztid(){
		return YhyxCache.getByYxztid(id);
	}
	
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("mc", this.mc);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("名称", this.mc);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeString(out,this.mc);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public Yxzt createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yxzt yxzt = new Yxzt();
		    yxzt.id = SK_InputStream.readInt(in,null);
		    yxzt.mc = SK_InputStream.readString(in,null);
		    return yxzt;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yxzt> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yxzt> yxzts = new ArrayList<Yxzt>();
		for (Map<String, Object> map : list) {
			yxzts.add(createForColumnNameMap(map));
		}
		return yxzts;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yxzt createForColumnNameMap(Map<String, Object> map){
    	Yxzt obj = new Yxzt();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("名称", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yxzt> createForList(List<Map<String, Object>> list){
    	List<Yxzt> yxzts = new ArrayList<Yxzt>();
		for (Map<String, Object> map : list) {
			yxzts.add(createForColumnNameMap(map));
		}
		return yxzts;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yxzt createForMap(Map<String, Object> map){
    	Yxzt obj = new Yxzt();
	    obj.id = SK_Map.getInt("id", map);
	    obj.mc = SK_Map.getString("mc", map);
        return obj;
    }
    
    /** 延迟插入数据库 */
    public Yxzt insert(){
    	return YxztCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yxzt update(){
    	return YxztCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YxztCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yxzt insertAndFlush(){
    	return YxztCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yxzt updateAndFlush(){
    	return YxztCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YxztCache.deleteAndFlush(this);
    }
}