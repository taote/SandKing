package com.sandking.db.bean;

import com.sandking.tools.SK_Map;
import java.util.ArrayList;
import com.sandking.db.cache.YhCache;
import java.util.Map;
import com.sandking.db.cache.YhysCache;
import com.sandking.io.SK_OutputStream;
import java.util.List;
import com.sandking.io.SK_InputStream;
import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.io.ByteArrayOutputStream;

/**
 * 用户映射
 */
public class Yhys {

	public static final String TABLENAME = "用户映射";
	public static final String CLASSNAME = "Yhys"; 
	/**
	 * 需要更新字段集
	 */
	private Map<String, Object> updateColumns;

	/** id */
	private int id;
	
	/** 建筑 */
	private String jz;
	
	/** 障碍 */
	private String za;
	
	/** 装饰 */
	private String zs;
	
	/** 陷阱 */
	private String xj;
	
	/** 用户_id */
	private int yh_id;
	
	
	public Yhys() {
		super();
	}
	
	public Yhys(int id, String jz, String za, String zs, String xj, int yh_id) {
		super();
		this.id = id;
		this.jz = jz;
		this.za = za;
		this.zs = zs;
		this.xj = xj;
		this.yh_id = yh_id;
	}
	
	public Map<String, Object> getUpdateColumns() {
		if(updateColumns == null)
			updateColumns = new HashMap<String, Object>();
		return updateColumns;
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
		addUpdateColumn("id",id);
	}
	
	public void changeIdWith(int id){
		this.id += id;
		setId(this.id);
	}
	
	public void changeIdWithMin(int id,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMax(int id,int max){
		this.id += id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}
	
	public void changeIdWithMaxMin(int id,int max,int min){
		this.id += id;
		this.id = this.id < min ? min : this.id;
		this.id = this.id > max ? max : this.id;
		setId(this.id);
	}	
	public String getJz() {
		return jz;
	}
	
	public void setJz(String jz) {
		this.jz = jz;
		addUpdateColumn("建筑",jz);
	}
	
	public String getZa() {
		return za;
	}
	
	public void setZa(String za) {
		this.za = za;
		addUpdateColumn("障碍",za);
	}
	
	public String getZs() {
		return zs;
	}
	
	public void setZs(String zs) {
		this.zs = zs;
		addUpdateColumn("装饰",zs);
	}
	
	public String getXj() {
		return xj;
	}
	
	public void setXj(String xj) {
		this.xj = xj;
		addUpdateColumn("陷阱",xj);
	}
	
	public int getYh_id() {
		return yh_id;
	}
	
	public void setYh_id(int yh_id) {
		this.yh_id = yh_id;
		addUpdateColumn("用户_id",yh_id);
	}
	
	public void changeYh_idWith(int yh_id){
		this.yh_id += yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMin(int yh_id,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMax(int yh_id,int max){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}
	
	public void changeYh_idWithMaxMin(int yh_id,int max,int min){
		this.yh_id += yh_id;
		this.yh_id = this.yh_id < min ? min : this.yh_id;
		this.yh_id = this.yh_id > max ? max : this.yh_id;
		setYh_id(this.yh_id);
	}	
	
	//用户_id
	public Yh getYhPkYh_id(){
		return YhCache.getById(yh_id);
	}
	public void addUpdateColumn(String key, Object val) {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.put(key, val);
	}
	
	public void clearUpdateColumn() {
		Map<String, Object> map = getUpdateColumns();
		if (map == null)
			return;
		map.clear();
	}
	
	public Map<String, Object> toMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("jz", this.jz);
        result.put("za", this.za);
        result.put("zs", this.zs);
        result.put("xj", this.xj);
        result.put("yh_id", this.yh_id);
        return result;
    }
    
    public String toString(){
        return toMap().toString();
    }
    
    public Map<String, Object> toColumnNameMap(){
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("id", this.id);
        result.put("建筑", this.jz);
        result.put("障碍", this.za);
        result.put("装饰", this.zs);
        result.put("陷阱", this.xj);
        result.put("用户_id", this.yh_id);
        return result;
    }
    
    public String toColumnNameString(){
        return toColumnNameMap().toString();
    }
    
    public byte[] toBytes() throws Exception {
    	try (ByteArrayOutputStream out = new ByteArrayOutputStream();) {
		    SK_OutputStream.writeInt(out,this.id);
		    SK_OutputStream.writeString(out,this.jz);
		    SK_OutputStream.writeString(out,this.za);
		    SK_OutputStream.writeString(out,this.zs);
		    SK_OutputStream.writeString(out,this.xj);
		    SK_OutputStream.writeInt(out,this.yh_id);
		    return out.toByteArray();
    	}catch (Exception e) {
            throw e;
        }
    }
    
     public Yhys createForBytes(byte[] _byte) throws Exception {
     	try (ByteArrayInputStream in = new ByteArrayInputStream(_byte);) {
	     	Yhys yhys = new Yhys();
		    yhys.id = SK_InputStream.readInt(in,null);
		    yhys.jz = SK_InputStream.readString(in,null);
		    yhys.za = SK_InputStream.readString(in,null);
		    yhys.zs = SK_InputStream.readString(in,null);
		    yhys.xj = SK_InputStream.readString(in,null);
		    yhys.yh_id = SK_InputStream.readInt(in,null);
		    return yhys;
    	}catch (Exception e) {
            throw e;
        }
     }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhys> createForColumnNameList(List<Map<String, Object>> list){
    	List<Yhys> yhyss = new ArrayList<Yhys>();
		for (Map<String, Object> map : list) {
			yhyss.add(createForColumnNameMap(map));
		}
		return yhyss;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhys createForColumnNameMap(Map<String, Object> map){
    	Yhys obj = new Yhys();
	    obj.id = SK_Map.getInt("id", map);
	    obj.jz = SK_Map.getString("建筑", map);
	    obj.za = SK_Map.getString("障碍", map);
	    obj.zs = SK_Map.getString("装饰", map);
	    obj.xj = SK_Map.getString("陷阱", map);
	    obj.yh_id = SK_Map.getInt("用户_id", map);
        return obj;
    }
    
    /**
     * 根据list创建对象
     */
    public static List<Yhys> createForList(List<Map<String, Object>> list){
    	List<Yhys> yhyss = new ArrayList<Yhys>();
		for (Map<String, Object> map : list) {
			yhyss.add(createForColumnNameMap(map));
		}
		return yhyss;
    }
    
    /**
     * 根据map创建对象
     */
    public static Yhys createForMap(Map<String, Object> map){
    	Yhys obj = new Yhys();
	    obj.id = SK_Map.getInt("id", map);
	    obj.jz = SK_Map.getString("jz", map);
	    obj.za = SK_Map.getString("za", map);
	    obj.zs = SK_Map.getString("zs", map);
	    obj.xj = SK_Map.getString("xj", map);
	    obj.yh_id = SK_Map.getInt("yh_id", map);
        return obj;
    }
    
    /** 延迟插入数据库 */
    public Yhys insert(){
    	return YhysCache.insert(this);
    }
    /** 延迟更新数据库 */
    public Yhys update(){
    	return YhysCache.update(this);
    }
    /** 延迟删除数据库 */
    public boolean delete(){
    	return YhysCache.delete(this);
    }
    /** 即时插入数据库 */
    public Yhys insertAndFlush(){
    	return YhysCache.insertAndFlush(this);
    }
    /** 即时更新数据库 */
    public Yhys updateAndFlush(){
    	return YhysCache.updateAndFlush(this);
    }
    /** 即时删除数据库 */
    public boolean deleteAndFlush(){
    	return YhysCache.deleteAndFlush(this);
    }
}