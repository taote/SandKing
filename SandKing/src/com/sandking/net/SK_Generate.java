package com.sandking.net;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;

import com.sandking.net.template.NTP;
import com.sandking.tools.SK_Map;

import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;

/**
 * @UserName : SandKing
 * @DataTime : 2014年2月27日 上午10:22:39
 * @Description ：网络接口生成器
 */
public class SK_Generate {
	private static Configuration cfg;

	public static Configuration getConfiguration() {
		if (cfg == null) {
			cfg = new Configuration();
			cfg.setClassForTemplateLoading(NTP.class, "");
			cfg.setObjectWrapper(new DefaultObjectWrapper());
		}
		return cfg;
	}

	public static Configuration getConfiguration(String path) {
		if (cfg == null) {
			try {
				cfg = new Configuration();
				cfg.setDirectoryForTemplateLoading(new File(path));
				cfg.setObjectWrapper(new BeansWrapper());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return cfg;
	}

	/**
	 * 生成常量对象
	 * 
	 * @param data
	 * @param cfg
	 * @throws IOException
	 */
	public static void runConstant(Map<String, Object> data, Configuration cfg)
			throws IOException {
		String[] templateNames = new String[]{"constant_java.ftl",
				"constant_c#.ftl"};
		String[] fileNames = new String[]{"/Constant.java", "/Constant.cs"};
		String pathname = "src/" + SK_Map.getString("packageName", data);
		pathname = pathname.replace(".", "/");
		File file = null;
		Template template = null;
		for (int i = 0; i < templateNames.length; i++) {
			file = new File(pathname + fileNames[i]);
			template = cfg.getTemplate(templateNames[i]);
			fileWriter(template, file, data);
		}
	}
	/**
	 * 生成网络对象
	 * 
	 * @param data
	 * @param cfg
	 * @throws IOException
	 */
	public static void runNetObject(Map<String, Object> data, Configuration cfg)
			throws IOException {
		String[] templateNames = new String[]{"netObject_java.ftl",
				"netObject_c#.ftl"};
		String[] fileNames = new String[]{"/NetObject.java", "/NetObject.cs"};
		String pathname = "src/" + SK_Map.getString("packageName", data);
		pathname = pathname.replace(".", "/");
		File file = null;
		Template template = null;
		for (int i = 0; i < templateNames.length; i++) {
			file = new File(pathname + fileNames[i]);
			template = cfg.getTemplate(templateNames[i]);
			fileWriter(template, file, data);
		}
	}

	/**
	 * 生成接口
	 * 
	 * @param data
	 * @param cfg
	 * @throws IOException
	 */
	public static void runInterface(Map<String, Object> data, Configuration cfg)
			throws IOException {
		String[] templateNames = new String[]{"interface_java.ftl",
				"interface_c#.ftl"};
		String[] fileNames = new String[]{"/NetInterface.java", "/NetInterface.cs"};
		String pathname = "src/" + SK_Map.getString("packageName", data);
		pathname = pathname.replace(".", "/");
		File file = null;
		Template template = null;
		for (int i = 0; i < templateNames.length; i++) {
			file = new File(pathname + fileNames[i]);
			template = cfg.getTemplate(templateNames[i]);
			fileWriter(template, file, data);
		}
	}

	private static void fileWriter(Template template, File file,
			Map<String, Object> data) {
		try {
			FileWriter fileWriter = new FileWriter(file);
			template.process(data, fileWriter);
			fileWriter.flush();
			// 打印输出
			Writer writer = new OutputStreamWriter(System.out);
			template.process(data, writer);
			writer.flush();
			System.out.println();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
