using System.Collections;

namespace com.sandking.net
{
	/**
	 * 用户
	 */
	public class UserInfos{
		/**
		 *	用户id
		 */
		private int id = 1;
		/**
		 *	昵称
		 */
		private string name = "1";
		/**
		 *	年龄
		 */
		private int age = 1;
	
		public int Id {
			get{ return  id;}
			set{ id = value;}
		}
		public string Name {
			get{ return  name;}
			set{ name = value;}
		}
		public int Age {
			get{ return  age;}
			set{ age = value;}
		}
	}
}