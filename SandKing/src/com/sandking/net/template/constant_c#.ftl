using System.Collections;

namespace ${packageName}
{
	<#list classs as class>
	/**
	 * ${class.classRemark}
	 */
	public class ${class.className}{
	<#list class.classFields as field>
		/**
		 *	${field.fieldRemark}
		 */
		<#if field.fieldType == "String">
		public const string ${field.fieldName} = "${field.fieldValues}";
		<#else>
		public const ${field.fieldType} ${field.fieldName} = ${field.fieldValues};
		</#if>
	</#list>
	}
	</#list>
}