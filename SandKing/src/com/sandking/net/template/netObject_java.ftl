package ${packageName};

<#list classs as class>
/**
 * ${class.classRemark}
 */
class ${class.className}{
<#list class.classFields as field>
	/**
	 *	${field.fieldRemark}
	 */
	private ${field.fieldType} ${field.fieldName};
</#list>

<#list class.classFields as field>
	public ${field.fieldType} get${field.fieldName?cap_first}(){
		return ${field.fieldName};
	}
	
	public void set${field.fieldName?cap_first}(${field.fieldType} ${field.fieldName}){
		this.${field.fieldName} = ${field.fieldName};
	}
	
</#list>
}
</#list>