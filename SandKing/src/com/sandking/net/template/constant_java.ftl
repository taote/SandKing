package ${packageName};

<#list classs as class>
/**
 * ${class.classRemark}
 */
class ${class.className}{
<#list class.classFields as field>
	/**
	 *	${field.fieldRemark}
	 */
	<#if field.fieldType == "String">
	public static final ${field.fieldType} ${field.fieldName} = "${field.fieldValues}";
	<#else>
	public static final ${field.fieldType} ${field.fieldName} = ${field.fieldValues};
	</#if>
</#list>
}
</#list>