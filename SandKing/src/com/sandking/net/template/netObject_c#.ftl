using System.Collections;

namespace ${packageName}
{
	<#list classs as class>
	/**
	 * ${class.classRemark}
	 */
	public class ${class.className}{
	<#list class.classFields as field>
		/**
		 *	${field.fieldRemark}
		 */
		<#if field.fieldType == "String">
		private string ${field.fieldName} = "${field.fieldValues}";
		<#else>
		private ${field.fieldType} ${field.fieldName} = ${field.fieldValues};
		</#if>
	</#list>
	
	<#list class.classFields as field>
		<#if field.fieldType == "String">
		public string ${field.fieldName?cap_first} {
		<#else>
		public ${field.fieldType} ${field.fieldName?cap_first} {
		</#if>
			get{ return  ${field.fieldName};}
			set{ ${field.fieldName} = value;}
		}
	</#list>
	}
	</#list>
}