package com.sandking.net;

/**
 * 用户
 */
class UserInfos{
	/**
	 *	用户id
	 */
	private int id;
	/**
	 *	昵称
	 */
	private String name;
	/**
	 *	年龄
	 */
	private int age;

	public int getId(){
		return id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public int getAge(){
		return age;
	}
	
	public void setAge(int age){
		this.age = age;
	}
	
}
