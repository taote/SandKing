package com.sandking.tools;

import java.util.Date;

/**
 * @UserName : SandKing
 * @DataTime : 2013年11月26日 下午10:29:35
 * @Description ：Please describe this document
 */
public class SK_Date {
	public static final long TIME_MILLISECOND = 1;

	public static final long TIME_SECOND = 1000 * TIME_MILLISECOND;

	public static final long TIME_MINUTE = 60 * TIME_SECOND;

	public static final long TIME_HOUR = 60 * TIME_MINUTE;

	public static final long TIME_DAY = 24 * TIME_HOUR;

	public static final long TIME_WEEK = 7 * TIME_DAY;
	
	public static Date addWeek(int w,Date date) {
		return addMillisSecond(w * TIME_WEEK,date);
	}

	public static Date addDay(int d,Date date) {
		return addMillisSecond(d * TIME_DAY,date);
	}

	public static Date addHour(int h,Date date) {
		return addMillisSecond(h * TIME_HOUR,date);
	}

	public static Date addMinute(int m,Date date) {
		return addMillisSecond(m * TIME_MINUTE,date);
	}

	public static Date addSecond(int s,Date date) {
		return addMillisSecond(s * TIME_SECOND,date);
	}

	public static Date addMillisSecond(long t,Date date) {
		date.setTime(date.getTime() + t);
		return date;
	}
}
